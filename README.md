# Knowmads Backpackers
Ever wanted to connect with fellow travelers near you? Join us at Knowmads and connect with travelers around where you are. Travel doesn't have to be a lonely experience anymore.

![alt text](https://firebasestorage.googleapis.com/v0/b/carbon-syntax-155109.appspot.com/o/others%2Ffeature.jpg?alt=media&token=78814743-7b0c-4739-bb5f-a788e809b6b0 "Feature")

## Development

This repository is focused on developing Knowmads using React Native.

### Getting Started
* Follow the [setup guide](https://facebook.github.io/react-native/docs/getting-started.html) from React Native.
* Test the app by running ```npm start``` from the root directory.

### Building
A shell script ```auto-build.sh``` is available inside the ```android``` directory for generating a signed APK file.

### Cloud API
Visit the ```cloud_functions``` directory for more info.
