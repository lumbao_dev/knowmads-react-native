# Knowmads Backpacker Cloud API
> A series of useful cloud functions.

### getTrackingCoordinates
|Description|Return|
|---|---|
|Retrieves the tracking coordinates of the requested backpacker.|JSON Object|

| Param | Type | Required | Description | Default |
|---|---|---|---|---|
| `uid` | `string` | Required | User ID of the backpacker whose tracking coordinates are requested. | `null`

### submitFollowRequest
|Description|Return|
|---|---|
|Handles the follow request of the backpacker whether to confirm or revoke the request.|JSON Object|

| Param | Type | Required | Description | Default |
|---|---|---|---|---|
| `touid` | `string` | Required | Uid of the backpacker requesting for follow request service. | `null`
| `notifkey` | `string` | Required | Key of the notification retrived from Firebase. | `null`
| `action` | `string` | Required | Action of the request. Only two values are accepted (`confirm` or `revoke`)| `null`
