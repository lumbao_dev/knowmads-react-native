// Cloud API for Knowmads
// ======================

// These are used for atomic transactions
var functions = require('firebase-functions');
const admin = require('firebase-admin');
const async = require('async');
admin.initializeApp(functions.config().firebase);


exports.submitFollowRequest = functions.https.onRequest((req, res) => {
  var followSourceUid = null;
  // Source UID of the notification.
  const touid = req.query.touid;
  // Key of the notification.
  const notifkey = req.query.notifkey;
  // Action type (confirm or revoke the follow item)
  const action = req.query.action;

  const database = admin.database();

  let notifSourceRef = '/users/' + touid + '/notifications/' + notifkey + '/followSourceUid';
  let notifStatusRef = '/users/' + touid + '/notifications/' + notifkey + '/approved';
  let followersRef = '/users/' + touid + '/connections/followers/';

  database.ref(notifSourceRef)
    .once('value')
    .then(snap => {
      let data = snap.val();
      return data;
    })
    .then((value) => {

      let followingRef = '/users/' + value + '/connections/following/';
      let pendingRef = '/users/' + touid + '/connections/pending/';

      if (action == 'confirm') {
        let updateFollowers = JSON.parse('{"' + value + '":true' + '}');

        let updateFollowing = JSON.parse('{"' + touid + '":true' + '}');

        let notificationStatusUpdate = database.ref(notifStatusRef).set(true);

        let followersUpdate = database.ref(followersRef).update(updateFollowers);

        let followingUpdate = database.ref(followingRef).update(updateFollowing);

        let pendingReqUpdate = database.ref(pendingRef + value).remove();

        return Promise.all([notificationStatusUpdate, followersUpdate, followingUpdate, pendingReqUpdate]);
      }

      if (action == 'revoke') {
        let updatePending = JSON.parse('{"' + value + '":true' + '}');

        let notificationStatusUpdate = database.ref(notifStatusRef).set(false);

        let followersUpdate = database.ref(followersRef + value).remove();

        let followingUpdate = database.ref(followingRef + touid).remove();

        let pendingReqUpdate = database.ref(pendingRef).update(updatePending);

        return Promise.all([notificationStatusUpdate, followersUpdate, followingUpdate, pendingReqUpdate]);
      }

    })
    .then(() => {
      res.send(JSON.parse('{"statusOk":"ok", "action":"' + action + '"}'))
    }).catch((error) => {
      console.log(error);
      res.send(error)
    })
});

exports.followBackpacker = functions.https.onRequest((req, res) => {
  // Target UID of the notification.
  const touid = req.query.touid;
  // Source UID of the notification.
  const fromuid = req.query.fromuid;
  // Action type (follow or track)
  const action = req.query.action;

  const database = admin.database();

  let ref = 'users/' + touid + '/notifications/';

  database.ref(ref).push().then((res) => {
    let newPostKey = res.key;
    firestack.ServerValue.then(map => {
      const postData = {
        type: action,
        timestamp: map.TIMESTAMP,
        followSourceUid: fromuid,
        reviewed: false,
        approved: false
      }
      let updates = {}
      updates[ref + newPostKey] = postData;
      database.ref().update(updates).then(() => {
        res.send(JSON.parse('{"statusOk":"ok", "action":"' + action + '"}'))
      }).catch((error) => {
        console.log(error);
        res.send(error)
      });
    })
  })
})

// Get friends
exports.getFollowing = functions.https.onRequest((req, res) => {
  // Target UID of the notification.
  const touid = req.query.touid;

  const database = admin.database();

  let ref = 'users/' + touid + 'connections/following/';

  database.ref(ref)
    .once('value')
    .then(snap => {
      let data = snap.val();
      res.send(data)
    }).catch((error) => {
      console.log(error);
      res.send(error)
    })
})

// Get followers
exports.getFollowers = functions.https.onRequest((req, res) => {
  // Target UID of the notification.
  const touid = req.query.touid;

  const database = admin.database();

  let ref = 'users/' + touid + 'connections/followers/';

  database.ref(ref)
    .once('value')
    .then(snap => {
      let data = snap.val();
      res.send(data);
    }).catch((error) => {
      console.log(error);
      res.send(error)
    })
})

exports.getFriends = functions.https.onRequest((req, res) => {
  const touid = req.query.touid;
  const database = admin.database();
  let rootRef = 'users/';
  let followingRef = 'users/' + touid + '/connections/following';
  let followersRef = 'users/' + touid + '/connections/followers';
  let friends = null;
  let friendsFiltered = null;
  let keyList = [];

  database.ref(followingRef)
    .once('value')
    .then(snap => {
      if (snap.val() != null) {
        followingsnap = snap.val();
      } else {
        followingsnap = {}
      }
      return snap;
    })
    .then((value) => {
      return database.ref(followersRef)
        .once('value')
        .then(snap => {
          if (snap.val() != null) {
            followerssnap = snap.val();
          } else {
            followerssnap = {}
          }
          friends = Object.assign(followingsnap, followerssnap);
          return snap
        })
    })
    .then((value) => {
      Object.keys(friends).map((key) => {
        keyList.push(key)
      });
      return value
    })
    .then((value) => {
      return database.ref(rootRef)
        .once('value')
        .then(snap => {
          let backpackers = snap.val();
          Object.keys(backpackers).map((key) => {
            if (key in friends) {
              // do nothing
            } else {
              delete backpackers[key]
            }
          });
          friendsFiltered = backpackers;
          return snap
        })
    })
    .then((value) => {
      res.send(friendsFiltered)
    })
})


exports.getNonFriends = functions.https.onRequest((req, res) => {
  // Target UID of the notification.
  const touid = req.query.touid;
  const database = admin.database();
  let rootRef = 'users/';
  let followingRef = 'users/' + touid + '/connections/following';
  let followersRef = 'users/' + touid + '/connections/followers';
  var followingsnap = null;
  var followerssnap = null;
  var friends = null;
  var notFriends = null;

  database.ref(followingRef)
    .once('value')
    .then(snap => {
      if (snap.val() != null) {
        followingsnap = snap.val();
      } else {
        followingsnap = {}
      }
      return snap;
    })
    .then((value) => {
      return database.ref(followersRef)
        .once('value')
        .then(snap => {
          if (snap.val() != null) {
            followerssnap = snap.val();
          } else {
            followerssnap = {}
          }
          let mergedObj = Object.assign(followingsnap, followerssnap);
          friends = mergedObj;
          return snap
        })
    })
    .then((value) => {
      return database.ref(rootRef)
        .once('value')
        .then(snap => {
          let backpackers = snap.val();
          Object.keys(backpackers).map((key) => {
            if (key in friends) {
              delete backpackers[key]
            }
          });
          delete backpackers[touid]; // Remove client uid
          notFriends = backpackers;
          return snap
        })
    })
    .then((value) => {
      res.send(notFriends)
    })
})

// Prototype for cleaning array.
Array.prototype.clean = function(deleteValue) {
  for (var i = 0; i < this.length; i++) {
    if (this[i] == deleteValue) {
      this.splice(i, 1);
      i--;
    }
  }
  return this;
};

// Retrieves the tracking coordinates of the requested backpacker.
// Required URL parameter: uid.
// Return: Array of coordinates in JSON form.
exports.getTrackingCoordinates = functions.https.onRequest((req, res) => {
  const uid = req.query.uid;
  const database = admin.database();
  let trackingRef = 'users/' + uid + '/trackingCoordinates';
  let trackingSourceRef = 'users/' + uid + '/lastPosition';
  let trackingData = [];

  database.ref(trackingRef).once('value').then((snap) => {
      trackingData = snap.val();
      console.log("==========================================");
      console.log(trackingData);
      return snap;
    })
    .then((value) => {
      return database.ref(trackingSourceRef).once('value').then((snap) => {
        if(snap.val()!==null){
          trackingData.unshift(snap.val())
        }
        return snap;
      })
    })
    .then((value) => {
      // Clean null entries
      res.send(trackingData.clean(null))
    })
    .catch((error) => {
      console.log(error);
    })
})


exports.getAccommodation = functions.https.onRequest((req, res) => {
  const database = admin.database();
  let placesRef = 'places/';

  database.ref(placesRef).orderByChild('type').equalTo('accommodation')
    .once('value')
    .then(snap => {
      res.send(snap.val())
    }).catch((err) => {
      res.send(err)
    })
})

exports.getAttraction = functions.https.onRequest((req, res) => {
  const database = admin.database();
  let placesRef = 'places/';

  database.ref(placesRef).orderByChild('type').equalTo('attraction')
    .once('value')
    .then(snap => {
      res.send(snap.val())
    }).catch((err) => {
      res.send(err)
    })
})

exports.getFeed = functions.https.onRequest((req, res) => {
  const database = admin.database();
  const rootRef = database.ref();
  const placesRef = database.ref('places/')
  const feedRef = database.ref('feed/').orderByChild('timestamp')
  const usersRef = database.ref('users/')
  var joinedData = [];
  feedRef
    .once('value')
    .then((snap) => {

      let data = snap.val();

      async.each(Object.keys(data), (key, callback) => {
        usersRef.child(data[key].author).once('value').then((userSnap) => {
          placesRef.child(data[key].placeid).once('value').then((placeSnap) => {
            joinedData.push(Object.assign(data[key], {
              authorDetails: userSnap.val()
            }, {
              placeDetails: placeSnap.val()
            }, {
              feedid: key
            }));
            callback()
          }).catch((err) => {
            callback(err)
          })
        })
      }, (err) => {
        if (err) {
          res.send(err)
        } else {
          res.send(joinedData)
        }
      })
    })
})

// Minified users node. This is used to retrieve only the needed information
// for displaying all the backpackers in the new message screen.
exports.getBackpackers = functions.https.onRequest((req, res) => {
  const database = admin.database();
  const rootRef = database.ref();
  const usersRef = database.ref('users/');
  var joinedData = [];
  usersRef
    .once('value')
    .then((snap) => {
      let data = snap.val();
      async.each(Object.keys(data), (key, callback) => {
        delete data[key]["connections"];
        delete data[key]["birthday"];
        delete data[key]["nationality"];
        delete data[key]["feed"];
        delete data[key]["lastPosition"];
        delete data[key]["notifications"];
        delete data[key]["trackingCoordinates"];
        delete data[key]["messages"];
        joinedData.push(Object.assign(data[key], {
          "id": key
        }));
        callback();
      }, (err) => {
        if (err) {
          res.send(err)
        } else {
          res.send(joinedData)
        }
      })
    })
})

exports.getAllMessagesFromChannel = functions.https.onRequest((req, res) => {
  const database = admin.database();
  const usersRef = database.ref('users/');
  const fromuid = req.query.fromuid;
  const channel = req.query.channel;
  const messagesRef = database.ref('messages/');

  messagesRef.child(channel).once('value').then((messageSnap) => {
    let messageData = messageSnap.val()

    async.map(messageData, (item, callback) => {
      usersRef.child(item.user).once('value').then((userSnap) => {
        let userSnapData = userSnap.val();
        delete userSnapData["connections"];
        delete userSnapData["birthday"];
        delete userSnapData["nationality"];
        delete userSnapData["feed"];
        delete userSnapData["lastPosition"];
        delete userSnapData["notifications"];
        delete userSnapData["trackingCoordinates"];
        delete userSnapData["messages"];
        callback(null, Object.assign(item, {
          user: Object.assign(userSnapData, {_id:item.user, name:userSnapData.displayName, avatar: userSnapData.photoUrl}), _id: Math.round(Math.random() * 1000000), target:userSnapData.target
        }))
      })
    }, (e, r) => {
      res.send(r)
    })
  })
})


exports.getAllMessageChannelsFromClient = functions.https.onRequest((req, res) => {
  const database = admin.database();
  const usersRef = database.ref('users/');
  const fromuid = req.query.fromuid;
  const messagesRef = database.ref('messages/');
  var joinedData = [];
  usersRef.child(fromuid).child("messages").once('value').then((messageBucketSnap) => {
    async.each(Object.keys(messageBucketSnap.val()), (key, callback) => {
      messagesRef.child(key).once("value").then((messagesSnap) => {
        let data = messagesSnap.val();
        let userKey = data[Object.keys(data)[0]].user;
        usersRef.child(userKey).once('value').then((userSnap) => {
          let userSnapData = userSnap.val();
          console.log(userSnapData);
          delete userSnapData["connections"];
          delete userSnapData["birthday"];
          delete userSnapData["nationality"];
          delete userSnapData["feed"];
          delete userSnapData["lastPosition"];
          delete userSnapData["notifications"];
          delete userSnapData["trackingCoordinates"];
          delete userSnapData["messages"];
          joinedData.push(Object.assign(data[Object.keys(data)[0]], {
            user: userSnapData, channel: key
          }));
          return userSnap;
        }).then(() => {
          callback()
        })
      })
    }, (err) => {
      if (err) {
        res.send(err)
      } else {
        res.send(joinedData)
      }
    })
  })
})
