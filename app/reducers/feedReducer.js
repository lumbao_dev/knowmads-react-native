import * as types from '../actions/types'

const feed = (state = {
    feed: null,
    error: null
}, action) => {
	switch (action.type) {
		case types.FETCH_FEED_FULFILLED:
			{
				return { ...state,
					feed: action.payload
				}
			}
		case types.FETCH_FEED_FAILED:
			{
				return { ...state,
					error: action.payload
				}
			}
		default:
			return state
	}
}

export default feed