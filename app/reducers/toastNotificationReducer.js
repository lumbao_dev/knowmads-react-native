import * as types from '../actions/types'

const toastNotification = (state = {
  message: '',
}, action) => {
	switch (action.type) {
		case types.TOAST_NOTIFICATION:
			{
				return { ...state,
					message: action.payload
				}
			}
		default:
			return state
	}
}

export default toastNotification
