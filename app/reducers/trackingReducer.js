import * as types from '../actions/types'

const tracker = (state = {
  trackingCoordinates: null,
}, action) => {
	switch (action.type) {
		case types.FETCH_TRACKS_FULFILLED:
			{
				return { ...state,
					trackingCoordinates: action.payload
				}
			}
		case types.FETCH_TRACKS_FAILED:
			{
				return { ...state,
					trackingCoordinates: action.payload
				}
			}
		default:
			return state
	}
}

export default tracker
