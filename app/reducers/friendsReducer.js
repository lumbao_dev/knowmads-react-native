import * as types from '../actions/types'

const friends = (state = {
	friends: null,
	nonFriends: null
}, action) => {
	switch (action.type) {
		case types.FETCH_FRIENDS_FULFILLED:
			{
				return { ...state,
					friends: action.payload
				}
			}
		case types.FETCH_FRIENDS_FAILED:
		{
			return {
				...state,
				error: action.payload
			}
		}
    case types.FETCH_NON_FRIENDS_FULFILLED:
    {
      return { ...state,
        nonFriends: action.payload
      }
    }
    case types.FETCH_NON_FRIENDS_FAILED:
		{
			return {
				...state,
				error: action.payload
			}
		}
		default:
			return state
	}
}

export default friends
