import * as types from '../actions/types'

const mapAction = (state = {
  addAccommodation: false,
  addAttraction: false,
  accommodationSubmitted: false,
  attractionSubmitted: false,
  processingMapRequest: false
}, action) => {
	switch (action.type) {
		case types.ADD_ACCOMMODATION:
			{
				return { ...state,
					addAccommodation: action.payload
				}
			}
		case types.ADD_ATTRACTION:
			{
				return { ...state,
					attractionSubmitted: action.payload
				}
			}
		case types.ADD_ACCOMMODATION_FULFILLED:
			{
				return { ...state,
						accommodationSubmitted: action.payload
				}
			}
		case types.PROCESSING_MAP_REQUEST:
		{
			return { ...state,
					processingMapRequest: action.payload
			}			
		}

		default:
			return state
	}
}

export default mapAction
