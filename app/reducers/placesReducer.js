import * as types from '../actions/types'

const places = (state = {
	accommodations: null,
	attractions: null,
    error: null
}, action) => {
	switch (action.type) {
		case types.FETCH_ACCOMMODATIONS_FULFILLED:
			{
				return { ...state,
					accommodations: action.payload
				}
			}
		case types.FETCH_ACCOMMODATIONS_FAILED:
		{
			return {
				...state,
				error: action.payload
			}
		}
    case types.FETCH_ATTRACTIONS_FULFILLED:
    {
      return { ...state,
        attractions: action.payload
      }
    }
    case types.FETCH_ACCOMMODATIONS_FAILED:
		{
			return {
				...state,
				error: action.payload
			}
		}
		default:
			return state
	}
}

export default places
