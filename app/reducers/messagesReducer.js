import * as types from '../actions/types'

const messages = (state = {
    messages: null,
    messagesFromChannel: [],
    error: null
}, action) => {
	switch (action.type) {
		case types.FETCH_MESSAGES_FULFILLED:
			{
				return { ...state,
					messages: action.payload
				}
			}
		case types.FETCH_MESSAGES_FAILED:
			{
				return { ...state,
					error: action.payload
				}
			}
      case types.FETCH_MESSAGES_FROM_CHANNEL_FULFILLED:
  			{
  				return { ...state,
  					messagesFromChannel: action.payload
  				}
  			}
  		case types.FETCH_MESSAGES_FROM_CHANNEL_FAILED:
  			{
  				return { ...state,
  					error: action.payload
  				}
  			}
		default:
			return state
	}
}

export default messages
