import {combineReducers} from "redux"
import user from "./userReducer"
import mapSettings from "./mapSettingReducer"
import userConnections from "./userConnectionsReducer"
import mapAction from "./mapActionPanelReducer"
import friends from "./friendsReducer"
import tracker from "./trackingReducer"
import toastNotification from "./toastNotificationReducer"
import places from "./placesReducer"
import feed from "./feedReducer"
import backpackers from "./backpackerReducer"
import messages from "./messagesReducer"

export default combineReducers({
	user,
	mapSettings,
	userConnections,
	mapAction,
	friends,
	tracker,
	toastNotification,
	places,
	feed,
	backpackers,
	messages
})
