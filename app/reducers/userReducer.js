import * as types from '../actions/types'

const user = (state = {
  user: {
    uid: '',
    displayName: '',
    photoUrl: 'https://firebasestorage.googleapis.com/v0/b/carbon-syntax-155109.appspot.com/o/photos%2Fprofile%2Favatar-default.png?alt=media&token=aaf56f25-3f4e-4728-ad06-ac87d4496ca8',
    email: '',
    nationality: ''
  },
  fetching: false,
  fetched: false,
  error: null
}, action) => {
  switch (action.type) {
    case types.FETCH_USER:
      {
        return { ...state,
          fetching: true
        }
      }
    case types.FETCH_USER_REJECTED:
      {
        return { ...state,
          fetching: false,
          error: action.payload
        }
      }
    case types.FETCH_USER_FULFILLED:
      {
        return {
          ...state,
          fetching: false,
          fetched: true,
          user: action.payload
        }
      }
      case types.SET_USER:
      {
        return {
          ...state,
          user: action.payload
        }
      }
      default:
        return state
  }
}

export default user
