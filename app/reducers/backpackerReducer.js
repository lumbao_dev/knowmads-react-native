import * as types from '../actions/types'

const backpackers = (state = {
    backpackers: null,
    error: null
}, action) => {
	switch (action.type) {
		case types.FETCH_BACKPACKERS_FULFILLED:
			{
				return { ...state,
					backpackers: action.payload
				}
			}
		case types.FETCH_BACKPACKERS_FAILED:
			{
				return { ...state,
					error: action.payload
				}
			}
		default:
			return state
	}
}

export default backpackers
