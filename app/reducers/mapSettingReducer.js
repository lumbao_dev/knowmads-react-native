import * as types from '../actions/types'

const mapSettings = (state = {
	showOwnerLocation: true,
	showPublicUsers: false,
	showPrivateUsers: false,
	showAccommodations: false,
	showAttractions: false
}, action) => {
	switch (action.type) {
		case types.SHOW_CLIENT_LOCATION:
			{
				return { ...state,
					showOwnerLocation: action.payload
				}
			}
		case types.SHOW_PUBLIC_USERS:
			{
				return { ...state,
					showPublicUsers: action.payload
				}
			}
		case types.SHOW_PRIVATE_USERS:
			{
				return { ...state,
					showPrivateUsers: action.payload
				}
			}
		case types.SHOW_ACCOMMODATIONS:
			{
				return { ...state,
					showAccommodations: action.payload
				}
			}
		case types.SHOW_ATTRACTIONS:
			{
				return { ...state,
					showAttractions: action.payload
				}
			}
		case types.RESET_MAP_FILTERS:
			{
				return {...state,
					showOwnerLocation: true,
					showPublicUsers: true,
					showPrivateUsers: false,
					showAccommodations: false,
					showAttractions: false				
				}
			}
		default:
			return state
	}
}

export default mapSettings