import * as types from '../actions/types'

const userConnections = (state = {
	userConnections: {},
	error: null
}, action) => {
	switch (action.type) {
		case types.FETCH_USER_CONNECTIONS_FULFILLED:
			{
				return { ...state,
					userConnections: action.payload
				}
			}
		case types.FETCH_USER_CONNECTIONS_REJECTED:
		{
			return {
				...state,
				error: action.payload
			}
		}
		default:
			return state
	}
}

export default userConnections
