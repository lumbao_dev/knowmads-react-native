import {applyMiddleware, createStore, compose} from "redux"
import {createLogger} from "redux-logger"
import thunk from "redux-thunk"
import promise from "redux-promise-middleware"
import initSubscriber from 'redux-subscriber'
import {autoRehydrate} from 'redux-persist'

import reducer from "../reducers/"

const logger = createLogger({ predicate: (getState, action) => __DEV__  });

function configureStore(initialState) {
  const enhancer = compose(
    applyMiddleware(
      promise(),
      thunk,
      logger
    ),
    autoRehydrate()
  );
  return createStore(reducer, initialState, enhancer);
}

const store = configureStore({});

const subscribe = initSubscriber(store);

module.exports = store;
