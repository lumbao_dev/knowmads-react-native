// Local API for Knowmads
// ----------------

// This API includes all the global methods that can be used on any parts of the app.
import RNFirebase from 'react-native-firebase'
//import Firestack from 'react-native-firestack'

import React, { Component, PropTypes } from 'react'

import {
  Image,
  StyleSheet,
  Navigator,
  View,
  Text,
  Alert,
  AsyncStorage,
  ToastAndroid
} from 'react-native'

import MapView from 'react-native-maps'

import NotificationItem from '../components/NotificationItem/'

import OnButtonPress from '../components/OnButtonPress/'

const styles = require('../components/GlobalStyles/Main.js')

const colors = require('../components/GlobalStyles/ColorPalette.js')

import StarRatingStatic from '../components/StarRatingStatic/'

import { Actions, ActionConst } from 'react-native-router-flux'

//const firestack = new Firestack();

const configurationOptions = {
  debug: false
}

firebase = RNFirebase.initializeApp(configurationOptions);

export default class API {

  static backpackers = () => {
    return firebase.database().ref('/users')
  }

  static backpackerConnections = (uid) => {
    return firebase.database().ref('/users/' + uid + '/connections')
  }

  static backpacker = (uid) => {
    return firebase.database().ref('/users/' + uid)
  }

  static backpackerPendingFollowRequest = (uid) => {
    return firebase.database().ref('/users/' + uid + '/connections/pending')
  }

  // Profile notifications contain only two types as of now, tracker and follow requests.
  static backpackerProfileNotifications = (uid) => {
    let ref = '/users/' + uid + '/notifications';
    return firebase.database().ref(ref);
  }

  static updateViewCount(feedid, uid){
    let updateString = '{"'+uid+'":'+'true'+'}'
    firebase.database().ref('feed/'+feedid+'/views').update(JSON.parse(updateString));
  }

  static submitPhotoToFeed = (path, author, props, placeid) => {
    let newPostKey;
    let updates = {};
    var constantKey;
    props.processingMapRequest(true);
        return firebase.database().ref().push().then((res) => {
          firestack.ServerValue.then(map => {
            newPostKey = res.key;
            constantKey = newPostKey;
            const postData = {
              type: 'photo',
              photoUrl: '',
              author: author,
              placeid: placeid,
              timestamp: map.TIMESTAMP,
              views: 0
            }
            updates['/feed/' + newPostKey] = postData;
            firebase.database().ref().update(updates)
          })
          .then(()=>{
              firestack.storage.uploadFile('photos/places/' + newPostKey, path, {
                contentType: 'image/jpeg',
                contentEncoding: 'base64'
              }).then((res) => {
                  const storageRef = firestack.storage.ref('photos/places/' + newPostKey);
                  return storageRef.downloadUrl().then((res_from_storage) => {
                    return Promise.all(
                      firebase.database().ref("feed/"+newPostKey).update({photoUrl: res_from_storage.fullPath}),
                      firebase.database().ref('users/' + author).child('feed').push([newPostKey])
                    )
                  })
                .then(()=>{
                  ToastAndroid.show('Your photo is now posted!', ToastAndroid.SHORT);
                  props.processingMapRequest(false);
                })
              })
          })
          .catch((err) => {
              ToastAndroid.show('Sorry. An error occured!', ToastAndroid.SHORT);
              props.processingMapRequest(false);
          });
        })
  }

  static submitCheckinToFeed = (author, placeid, props) => {
    props.processingMapRequest(true);
    return firebase.database().ref().push().then((res) => {

      let newPostKey;
      let updates = {};
      var constantKey;
      firestack.ServerValue.then(map => {
        newPostKey = res.key;
        constantKey = newPostKey;
        const postData = {
          type: 'checkin',
          placeid: placeid,
          author: author,
          timestamp: map.TIMESTAMP,
          views: 0
        }

        updates['/feed/' + newPostKey] = postData;

        return firebase.database().ref().update(updates)
      }).then(() => {
        return firebase.database().ref('users/' + author).child('feed').push([constantKey])
      }).then(() => {
        return firebase.database().ref('users/' + author).child('trackingCoordinates/' + constantKey).update(placeid).then(() => {
          ToastAndroid.show('Your status is now updated!', ToastAndroid.SHORT);
        })
      }).catch((err) => {
        console.log(err);
      })
    });
  }

  static unFollowBackpacker(clientUid, displayName, targetUid) {
    Alert.alert('Knowmads', "Unfollow " + displayName + "?", [
      {
        text: 'Cancel',
        onPress: () => { }
      }, {
        text: 'Unfollow',
        onPress: () => { }
      }
    ]);
  };

  static updateBackpackerNotification(targetUid, clientUid, type, data) {
    let ref = 'users/' + targetUid + '/notifications/';

    // NOTE: Switch is not recommended for use here.

    // Client sends a follow request to a backpacker.
    if (type == 'follow') {
      firebase.database().ref(ref).push().then((res) => {
        let newPostKey = res.key;
        firestack.ServerValue.then(map => {
          const postData = {
            type: 'follow',
            timestamp: map.TIMESTAMP,
            followSourceUid: clientUid,
            reviewed: false,
            approved: false
          }
          let updates = {}
          updates[ref + newPostKey] = postData;
          firebase.database().ref().update(updates).then(() => {
            console.log('Notification Updated');
          }).catch((error) => {
            console.log(error.message);
          });
        })
      })
    }

    /*
  * TODO: Create an if condition for the tracker request
  */

    /*
  * TODO: Create an if condition for other requests
  */
  }

  static resetMapFilters(props) {
    let defaultSettings = [
      false,
      false,
      true,
      true,
      false,
      false
    ];

    AsyncStorage.setItem("showAccommodations", JSON.stringify(defaultSettings[0]));
    AsyncStorage.setItem("showAttractions", JSON.stringify(defaultSettings[1]));
    AsyncStorage.setItem("showOwnerLocation", JSON.stringify(defaultSettings[2]));
    AsyncStorage.setItem("showPublicUsers", JSON.stringify(defaultSettings[3]));
    AsyncStorage.setItem("showPrivateUsers", JSON.stringify(defaultSettings[4]));

    console.log(props);
  }

  static followBackpacker(targetUid, clientUid) {
    let ref = 'users/' + targetUid + '/connections/pending/';
    let update = JSON.parse('{"' + clientUid + '":true' + '}');
    //console.log('{"'+clientUid+'":true'+'}');

    firebase.database().ref(ref).update(update).then(() => {
      Alert.alert('Knowmads', 'Follow request sent!')
    }).catch((error) => {
      console.log(error.message);
    });
  }

  static calculateStars = (arrayObj) => {
    let count = Object.keys(arrayObj).length;
    let curr = 0;
    let max = 0;
    Object.keys(arrayObj).map(function (key) {
      //console.log(arrayObj[key]);
      curr += 1;
      max += 1
    });
    max *= 5;
    return Math.floor(((curr / max) * 50 + 50) * 0.05)
  }

  static getBackpackerComponents = (target, data, active_user, active_user_connections) => {
    var components = [];
    var color = "#31C593";
    var following = false; // Determine if the current backpacker is following the user. This will be passed on the props.
    var coordinate = null;
    if (typeof data != 'undefined' && data !== null) {
      Object.keys(data).map((key) => {
        if (typeof key != 'undefined') {

          if (typeof data[key].lastPosition.latitude != 'undefined' || typeof data[key].lastPosition.longitude != 'undefined') {
            coordinate = {
              latitude: data[key].lastPosition.latitude,
              longitude: data[key].lastPosition.longitude
            }
          } else {
            coordinate = {
              latitude: 0,
              longitude: 0
            }
          }

          // Check if the user is following the backpacker then change the outline color (Public or Private).
          if (active_user_connections == null) {
            color = colors.Aquamarine
          } else {
            if (typeof active_user_connections.following != 'undefined') {
              if (key in active_user_connections.following) {
                console.log("Key is here...");
                color = colors.Aquamarine;
                following = true;
              } else {
                color = colors.Crimson;
                following = false;
              }
            } else {
              color = colors.Crimson;
              following = false;
            }
          }

          let followersCounter = 0;
          let followingCounter = 0;

          if (typeof data[key].connections != 'undefined') {
            if (typeof data[key].connections.followers != 'undefined') {
              followersCounter = Object.keys(data[key].connections.followers).length
            }
          }

          if (typeof data[key].connections != 'undefined') {
            if (typeof data[key].connections.following != 'undefined') {
              followingCounter = Object.keys(data[key].connections.following).length
            }
          }

          if (key.toString() != active_user) {
            components.push(
              <MapView.Marker key={key.toString() + '_' + Date.now()} coordinate={coordinate} title={""} description={""}>
                <Image style={[
                  styles.pointMarkers, {
                    borderColor: color
                  }
                ]} source={{
                  uri: data[key].photoUrl
                }}></Image>
                <MapView.Callout style={styles.pointMarkerCallout}>
                  <View style={{
                    flex: 1,
                    flexDirection: 'row',
                    alignItems: 'center'
                  }}>
                    <View style={styles.avatarContainerMap}>
                      <Image source={{
                        uri: data[key].photoUrl
                      }} style={styles.markerAvatar} />
                    </View>
                    <View style={{
                      flexDirection: 'column'
                    }}>
                      <Text style={styles.markerText} ellipsizeMode="tail">{data[key].displayName}</Text>
                      <View style={{
                        flexDirection: 'row',
                        paddingLeft: 10
                      }}>
                        <Text style={styles.followerText}>{followingCounter + " Following"}</Text>
                        <Text style={styles.followerText}>{followersCounter + " Followers"}</Text>
                      </View>
                    </View>
                  </View>
                </MapView.Callout>
              </MapView.Marker>
            )
          }
        }
      });
    }

    return (components);
  }

  static setFriendMarkers(data) {
    let components = [];
    if (data != null) {
      Object.keys(data).map((key) => {
        if (typeof data[key] != 'undefined') {
          if (typeof data[key].lastPosition.latitude != 'undefined' || typeof data[key].lastPosition.longitude != 'undefined') {
            coordinate = {
              latitude: data[key].lastPosition.latitude,
              longitude: data[key].lastPosition.longitude
            }
          } else {
            coordinate = {
              latitude: 0,
              longitude: 0
            }
          }

          let followersCounter = 0;
          let followingCounter = 0;

          if (typeof data[key].connections != 'undefined') {
            if (typeof data[key].connections.followers != 'undefined') {
              followersCounter = Object.keys(data[key].connections.followers).length
            }
          }

          if (typeof data[key].connections != 'undefined') {
            if (typeof data[key].connections.following != 'undefined') {
              followingCounter = Object.keys(data[key].connections.following).length
            }
          }
          components.push(
            <MapView.Marker key={key.toString() + '_' + Date.now()} coordinate={coordinate} title={""} description={""}>
              <Image style={[
                styles.pointMarkers, {
                  borderColor: colors.Aquamarine
                }
              ]} source={{
                uri: data[key].photoUrl
              }}></Image>
              <MapView.Callout style={styles.pointMarkerCallout} onPress={() => Actions.previewProfile({ previewProfileData: data[key], previewProfileType: 'public', previewProfileKey: key })}>
                <View style={{
                  flex: 1,
                  flexDirection: 'row',
                  alignItems: 'center'
                }}>
                  <View style={styles.avatarContainerMap}>
                    <Image source={{
                      uri: data[key].photoUrl
                    }} style={styles.markerAvatar} />
                  </View>
                  <View style={{
                    flexDirection: 'column'
                  }}>
                    <Text style={styles.markerText} ellipsizeMode="tail">{data[key].displayName}</Text>
                    <View style={{
                      flexDirection: 'row',
                      paddingLeft: 10
                    }}>
                      <Text style={styles.followerText}>{followingCounter + " Following"}</Text>
                      <Text style={styles.followerText}>{followersCounter + " Followers"}</Text>
                    </View>
                  </View>
                </View>
              </MapView.Callout>
            </MapView.Marker>
          )

        }
      })
    }
    return components
  }

  static setNonFriendMarkers(data) {
    let components = [];
    console.log("======================setNonFriendMarkers============================");
    console.log(data);
    console.log("======================/setNonFriendMarkers============================");
    if (data !== null && data !== undefined) {
      Object.keys(data).map((key) => {
        if (data[key] !== undefined) {
          console.log(data[key].lastPosition);
          const lat = data[key].lastPosition.latitude;
          const lng = data[key].lastPosition.longitude;
          if ( lat !== undefined || lng !== undefined) {
            coordinate = {
              latitude: lat,
              longitude: lng
            }
          } else {
            coordinate = {
              latitude: 0,
              longitude: 0
            }
          }

          let followersCounter = 0;
          let followingCounter = 0;

          if (typeof data[key].connections != 'undefined') {
            if (typeof data[key].connections.followers != 'undefined') {
              followersCounter = Object.keys(data[key].connections.followers).length
            }
          }

          if (typeof data[key].connections != 'undefined') {
            if (typeof data[key].connections.following != 'undefined') {
              followingCounter = Object.keys(data[key].connections.following).length
            }
          }
          components.push(
            <MapView.Marker key={key.toString() + '_' + Date.now()} coordinate={coordinate} title={""} description={""}>
              <Image style={[
                styles.pointMarkers, {
                  borderColor: colors.Crimson
                }
              ]} source={{
                uri: data[key].photoUrl
              }}></Image>
              <MapView.Callout style={styles.pointMarkerCallout} onPress={() => Actions.previewProfile({ previewProfileData: data[key], previewProfileType: 'private', previewProfileKey: key })}>
                <View style={{
                  flex: 1,
                  flexDirection: 'row',
                  alignItems: 'center'
                }}>
                  <View style={styles.avatarContainerMap}>
                    <Image source={{
                      uri: data[key].photoUrl
                    }} style={styles.markerAvatar} />
                  </View>
                  <View style={{
                    flexDirection: 'column'
                  }}>
                    <Text style={styles.markerText} ellipsizeMode="tail">{data[key].displayName}</Text>
                    <View style={{
                      flexDirection: 'row',
                      paddingLeft: 10
                    }}>
                      <Text style={styles.followerText}>{followingCounter + " Following"}</Text>
                      <Text style={styles.followerText}>{followersCounter + " Followers"}</Text>
                    </View>
                  </View>
                </View>
              </MapView.Callout>
            </MapView.Marker>
          )

        }
      })
    }
    return components
  }

  static setAccommodationMarkers(data) {
    let components = [];
    let starRating = [];
    if (data != null) {
      Object.keys(data).map((key) => {
        if (typeof data[key] != 'undefined') {
          if (typeof data[key].location.latitude != 'undefined' || typeof data[key].location.longitude != 'undefined') {
            coordinate = {
              latitude: data[key].location.latitude,
              longitude: data[key].location.longitude
            }
          } else {
            coordinate = {
              latitude: 0,
              longitude: 0
            }
          }
          if (data[key].rating != null) {
            starRating = (<StarRatingStatic stars={this.calculateStars(data[key].rating)} />)
          } else {
            starRating = (<StarRatingStatic stars={0} />)
          }

          components.push(
            <MapView.Marker key={key.toString() + '_' + Date.now()} coordinate={coordinate} title={""} description={""}>
              <Image style={{ width: 20, height: 20 }} source={require('../assets/accommodation-marker.png')}></Image>
              <MapView.Callout style={styles.pointMarkerCallout} onPress={() => Actions.previewPlace({ previewPlaceData: data[key], previewPlaceKey: key })}>
                <View style={{
                  flex: 1,
                  flexDirection: 'row',
                  alignItems: 'center'
                }}>
                  <View style={styles.avatarContainerMap}>
                    <Image source={{
                      uri: data[key].featureImage
                    }} style={styles.markerAvatar} />
                  </View>
                  <View style={{
                    flexDirection: 'column'
                  }}>
                    <Text style={styles.markerText} ellipsizeMode="tail">{data[key].displayName}</Text>
                    <View style={{
                      flexDirection: 'row',
                      paddingLeft: 10
                    }}>
                      {starRating}
                    </View>
                  </View>
                </View>
              </MapView.Callout>
            </MapView.Marker>
          )
        }
      })
    }
    //console.log(data);
    return components
  }

  static setAttractionMarkers(data) {
    let components = [];
    if (data !== null) {
      Object.keys(data).map((key) => {
        if (typeof data[key] != 'undefined') {
          if (typeof data[key].location.latitude != 'undefined' || typeof data[key].location.longitude != 'undefined') {
            coordinate = {
              latitude: data[key].location.latitude,
              longitude: data[key].location.longitude
            }
          } else {
            coordinate = {
              latitude: 0,
              longitude: 0
            }
          }

          components.push(
            <MapView.Marker key={key.toString() + '_' + Date.now()} coordinate={coordinate} title={""} description={""}>
              <Image style={{ width: 20, height: 20 }} source={require('../assets/attraction-marker.png')}></Image>
              <MapView.Callout style={styles.pointMarkerCallout} onPress={() => Actions.previewPlace({ previewPlaceData: data[key], previewPlaceKey: key })}>
                <View style={{
                  flex: 1,
                  flexDirection: 'row',
                  alignItems: 'center'
                }}>
                  <View style={styles.avatarContainerMap}>
                    <Image source={{
                      uri: data[key].featureImage
                    }} style={styles.markerAvatar} />
                  </View>
                  <View style={{
                    flexDirection: 'column'
                  }}>
                    <Text style={styles.markerText} ellipsizeMode="tail">{data[key].displayName}</Text>
                    <View style={{
                      flexDirection: 'row',
                      paddingLeft: 10
                    }}>
                      <StarRatingStatic stars={4} />
                    </View>
                  </View>
                </View>
              </MapView.Callout>
            </MapView.Marker>
          )
        }
      })
    }
    return components
  }

  static getNotificationComponents = (active_user, data) => {
    var components = [];
    // console.log(JSON.stringify(data));
    if (active_user != null) {
      let uid = active_user.uid;
      if (typeof data != 'undefined' && data !== null) {
        Object.keys(data).map((key) => {
          components.push(<NotificationItem key={"_" + key} notifId={key} approved={data[key].approved} fromUid={data[key].followSourceUid} toUid={uid} type={data[key].type} timestamp={data[key].timestamp} />)
        })
      }
    }
    return (components)
  }

  static signOut = () => {
    Alert.alert('Knowmads', "Sign out of Knowmads?", [
      {
        text: 'Cancel',
        onPress: () => { }
      }, {
        text: 'Sign Out',
        onPress: () => {
          /** FIXME: Bug here. Maybe causing the random crash.
              */
          firestack.auth.signOut().then(() => {
            console.log('You have been signed out');
            Actions.auth;
          }).catch(err => console.error('Uh oh... something weird happened'))
        }
      }
    ]);
  }

  static createReducer = (initialState, handlers) => {
    return function reducer(state = initialState, action) {
      if (handlers.hasOwnProperty(action.type)) {
        return handlers[action.type](state, action)
      } else {
        return state
      }
    }
  }

  static async submitFollowRequest(fromUid, toUid, notification_key, action) {
    try {
      let response = await fetch('https://us-central1-carbon-syntax-155109.cloudfunctions.net/submitFollowRequest?touid=' + toUid + '&notifkey=' + notification_key + '&action=' + action);
      // let responseJson = await response.json();
      let responseJson = await response.json();
      console.log(responseJson);
    } catch (error) {
      console.log(error)
    }
  }

  static uuid() {
      var i, random;
      var uuid = '';
      for (i = 0; i < 32; i++) {
          random = Math.random() * 16 | 0;
          if (i === 8 || i === 12 || i === 16 || i === 20) {
              uuid += '-';
          }

          uuid += (i === 12 ? 4 : (i === 16 ? (random & 3 | 8) : random)).toString(16);
      }

      return uuid;
  }

  static submitMessage = (data) => {
    let updates = {};
    return firebase.database().ref().push().then((res) => {
      let messageKey = res.key;
      firestack.ServerValue.then(map => {
        newPostKey = res.key;
        constantKey = newPostKey;
        const postData = true;
        updates[data.user+'/messages/' + newPostKey] = postData;
        firebase.database().ref().update(updates)
      })
    })
  }
}
