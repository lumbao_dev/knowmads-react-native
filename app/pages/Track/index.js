import React, {Component, PropTypes} from 'react'
import {View, Text, StyleSheet, Image, TouchableOpacity} from 'react-native'
import {Actions} from 'react-native-router-flux'
import MapView from 'react-native-maps'
import {connect} from 'react-redux'
import {setTrackingCoordinates} from '../../actions/trackingActions'
import API from '../../api'
const colors = require('../../components/GlobalStyles/ColorPalette.js')
const styles = require('./style.js')


var _mapView: MapView;

@connect((store) => {
  return {user: store.user.user, trackingCoordinates: store.tracker.trackingCoordinates}
}, (dispatch) => {
  return {
    getTrackingCoordinates: (uid) => {
      dispatch(setTrackingCoordinates(uid))
    }
  }
})

// function getCenterOffsetForAnchor(anchor, markerWidth, markerHeight) {
//    return {
//      x: (markerWidth * 0.5) - (markerWidth * anchor.x),
//      y: (markerHeight * 0.5) - (markerHeight * anchor.y),
//    };
//  }

export default class Track extends Component {

  constructor(props) {
    super(props);

    this.state = {
			initialRender: true,

    }

    this.trackingData = [];

    this.trackingCoordinates = [];

    this.trackingCoordinatesRootLength = 0;

    this.coordinateTarget = {
      "latitude": 0,
      "longitude": 0
    };

  }

	animateCoordinate(coordinate){
		this.map.animateToCoordinate(coordinate, 1000);
	}

  componentWillMount() {
    this.props.getTrackingCoordinates(this.props.backpackerKey);
  }

  componentDidMount() {
    const app = this;
    let data = this.props.trackingCoordinates;

    //Clean up
    data.forEach((item) => {
      let coordinates = {
        latitude: item.latitude,
        longitude: item.longitude
      }
      this.trackingCoordinates.push(coordinates)
    })

    this.trackingCoordinatesRootLength = this.trackingCoordinates.length - 1;
    this.coordinateTarget = this.trackingCoordinates[this.trackingCoordinatesRootLength];
  }

  render() {
    var trackingComponents = [];
    var trackerUIDComp = [];
    var trackerLastPos = [];

    trackingComponents.push(<MapView.Polyline key={"trackingLine"} coordinates={this.trackingCoordinates} strokeWidth={4} strokeColor={colors.Aquamarine}/>)

    trackerUIDComp.push(
      <MapView.Marker key={'trackedUid'} coordinate={{
        latitude: this.coordinateTarget.latitude,
        longitude: this.coordinateTarget.longitude
      }} title={""} description={""}>
        <Image style={[
          styles.pointMarkers, {
            borderColor: colors.Aquamarine
          }
        ]} source={{
          uri: this.props.data.photoUrl
        }}></Image>
      </MapView.Marker>
    )

    trackerLastPos.push(
      <MapView.Marker key={'lastPoint'} coordinate={{
        latitude: this.props.data.lastPosition.latitude,
        longitude: this.props.data.lastPosition.longitude
      }} title={""} description={""}>
        <Image
					style={styles.endMarker}
					source={require('../../assets/knowmad-public-marker-50x50.png')}
					onLayout={() => this.setState({ initialRender: false })}
          key={`${this.state.initialRender}`}
					></Image>
      </MapView.Marker>
    )

    return (
      <View style={{
        flex: 1
      }}>

        <MapView
					ref={ref => { this.map = ref; }}
					initialRegion={{
          latitude: this.props.data.lastPosition.latitude,
          longitude: this.props.data.lastPosition.longitude,
          latitudeDelta: 1,
          longitudeDelta: 1
        }} style={styles.map}
				>

          {trackingComponents}

          {trackerUIDComp}

          {trackerLastPos}

        </MapView>

				<TouchableOpacity style={styles.endFabFloating} onPress={() => this.animateCoordinate(
						{
							latitude: this.props.data.lastPosition.latitude,
		          longitude: this.props.data.lastPosition.longitude,
						}
					)}>
					<View style={styles.startFabIcon}>
						<Text>Origin</Text>
					</View>
				</TouchableOpacity>

				<TouchableOpacity style={styles.startFabFloating} onPress={() => this.animateCoordinate(
					{
						latitude: this.coordinateTarget.latitude,
						longitude: this.coordinateTarget.longitude
						}
					)}>
					<Image style={styles.fabFloatingIcon} source={{
	          uri: this.props.data.photoUrl
	        }} />
				</TouchableOpacity>

      </View>
    )
  }
}
