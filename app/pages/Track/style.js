import {StyleSheet} from 'react-native';
const colors = require('../../components/GlobalStyles/ColorPalette.js')
module.exports = StyleSheet.create({
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  pointMarkers: {
    width: 30,
    height: 30,
    borderRadius: 50,
    borderWidth: 2,
    borderColor:'#31C593'
  },
  endMarker:{
  	width: 20,
      height: 20,
  },
  startFabFloating: {
      backgroundColor: '#FFF',
      width: 60,
      height: 60,
      borderRadius: 50,
      margin: 20,
      alignItems: 'center',
      justifyContent: 'center',
      position: 'absolute',
      right: 5,
      bottom: 5
  },
  endFabFloating: {
      backgroundColor: '#FFF',
      width: 60,
      height: 60,
      borderRadius: 50,
      margin: 20,
      alignItems: 'center',
      justifyContent: 'center',
      position: 'absolute',
      right: 5,
      bottom: 80
  },
  fabFloatingIcon: {
      width: 50,
      height: 50,
      borderRadius: 25
  },
  startFabIcon: {
    width: 50,
    height: 50,
    borderRadius: 25,
    borderColor: colors.Aquamarine,
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 2
  }
})
