import React, {Component, PropTypes} from 'react';
// TODO: Organize imports to be aranged alphabetically
// TODO: Code is too dirty. Make components
import {
  StyleSheet,
  Text,
  View,
  Alert,
  Image,
  TextInput,
  ScrollView,
  TouchableOpacity,
  InteractionManager,
  AsyncStorage,
  ActivityIndicator
} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';
// import Firestack from 'react-native-firestack';
import OnButtonPress from '../../components/OnButtonPress/';
import {Actions, ActionConst} from 'react-native-router-flux';
import * as types from '../../actions/types';
import {connect} from 'react-redux';
// import OAuthManager from 'react-native-oauth';
const FBSDK = require('react-native-fbsdk');
const {LoginButton, LoginManager, GraphRequest, GraphRequestManager, AccessToken} = FBSDK;
// const firestack = new Firestack();

import RNFirebase from 'react-native-firebase'
const configurationOptions = {
    debug: false
  }

firebase = RNFirebase.initializeApp(configurationOptions);

// Redux actions
import {setMapUser} from '../../actions/userActions';

const styles = require('./style.js');
const reactMixin = require('react-mixin');



// const manager = new OAuthManager('facebook');

// manager.configure({
//   facebook: {
//     client_id: '1827471380829263',
//     client_secret: 'b06b8da5ecf7fbd61d6e36772dac6594'
//   }
// });

const infoRequest = new GraphRequest('/me', null, this._responseInfoCallback,);

const elipsisCounter = 0;
const timer = null;

@connect((store)=>{
  return {
    user: store.user.user
  }
}, (dispatch)=>{
  return {
    setUser: (user)=>{
      dispatch({
        type: types.SET_USER,
        payload: user
      })
    },
    // signInUser: (email,password) => {
    //   dispatch(signInUser(email, password))
    // }
  }
})

export default class Auth extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loaded: true,
      email: '',
      password: '',
      passwordInitShow: true,
      signedIn: false,
      renderPlaceholderOnly: true,
      submit: false,
      animatedEllipsis:'',
      signInText: 'Sign In',
      loading: false,
    }

    this.unsubscribe = null;
  }

  componentDidMount() {
    const app = this;
    //firebase.database().setPersistence(true);
    // timer = app.setInterval(
    //       () => {
    //         elipsisCounter+=1;
    //         elipsisString = '';
    //         for(var i = 0; i<elipsisCounter; i++){
    //           elipsisString+='.'
    //         }
    //         if(elipsisCounter>7){
    //           elipsisCounter=0;
    //         }
    //         app.setState({animatedEllipsis:elipsisString})
    //       },
    //       1000
    //     );

    InteractionManager.runAfterInteractions(() => {
      this.unsubscribe = firebase.auth().onAuthStateChanged((evt) => {
        if(evt){
          if(evt._auth.authenticated){
            app.props.setUser(evt._user);
            Actions.home();
          }else{
            Alert.alert('Aw snap!', "Error logging in.", [
              {
                text: 'Try Again'
              }
            ]);
          }
        }
      });
    });
  }

  componentWillUnmount() {
    if (this.unsubscribe) {
      this.unsubscribe();
    }
  }

  togglePasswordInit() {
    let value = !this.state.passwordInitShow;
    this.setState({passwordInitShow: value});
  }

  signin() {
    const app = this;
    if (app.state.email == '' || app.state.password == '') {
      Alert.alert('Aw snap!', "Incomplete details. All fields are required.", [
        {
          text: 'Try Again'
        }
      ]);
     // app.setState({submit:false});
    } else {
      app.setState({signInText: "We'll be there in a jiffy..."});
      app.setState({loading: true});
      firebase.auth().signInWithEmailAndPassword(app.state.email,  app.state.password)
      .then((user) => {
        console.log('User successfully logged in', user);
        app.setState({loading: false});
      })
      .catch((err) => {
        Alert.alert('Aw snap!', err.toString(), [
          {
            text: 'Try Again'
          }
        ]);
        app.setState({signInText: "Sign In"});
        app.setState({loading: false});
      });
    }
  }

  authFacebook() {
    const app = this;
    app.setState({loading: true});
    LoginManager
    .logInWithReadPermissions(['public_profile', 'email'])
    .then((result) => {
      if (result.isCancelled) {
        return Promise.resolve('cancelled');
      }
      console.log(`Login success with permissions: ${result.grantedPermissions.toString()}`);
      // get the access token      
      return AccessToken.getCurrentAccessToken();
    })
    .then(data => {
      // create a new firebase credential with the token
      const credential = firebase.auth.FacebookAuthProvider.credential(data.accessToken);

      // login with credential
      return firebase.auth().signInWithCredential(credential);
    })
    .then((currentUser) => {
      if (currentUser === 'cancelled') {
        console.log('Login cancelled');
      } else {
        // now signed in
        app.setState({loading: false});
        console.warn(JSON.stringify(currentUser.toJSON()));
      }
    })
    .catch((error) => {
      app.setState({loading: false});
      console.log(`Login fail with error: ${error}`);
    });
  }

  render() {
    // TODO: This should be replaced by a "Please wait..." text. Also
    // handle back button events when the user cancels the operation.
    //var signinText = this.state.submit?(<Text>{this.state.animatedEllipsis}</Text>):(<Text>{"Sign In"}</Text>);

    if (this.staterenderPlaceholderOnly) {
      return null; // render some non-heavy component
    } else {
      return (
        <ScrollView style={{
          flex: 2,
          backgroundColor: '#FFF'
        }}>
        {this.state.loading&&
            <View style={{
              position: 'absolute',
              left: 0,
              right: 0,
              top: 0,
              bottom: 0,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
              <ActivityIndicator size='large' color="#36ADE3"/>
            </View>
            }
          <View style={styles.mainContainer}>

            <Image style={styles.appLogo} source={require('../../assets/knowmads-logo.png')}/>
            <View style={styles.contents}>

              <View style={styles.inputGroup}>
                <Icon name="user" style={styles.inputIcon}></Icon>
                <TextInput style={styles.textInputDefault} placeholder={"EMAIL"} underlineColorAndroid="transparent" value={this.state.email} onChangeText={(text) => {
                  var e = text.replace(/\s/g, '');
                  this.setState({email: e});
                }}/>
              </View>

              <View style={styles.inputGroup}>
                <Icon name="lock" style={styles.inputIcon}></Icon>
                <TextInput style={styles.textInputDefault} placeholder={"PASSWORD"} underlineColorAndroid="transparent" onChangeText={(text) => this.setState({password: text})} secureTextEntry={this.state.passwordInitShow} value={this.state.password}/>
                <TouchableOpacity onPress={this.togglePasswordInit.bind(this)}>
                  <Text>{this.state.passwordInitShow == true
                      ? "SHOW"
                      : "HIDE"}</Text>
                </TouchableOpacity>
              </View>

              <TouchableOpacity style={styles.rightAlignContainer} onPress={OnButtonPress} underlayColor='#F4EDEE'>
                <Text style={styles.forgotPassword}>{"Forgot Password?"}</Text>
              </TouchableOpacity>

              <TouchableOpacity onPress={this.signin.bind(this)} style={styles.loginButtonDefault}>
                <View style={styles.authButtonContents}>
                  <Icon name="sign-in" onPress={OnButtonPress} style={styles.loginButtonIcon}></Icon>
                  <Text style={styles.homeText}>
                    {this.state.signInText}
                  </Text>
                </View>
              </TouchableOpacity>

              <TouchableOpacity onPress={this.authFacebook.bind(this)} style={styles.loginButtonFacebook}>
                <View style={styles.authButtonContents}>
                  <Icon name="facebook" style={styles.loginButtonIcon}></Icon>
                  <Text style={styles.homeText}>{"Continue with Facebook"}</Text>
                </View>
              </TouchableOpacity>

            </View>
            <TouchableOpacity onPress={Actions.newAccount} style={styles.signupButton}>
              <View style={styles.signUpButtonContents}>
                <Text style={styles.signUpText}>{"DON'T HAVE AN ACCOUNT? "}</Text>
                <Text style={styles.signUpTextLink}>{"SIGN UP"}</Text>
              </View>
            </TouchableOpacity>
          </View>

        </ScrollView>
      )
    }
  }
}

// NOTE: Might be causing conflict to babel decorators.
// reactMixin(Auth.prototype, TimerMixin);
