import {StyleSheet} from 'react-native';
const primaryColor = '#36ADE3';
module.exports = StyleSheet.create({
  mainContainer: {
         flex: 1,
         backgroundColor: '#FFF'
     },
     contents: {
         justifyContent: 'center',
         backgroundColor: '#FFF',
         paddingTop: 40,
         flex: 1,
         margin: 15
     },
     welcome: {
         fontSize: 20,
         textAlign: 'center',
         margin: 10
     },
     instructions: {
         textAlign: 'center',
         color: '#333333',
         marginBottom: 5
     },
     appLogo: {
         height: 80,
         resizeMode: 'contain',
         marginTop: 70,
         alignSelf: 'center'
     },
     inputLabelDefault: {
         textAlign: 'left',
         fontSize: 12
     },
     inputGroup: {
         flexDirection: 'row',
         alignItems: 'center',
         borderBottomWidth: 0.5,
         borderColor: "rgba(0,0,0,0.1)",
         paddingTop: 10,
         paddingBottom: 10,
         paddingLeft: 20,
         paddingRight: 20,
         justifyContent: 'center'
     },
     inputIcon: {
         marginRight: 10
     },
     textInputDefault: {
         flex: 1,
         backgroundColor: '#FFF',
         padding: 5,
         borderWidth: 0,
         textAlign: 'left'
     },
     defaultButton: {
         padding: 10
     },
     loginButtonDefault: {
         backgroundColor: primaryColor,
         padding: 16,
         borderRadius: 5,
         marginTop: 5,
         marginBottom: 5
     },
     loginButtonFacebook: {
         backgroundColor: '#3b5998',
         padding: 16,
         borderRadius: 5,
         marginTop: 5,
         marginBottom: 5
     },
     homeText: {
         flex: 1,
         color: '#fff',
         fontSize: 17,
         textAlign: 'center'
     },
     forgotPassword: {
         textAlign: 'right',
         fontSize: 12,
         fontWeight: '700',
         margin: 10
     },
     rightAlignContainer: {
         flex: 0,
         justifyContent: 'flex-end',
         flexDirection: 'row',
         alignItems: 'flex-end'
     },
     signUpText: {
         fontSize: 13
     },
     signUpTextLink: {
         fontSize: 13,
         fontWeight: '700'
     },
     loginButtonIcon: {
         color: '#FFF',
         fontSize: 25
     },
     signupButton:{
       marginBottom: 20
     },
     signUpButtonContents:{
       flex: 1,
       flexDirection: 'row',
       justifyContent: 'center',
       padding: 10
     },
     authButtonContents:{
       alignItems: 'center',
       flexDirection: 'row'
     }

})
