import React, {Component} from 'react'
// import Firestack from 'react-native-firestack'
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TextInput,
  Image,
  TouchableOpacity,
  Alert
} from 'react-native'
import API from '../../api'
import Icon from 'react-native-vector-icons/FontAwesome'
import {GiftedChat} from 'react-native-gifted-chat'
import {Actions} from 'react-native-router-flux'
import {setChannelMessages} from '../../actions/messages'
import {connect} from 'react-redux'
import async from 'async'
// const firestack = new Firestack()
const styles = require('./style.js')

import RNFirebase from 'react-native-firebase'
const configurationOptions = {
  debug: false
}

firebase = RNFirebase.initializeApp(configurationOptions);

@connect((store) => {
  return {user: store.user.user, messages: store.messages.messagesFromChannel}
}, (dispatch) => {
  return {
    getMessagesFromChannel: (fromuid, channel) => {
      dispatch(setChannelMessages(fromuid, channel))
    }
  }
})
export default class Chat extends Component {
  constructor(props) {
    super(props);
    this.state = {
      messages: [],
      loadEarlier: true,
      typingText: null,
      isLoadingEarlier: false,
      channel: null
    }

    this._isMounted = false;
    this.onSend = this.onSend.bind(this);
    this.onReceive = this.onReceive.bind(this);
    this.onLoadEarlier = this.onLoadEarlier.bind(this);
  }

  componentWillMount() {
    const app = this;
    this._isMounted = true;
    app.initMessages(app)
  }

  componentWillUnmount() {
    this._isMounted = false;

  }

  onLoadEarlier() {

    const app = this;

    app.setState((previousState) => {
      return {isLoadingEarlier: true};
    });

    if (app._isMounted === true) {
      app.setState((previousState) => {
        return {
          messages: GiftedChat.prepend(previousState.messages, app.props.messages),
          loadEarlier: false,
          isLoadingEarlier: false
        };
      });
    }

  }

  // TODO: This should return a promise.
  sendMessage(messageRef, messages, app) {
    let date = new Date(messages[0].createdAt);
    var updates = {};
    let newPostKey = firebase.database().ref().push().key;
    const postData = {
      createdAt: date.getTime(),
      text: messages[0].text,
      user: app.props.user.uid
    };
    updates[newPostKey] = postData;
    messageRef.update(updates).then(() => {
      app.setState((previousState) => {
        return {
          messages: GiftedChat.append(previousState.messages, messages)
        };
      });
    })
  }

  onSend(messages = []) {
    const app = this;

    var messageRef;

    switch (app.props.message_type) {
      case "existing":
        messageRef = firebase.database().ref('/messages').child(app.props.data.channel);
        app.sendMessage(messageRef, messages, app);
        break;
      case "new":
        // Update message connection keys
        let senderMessageKey = {};
        let recipientMessageKey = {};
        senderMessageKey['users/' + app.props.user.uid + '/messages/' + app.state.channel] = true;
        recipientMessageKey['users/' + app.props.target_uid + '/messages/' + app.state.channel] = true;
        firebase.database().ref().update(senderMessageKey);
        firebase.database().ref().update(recipientMessageKey);

        messageRef = firebase.database().ref('/messages').child(app.state.channel);
        app.sendMessage(messageRef, messages, app);
        break;
      default:
    }

  }

  answerDemo() {
    const app = this;
    // if (messages.length > 0) {
    //   if ((messages[0].image || messages[0].location) || !app._isAlright) {
    //     app.setState((previousState) => {
    //       return {typingText: 'React Native is typing'};
    //     });
    //   }
    // }

    setTimeout(() => {
      if (this._isMounted === true) {
        if (!this._isAlright) {
          this._isAlright = true;
          this.onReceive('Alright');
        }
      }

      app.setState((previousState) => {
        return {typingText: null};
      });
    }, 1000);
  }

  onReceive(data) {
    const app = this;
    var usersRefName = firebase.database().ref("users/" + data.user + "/displayName");
    var usersRefPhoto = firebase.database().ref("users/" + data.user + "/photoUrl");
    var displayName = "";
    var photoUrl = "";
    // Do not append to chat if user key is the same with from the database.
    if (this._isMounted === true) {
      if (data.user !== app.props.user.uid) {
        usersRefName.once("value").then((n) => {
          displayName = n.val();
          usersRefPhoto.once("value").then((p) => {
            photoUrl = p.val();
            app.setState((previousState) => {
              return {
                messages: GiftedChat.append(previousState.messages, {
                  _id: API.uuid(),
                  text: data.text,
                  createdAt: data.createdAt,
                  user: {
                    _id: data.user,
                    name: displayName,
                    avatar: photoUrl
                  }
                })
              };
            });
          })
        })
      }
    }

  }

  initMessages(app) {
    switch (app.props.message_type) {
      case "existing":
        app.props.getMessagesFromChannel(app.props.user.uid, app.props.data.channel);
        var messageRef = firebase.database().ref('/messages').child(app.props.data.channel);
        messageRef.orderByChild('createdAt').limitToLast(1).on("child_added", (snapshot) => {
          app.onReceive(snapshot.val())
        });
        break;
      case "new":
        let newPostKey = firebase.database().ref().push().key;
        app.setState({channel: newPostKey});
        break;
      default:
        console.log("============================");
        console.log("No selection.");
        console.log("============================");
    }
  }

  componentDidMount() {}

  componentWillReceiveProps() {}

  render() {
    return (
      <View style={{
        flex: 1,
        marginTop: 55
      }}>
        <GiftedChat
          messages={this.state.messages}
          onSend={this.onSend}
          loadEarlier={this.state.loadEarlier}
          onLoadEarlier={this.onLoadEarlier}
          isLoadingEarlier={this.state.isLoadingEarlier}
          user={{
          _id: this.props.user.uid, // sent messages should have same user._id
          }}/>
      </View>
    );
  }
}
