import {
  StyleSheet
} from 'react-native';
const primaryColor = '#36ADE3';
module.exports = StyleSheet.create({
  container: {
    alignItems: 'center',
    flex: 1
  },
  background:{
    flex: 1,
    width: null,
    alignSelf: 'stretch',
    alignItems: 'center',
    borderWidth: 0,
    resizeMode: 'cover',
  },
  wrapper: {
    alignItems: 'center',
    flex: 1
  },
  slideContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    margin: 20
  },
  swiperWrapper:{
    flex: 1,
    height: '100%',
    width: '100%',
    alignSelf: 'stretch'
  },
  text: {
    color: '#333',
    fontSize: 20,
    textAlign: 'center'
  },
  textSmall: {
    fontSize: 15,
    textAlign: 'center'
  },
  appIcon: {
    height: 80,
    resizeMode: 'contain',
    marginBottom: 30,
    marginTop: 20
  },
  onboardImage:{
    height: 350,
    resizeMode: 'contain',
  },
  textbox: {
    padding: 20,
    alignSelf: 'center',
    marginTop: 20
  },
  defaultDefaultText:{
    color:'#FFF'
  },
  onboardingNav:{
    color: '#333'
  },
  skipButtonContainer:{
    position:'absolute', bottom: 20, right: 20, margin: 20
  },
  skipButton: {
    backgroundColor: primaryColor,
    padding: 10,
    paddingLeft: 20,
    paddingRight: 20,
    borderRadius: 8
  },
  skipButtonText:{
    color:'#FFF',
    fontSize: 30
  }
})
