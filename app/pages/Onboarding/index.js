import React, {Component} from 'react'
import {View, Text, StyleSheet, TouchableOpacity, Image, AsyncStorage} from 'react-native'
import {Actions, ActionConst} from 'react-native-router-flux'
import Swiper from 'react-native-swiper'
import Icon from 'react-native-vector-icons/FontAwesome'
import Display from 'react-native-display'
const styles = require('./style.js')
const primaryColor = '#36ADE3'

const dot = (<View style={{
  backgroundColor: 'rgba(255,255,255,.2)',
  width: 8,
  height: 8,
  borderRadius: 4,
  marginLeft: 3,
  marginRight: 3,
  marginTop: 3,
  marginBottom: 3
}}/>)

const activeDot = (<View style={{
  backgroundColor: '#FFF',
  width: 8,
  height: 8,
  borderRadius: 4,
  marginLeft: 3,
  marginRight: 3,
  marginTop: 3,
  marginBottom: 3
}}/>)

const nextButton = (
  <Icon name="caret-right" size={30} color={primaryColor} />
)

const prevButton = (
  <Icon name="caret-left" size={30} color={primaryColor} />
)
export default class Onboarding extends Component {
  constructor(props) {
    super(props);
  }

  finish(){
    try {
      AsyncStorage.setItem('onboardingDone', 'true').then(()=>Actions.auth({type: ActionConst.RESET}));
    } catch (error) {
      console.log("Error saving data");
    }
  }
  ren

  render() {
    return (
      <View style={[styles.container, {flex:1}]}>
        <Image style={styles.background} source={require('../../assets/profile-public-preview-background.png')}>

          <Swiper style={styles.wrapper} showsButtons loop={false} dot={dot} activeDot={activeDot} nextButton={nextButton} prevButton={prevButton} containerStyle={styles.swiperWrapper}>
            <View style={styles.slideContainer}>
              <Image style={styles.appIcon} source={require('../../assets/knowmads-logo.png')}></Image>
              <Text style={styles.text}>{"Welcome Fellow Backpacker!"}</Text>
              <View style={styles.textbox}>
                <Text style={styles.textSmall}>{"With Knowmads you can connect with those traveling around where you at!"}</Text>
              </View>
              <Icon.Button name="arrow-right" backgroundColor="#36ADE3" onPress={()=>this.finish()} style={[styles.defaultButton,styles.skipButton]}>
                <Text style={[styles.defaultDefaultText,{fontSize:15}]}>Skip</Text>
              </Icon.Button>
            </View>
            <View style={styles.slideContainer}>
              <View style={styles.textbox}>
                <Text style={styles.textSmall}>{"Follow your fellow backpacker."}</Text>
              </View>
              <Image style={styles.onboardImage} source={require('../../assets/onboard1.png')}></Image>
            </View>
            <View style={styles.slideContainer}>
              <View style={styles.textbox}>
                <Text style={styles.textSmall}>{"Add your favorite locations and update your status."}</Text>
              </View>
              <Image style={styles.onboardImage} source={require('../../assets/onboard2.png')}></Image>
            </View>
            <View style={styles.slideContainer}>
              <View style={styles.textbox}>
                <Text style={styles.textSmall}>{"Customize your map settings."}</Text>
              </View>
              <Image style={styles.onboardImage} source={require('../../assets/onboard3.png')}></Image>
            </View>
            <View style={styles.slideContainer}>
              <View style={styles.textbox}>
                <Text style={styles.textSmall}>{"Share your favorite location on the map."}</Text>
              </View>
              <Image style={styles.onboardImage} source={require('../../assets/onboard4.png')}></Image>
            </View>
            <View style={styles.slideContainer}>
              <Text style={styles.text}>{"Stay updated!"}</Text>
              <View style={styles.textbox}>
                <Text style={styles.textSmall}>{"Check your backpacker feed."}</Text>
              </View>
              <Image style={styles.onboardImage} source={require('../../assets/onboard5.png')}></Image>
            </View>
            <View style={styles.slideContainer}>
              <Text style={styles.text}>{"Note"}</Text>
              <View style={styles.textbox}>
                <Text style={styles.textSmall}>{"This app is still on it's alpha stage."}</Text>
              </View>

              <Icon.Button name="flask" backgroundColor="#36ADE3" onPress={()=>this.finish()} style={styles.defaultButton}>
                <Text style={styles.defaultDefaultText}>{"Get Started"}</Text>
              </Icon.Button>
            </View>
          </Swiper>

{/*
          <View style={styles.slideContainer}>
              <Text style={styles.text}>{"Note"}</Text>
              <View style={styles.textbox}>
                <Text style={styles.textSmall}>{"This app is still on it's alpha stage."}</Text>
              </View>

              <Icon.Button name="flask" backgroundColor="#36ADE3" onPress={Actions.auth} style={styles.defaultButton}>
                <Text style={styles.defaultDefaultText}>{"Get Started"}</Text>
              </Icon.Button>
            </View>
*/}
          {/*Option to skip the welcome screens*/}
          {/* <Display enable={true} style={styles.skipButtonContainer}>
            <TouchableOpacity style={styles.skipButton} onPress={()=>this.finish()}>
              <Text style={styles.skipButtonText}>{"Skip"}</Text>
            </TouchableOpacity>
          </Display> */}
        </Image>
      </View>
    )
  }
}
