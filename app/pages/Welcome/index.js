import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import Onboarding from 'react-native-simple-onboarding';
import { Actions } from 'react-native-router-flux';

export default class Welcome extends Component {

  render() {
    return (
      <View>
        <Onboarding
          pages={[
            { backgroundColor: '#fff', image: require('../../assets/avatar-01.png'), title: 'Simple Messenger UI', subtitle: 'Implemented in React Native' },
            { backgroundColor: "#fe6e58", image: require('../../assets/avatar-01.png'), title: 'Welcome', subtitle: 'To Earth' },
            { backgroundColor: "#999", image: require('../../assets/avatar-01.png'), title: 'Also', subtitle: 'Mars is nice' },
          ]}
          onEnd={Actions.auth}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({

});
