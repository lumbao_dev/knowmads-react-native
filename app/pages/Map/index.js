import React, { Component, PropTypes } from 'react'
import {
  ActivityIndicator,
  Alert,
  AsyncStorage,
  Button,
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableHighlight,
  TouchableOpacity,
  View,
  InteractionManager,
  Dimensions,
} from 'react-native'

import API from '../../api'
import * as Animatable from 'react-native-animatable'
import Display from 'react-native-display'
import IconBadge from 'react-native-icon-badge'
import RNGooglePlaces from 'react-native-google-places'
import ImagePicker from 'react-native-image-crop-picker'
import * as Progress from 'react-native-progress'
import { Actions } from 'react-native-router-flux'
import MapView from 'react-native-maps'
import { subscribe } from 'redux-subscriber'
import UserMarker from '../../components/UserMarker/'
import { connect } from 'react-redux'
import { setUser } from '../../actions/userActions'
import { setUserConnections, setFriends, setNonFriends } from '../../actions/userConnectionActions'
import { setAttraction, setAccommodation } from '../../actions/places'
import * as types from '../../actions/types'
const styles = require('../../components/GlobalStyles/Main.js')
const colors = require('../../components/GlobalStyles/ColorPalette.js')
const mapDefaultCoordinates = {
  latitude: 0,
  longitude: 0,
  latitudeDelta: 1,
  longitudeDelta: 1
};

import RNFirebase from 'react-native-firebase'
const configurationOptions = {
    debug: false,
    persistence: true
  }

firebase = RNFirebase.initializeApp(configurationOptions);

@connect((store) => {
  return {
    user: store.user.user,
    userConnections: store.userConnections.userConnections,
    showUserLocation: store.mapSettings.showUserLocation,
    showPublicUsers: store.mapSettings.showPublicUsers,
    showPrivateUsers: store.mapSettings.showPrivateUsers,
    showAccommodations: store.mapSettings.showAccommodations,
    showAttractions: store.mapSettings.showAttractions,
    addAccommodation: store.mapAction.addAccommodation,
    addAttraction: store.mapAction.addAttraction,
    processingMapRequestVal: store.mapAction.processingMapRequest,
    friends: store.friends.friends,
    nonFriends: store.friends.nonFriends,
    accommodations: store.places.accommodations,
    attractions: store.places.attractions,
    toastNotificationMessage: store.toastNotification.message,
  };
}, (dispatch) => {
  return {
    addAccommodation: () => {
      dispatch({ type: types.ADD_ACCOMMODATION, payload: true });
      dispatch({ type: types.ADD_ATTRACTION, payload: false });
      Actions.addPlace()
    },
    addAttraction: () => {
      dispatch({ type: types.ADD_ATTRACTION, payload: true });
      dispatch({ type: types.ADD_ACCOMMODATION, payload: false });
      Actions.addPlace()
    },
    showAccommodationsAction: (val) => {
      dispatch({ type: types.SHOW_ACCOMMODATIONS, payload: val })
    },
    showAttractionsAction: (val) => {
      dispatch({ type: types.SHOW_ATTRACTIONS, payload: val })
    },
    showPublicUsersAction: (val) => {
      dispatch({ type: types.SHOW_PUBLIC_USERS, payload: val })
    },
    showPrivateUsersAction: (val) => {
      dispatch({ type: types.SHOW_PRIVATE_USERS, payload: val })
    },
    setConnectionsAction: (uid) => {
      dispatch(setUserConnections(uid))
    },
    setFriends: (uid) => {
      dispatch(setFriends(uid))
    },
    setNonFriends: (uid) => {
      dispatch(setNonFriends(uid))
    },
    setAccommodations: () => {
      dispatch(setAccommodation())
    },
    setAttractions: () => {
      dispatch(setAttraction())
    },
    toastNotify: (val) => {
      dispatch({ type: types.TOAST_NOTIFICATION, payload: val })
    },
    processingMapRequest: (val) => {
      dispatch({ type: types.PROCESSING_MAP_REQUEST, payload: val })
    },

  }
})
export default class Map extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // App states.
      loading: true,
      actionSheetSliderIsClosed: true,

      // Map default settings.
      center: mapDefaultCoordinates,
      region: mapDefaultCoordinates,
      coordinateDelta: 1,
      showActionSheet: false,
      initialPosition: mapDefaultCoordinates,
      lastPosition: mapDefaultCoordinates,
      activePosition: mapDefaultCoordinates,
      showFab: false,
      showInfoBar: false,
      showFabDraggable: false,
      actionSheetMode: "",
      mapkey: new Date(),
      gpsEnabled: true,

      // Client default states
      user: this.props.user,
      accommodations: '',
      attractions: '',
      feedImage: '',
      feedImagePath: '',
      checkinLocation: '',
      backpackers: null,
      profileNotifications: 0,
      renderPlaceholderOnly: true,
      deviceWidth: Dimensions.get('window').width, //full width
      processingMapRequest: false,
      toastNotificationMessage: this.props.toastNotificationMessage
    }
  }

  launchPickerFromCamera() {
    const app = this;
    this.toggleActionSheet();
    ImagePicker.openCamera({ width: 1366, height: 768, cropping: true }).then(source => {
      // console.log(source);
      this.setState({
        feedImage: {
          uri: source.path
        }
      });
      this.setState({ feedImagePath: source.path });
      RNGooglePlaces.openPlacePickerModal().then((place) => {
        app.setState({ checkinLocation: place });
        app.setState({ processingMapRequestVal: true });
          firebase.database().ref('places').push().then((res) => {
          newPostKey = res.key;
          app.setState({appid:newPostKey});
          let updates = {};

          firebase.ServerValue.then(map => {
            const postData = {
              timestamp: map.TIMESTAMP,
              author: app.state.user.uid,
              location: place,
              type: "other"
            }
            updates['/places/' + newPostKey] = postData;

            firebase.database().ref().update(updates).then(()=>{
              API.submitPhotoToFeed(source.path, app.props.user.uid, app.props, newPostKey)
              .then(() => {
                app.setState({ processingMapRequestVal: false });
              })
            })
          })
          });
      }).catch((err) => {
        console.log(err.message)
      })

    });
  }

  checkIn() {
    const app = this;
    this.toggleActionSheet();
    RNGooglePlaces.openPlacePickerModal().then((place) => {
      app.setState({ checkinLocation: place });
      app.setState({ processingMapRequestVal: true });
        firebase.database().ref('places').push().then((res) => {
        newPostKey = res.key;
        app.setState({appid:newPostKey});
        let updates = {};

        firebase.ServerValue.then(map => {
          const postData = {
            timestamp: map.TIMESTAMP,
            author: app.state.user.uid,
            location: place,
            type: "other"
           }
           updates['/places/' + newPostKey] = postData;

           firebase.database().ref().update(updates).then(()=>{
            API.submitCheckinToFeed(app.state.user.uid, newPostKey, app.props)
            .then(() => {
              app.setState({ processingMapRequestVal: false });
            })
           })
         })
        });
    }).catch((err) => {
      console.log(err.message)
    })
  }

  loadData() {
    const app = this;
    // NOTE: Check if the settings is empty. If true writeout the default settings else set a new
    // one from storage. This setting should only be on the first page (Map)
    AsyncStorage.getItem("showAccommodations").then((value) => {
      let res = JSON.parse(value);
      if (value != null) {
        app.props.showAccommodationsAction(res)
      }
      return AsyncStorage.getItem("showAttractions")
    }).then((value) => {
      let res = JSON.parse(value);
      if (value != null) {
        app.props.showAttractionsAction(res)
      }
      return AsyncStorage.getItem("showUserLocation")
    }).then((value) => {
      let res = JSON.parse(value);
      if (value != null) {
        app.props.showUserLocationAction(res)
      }
      return AsyncStorage.getItem("showPublicUsers")
    }).then((value) => {
      let res = JSON.parse(value);
      if (value != null) {
        app.props.showPublicUsersAction(res)
      }
      return AsyncStorage.getItem("showPrivateUsers")
    }).then((value) => {
      let res = JSON.parse(value);
      if (value != null) {
        app.props.showPrivateUsersAction(res)
      }
      return AsyncStorage.getItem("user")
    }).then((value) => {
      let res = JSON.parse(value);
      app.setState({ loading: false })
    }).done();
  }

  watchID:
    ? number = null;

  componentDidMount() {

    let app = this;

    // const toastNotifWatcher = subscribe('mapAction.processingMapRequest', state => {
    //   app.setState({processingMapRequestVal:false})
    // });

    InteractionManager.runAfterInteractions(() => {
      app.setState({ renderPlaceholderOnly: false });
      navigator.geolocation.getCurrentPosition((position) => {
        this.setState({ gpsEnabled: true });
        this.setState({ showFab: true });
        this.setState({ initialPosition: position.coords });
        this.setState({ activePosition: position.coords });
        this.setState({
          region: {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            latitudeDelta: app.state.coordinateDelta,
            longitudeDelta: app.state.coordinateDelta
          }
        });
        //console.log(position.coords);
      }, (error) => {
        this.setState({ gpsEnabled: false });
      });

      app.watchID = navigator.geolocation.watchPosition((position) => {
        var lastPosition = position;
        // console.log(position);
        // app.setState({ lastPosition: position.coords });
        //app.setState({ activePosition: position.coords });
        //firebase.database().ref('users/' + app.state.user.uid).update({lastPosition: lastPosition});
      });
      app.loadData();
      app.props.setFriends(app.props.user.uid);
      app.props.setNonFriends(app.props.user.uid);
      app.props.setAccommodations();
      app.props.setAttractions();
    });
  }

  onRegionChange(region) {
    this.setState({ region });
  }

  componentWillUnmount() {
    navigator.geolocation.clearWatch(this.watchID);
  }

  componentWillReceiveProps(nextProps) {
    const app = this;
    // app.setState({processingMapRequestVal: nextProps.processingMapRequestVal});
    // app.setState({toastNotificationMessage: nextProps.toastNotificationMessage})
    //const {user} = this.state;
    //this.loadData();
    //firebase.database().ref('users/' + user.uid).update({lastPosition: this.state.activePosition});
  }

  toggleActionSheet() {

    let toggle = !this.state.showActionSheet;
    this.setState({ showActionSheet: toggle });
    this.state.actionSheetSliderIsClosed == true
      ? this.refs.actionSheet.mapActionSheetSlideDown().then(this.setState({ actionSheetSliderIsClosed: false }))
      : this.refs.actionSheet.mapActionSheetSlideUp().then(this.setState({ actionSheetSliderIsClosed: true }))
  }

  addPlace() {
    this.toggleActionSheet();
  }

  toggleFabActionButton() {
    let toggle = !this.state.showFab;
    this.setState({ showFab: toggle });
  }

  render() {
    // NOTE: Wait for AsyncStorage to finish the task before rendering the components to avoid empty states.

    /* FIXME: Coodinate causes random crash. Coordinate can be initialized with default values.
    * This bug may be resolved with rxjs + redux
    */
    let markerLoading = false;

    if (this.props.friends != null) {
      markerLoading = false;
    } else {
      markerLoading = true
    }

    if (this.staterenderPlaceholderOnly) {
      return null; // render some non-heavy component
    } else {
      return (

        <View style={styles.container}>

          <View style={styles.contents}>

            {this.state.gpsEnabled == true
              ? (
                <MapView
                  style={styles.map}
                  region={this.state.region}
                  initialRegion={{
                    latitude: parseFloat(this.state.initialPosition.latitude),
                    longitude: parseFloat(this.state.initialPosition.longitude),
                    latitudeDelta: parseFloat(this.state.coordinateDelta),
                    longitudeDelta: parseFloat(this.state.coordinateDelta)
                  }}
                  onRegionChange={this.onRegionChange.bind(this)}
                  toolbarEnabled={false}
                  loadingEnabled
                >

                  <Display key="userLocation" enable={this.props.showUserLocation}>
                    <UserMarker latitude={this.state.activePosition.latitude} longitude={this.state.activePosition.longitude}></UserMarker>
                  </Display>

                  <Display key="publicMarkers" enable={this.props.showPublicUsers}>
                    {API.setFriendMarkers(this.props.friends)}
                  </Display>

                  <Display key="privateMarkers" enable={this.props.showPrivateUsers}>
                    {API.setNonFriendMarkers(this.props.nonFriends)}
                  </Display>

                  <Display key="accommodations" enable={this.props.showAccommodations}>
                    {API.setAccommodationMarkers(this.props.accommodations)}
                  </Display>

                  <Display key="attractions" enable={this.props.showAttractions}>
                    {API.setAttractionMarkers(this.props.attractions)}
                  </Display>

                </MapView>
              )
              : (
                <View></View>
              )}
          </View>
          <Display key="fabAction" enable={this.state.showFab}>
            <TouchableOpacity style={styles.fabFloating} onPress={this.toggleActionSheet.bind(this)}>
              <Image style={styles.fabFloatingIcon} source={require('../../assets/map-fab-icon.png')} />
            </TouchableOpacity>
          </Display>
          <Display enable={this.state.showActionSheet} style={styles.modalBackground}>
            <TouchableHighlight style={{
              flex: 1
            }} onPress={this.toggleActionSheet.bind(this)} underlayColor="rgba(0,0,0,0.2)">
              <View></View>
            </TouchableHighlight>

          </Display>
          <Animatable.View duration={300} ref="actionSheet" useNativeDriver={false} style={styles.actionSheetContainer}>
            <View style={styles.actionSheetContent}>
              <Text style={styles.actionSheetTitle}>{"Add New"}</Text>

              <View style={styles.actionSheetLine}></View>
              <TouchableHighlight underlayColor="#CCC" onPress={() => { this.props.addAccommodation(); this.toggleActionSheet(); }}>
                <View style={styles.actionSheetItem}>
                  <Image style={styles.actionSheetIcon} source={require('../../assets/store-gray.png')} />
                  <Text style={styles.actionSheetText}>{"Accommodation"}</Text>
                </View>
              </TouchableHighlight>
              <TouchableHighlight underlayColor="#CCC" onPress={() => { this.props.addAttraction(); this.toggleActionSheet(); }}>
                <View style={styles.actionSheetItem}>
                  <Image style={styles.actionSheetIcon} source={require('../../assets/attraction-gray.png')} />
                  <Text style={styles.actionSheetText}>{"Attraction"}</Text>
                </View>
              </TouchableHighlight>
              <TouchableHighlight underlayColor="#CCC" onPress={this.checkIn.bind(this)}>
                <View style={styles.actionSheetItem}>
                  <Image style={styles.actionSheetIcon} source={require('../../assets/mylocation-gray.png')} />
                  <Text style={styles.actionSheetText}>{"Check In"}</Text>
                </View>
              </TouchableHighlight>
              <TouchableOpacity underlayColor="#CCC" onPress={this.launchPickerFromCamera.bind(this)}>
                <View style={styles.actionSheetLastItem}>
                  <Image style={styles.actionSheetIcon} source={require('../../assets/photo-gray.png')} />
                  <Text style={styles.actionSheetText}>{"Photo"}</Text>
                </View>
              </TouchableOpacity>
            </View>

            <TouchableHighlight style={styles.actionSheetDismiss} underlayColor="#CCC" onPress={this.toggleActionSheet.bind(this)}>
              <View>
                <Text style={styles.actionSheetDismissText}>{"Cancel"}</Text>
              </View>
            </TouchableHighlight>
          </Animatable.View>


          <Display enable={markerLoading}>
            <Animatable.Text animation="fadeIn" iterationCount={"infinite"} direction="alternate-reverse" style={{ position: 'absolute', bottom: 60, flex: 1, color: '#FFF', margin: 5 }} useNativeDriver={true} >{"Loading..."}</Animatable.Text>
          </Display>

          {/*Progress Indicator - Temporarily disabled*/}
          <Display enable={false}>
            <Progress.Bar
              progress={0.5}
              width={this.state.deviceWidth}
              indeterminate={true}
              borderWidth={0}
              height={3}
              color={colors.PoloBlue}
              unfilledColor={colors.PoloBlueLighter}
              borderRadius={0}
            />
          </Display>

        </View>

      )
    }
    //end
  }
}
