import {StyleSheet} from 'react-native';

module.exports = StyleSheet.create({
  icons: {
    width: 23,
    height: 23,
    padding: 5
  }
})
