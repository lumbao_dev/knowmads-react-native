import React, { Component } from 'react'
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    ScrollView,
    ActivityIndicator,
    ListView,
    RefreshControl,
    Image
} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import { Actions } from 'react-native-router-flux'
import * as Animatable from 'react-native-animatable'
import * as Progress from 'react-native-progress'
import { connect } from 'react-redux'
import FeedItem from '../../components/FeedItem/'
import { setFeed } from '../../actions/feed'
// import Firestack from 'react-native-firestack'
// const firestack = new Firestack()

const styles = require('../../components/GlobalStyles/Main.js')
const colors = require('../../components/GlobalStyles/ColorPalette.js')

import RNFirebase from 'react-native-firebase'
const configurationOptions = {
    debug: false
  }
  
firebase = RNFirebase.initializeApp(configurationOptions);

@connect((store) => {
    return {
        feed: store.feed.feed
    }
}, (dispatch) => {
    return {
        setFeed: () => {
            dispatch(setFeed())
        }
    }
})
export default class Feed extends Component {
    constructor(props) {
        super(props);

        this.ref = null;
        this.listView = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2,
        });

        this.state = {
            feed: this.listView.cloneWithRows({}),
            refreshing: false,
        };

        this.feed = {};
    }

    componentWillMount(){
        const app = this;
        app.handleFeedUpdate()
    }

    componentDidMount() {
        const app = this;
        app.ref = firebase.database().ref('feed/');
        app.ref.once(' value').then(()=>{
            app.handleFeedUpdate()
        });
    }

    // Unsubscribe from the feed on unmount
    componentWillUnmount() {
        // if (this.ref) {
        //     this.ref.off('value', this.handleFeedUpdate());
        // }
    }

    // Handle Feed updates
    handleFeedUpdate = () => {
        const app = this;
        this.setState({refreshing: true});
        app.props.setFeed();
        app.feed = app.props.feed || {};

        app.setState({
            feed: app.listView.cloneWithRows(this.feed),       
        });

        this.setState({refreshing: false});
    }

  //     _onRefresh() {
  //   this.setState({refreshing: true});
  //   fetchData().then(() => {
  //     this.setState({refreshing: false});
  //   });
  // }

    renderFeed(feed) {
        if (feed.complete) {
            return null;
        }
        return (
            <FeedItem data={feed} />
        );
    }

    componentWillReceiveProps(){
        this.handleFeedUpdate()     
    }

    render() {
        return (
            <View style={[styles.container, {flex: 1}]}>
                <ListView
                    dataSource={this.state.feed}
                    renderRow={(...args) => this.renderFeed(...args)}
                    enableEmptySections={true}
                    initialListSize={20}
                    refreshControl={
                      <RefreshControl
                        refreshing={this.state.refreshing}
                        onRefresh={this.handleFeedUpdate.bind(this)}
                      />
                    }
                />
            </View>
        )
    }
}
