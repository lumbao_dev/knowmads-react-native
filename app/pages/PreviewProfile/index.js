import React, {Component, PropTypes} from 'react';
// TODO: Organize imports to be aranged alphabetically
import {
    Text,
    View,
    StyleSheet,
    TouchableHighlight,
    TouchableOpacity,
    Alert,
    TextInput,
    Button,
    ScrollView,
    Image
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Display from 'react-native-display';
// import Firestack from 'react-native-firestack';
import {Actions} from 'react-native-router-flux';
import OnButtonPress from '../../components/OnButtonPress/';
import {connect} from 'react-redux';
// const firestack = new Firestack();

// API
import API from '../../api'
const styles = require('../../components/GlobalStyles/Main.js')

import RNFirebase from 'react-native-firebase'
const configurationOptions = {
    debug: false
  }
  
firebase = RNFirebase.initializeApp(configurationOptions);

@connect((store)=>{
  return {
    user: store.user.user
  }
})
export default class PreviewProfile extends Component {
    constructor(props) {
        super(props)
        this.state = {
            followersCounter: 0,
            followingCounter: 0,
            followAction: 'not-following'
        }
    }

    follow(intent){
      switch (intent) {
        case 'follow':
          API.followBackpacker(this.props.previewProfileKey,this.props.user.uid); // Last prop "data[3]" is the active user
          API.updateBackpackerNotification(this.props.previewProfileKey,this.props.user.uid,'follow');
          this.setState({followAction:'pending'});
        break;
        case 'unfollow':
          API.unFollowBackpacker(this.props.previewProfileKey, this.props.previewProfileData.displayName, this.props.user.uid);
        break
        default:
          console.log("No intent was found");
      }
    }

    componentDidMount(){

      console.log("=============Preview Profile Props=============");
      console.log(this.props);
      console.log("===============================================");

      const app = this;

      if(this.props.previewProfileData.connections != null){

        if(typeof this.props.previewProfileData.connections.following != 'undefined'){
          app.setState({followingCounter:Object.keys(this.props.previewProfileData.connections.following).length})
        }

        if(typeof this.props.previewProfileData.connections.followers != 'undefined'){
          app.setState({followersCounter:Object.keys(this.props.previewProfileData.connections.followers).length})
        }

        // Set follow actions.
        API.backpackerPendingFollowRequest(app.props.previewProfileKey).once('value').then((snapshot)=>{
          // this.props.data[3] is the active user
          if(this.props.user.uid in snapshot.val()){
            app.setState({followAction:'pending'})
          }
        }).catch((error)=>{
          console.log(error.message);
        });

        if(this.props.previewProfileType=='public'){
          app.setState({followAction:'following'})
        }else {
          app.setState({followAction:'not-following'})
        }
      }
    }


    render() {

      // Detect if the photoUrl is set. If not, use the default photoUrl
      var photoUrl;
      if(this.props.previewProfileData.photoUrl==null){
        photoUrl = 'https://firebasestorage.googleapis.com/v0/b/carbon-syntax-155109.appspot.com/o/photos%2Fprofile%2Favatar-default.png?alt=media&token=aaf56f25-3f4e-4728-ad06-ac87d4496ca8'
      }else{
        photoUrl = this.props.previewProfileData.photoUrl
      }

      // Conditional Components
      // ----------------------

      // Follow button states.

      /*
      * NOTE: this.props.data[2] contains whether the client is following
      * the backpacker or not. This is used to set the button state
      */

      // Default follow button state.
      let followButton = (
        <TouchableOpacity style={styles.followButton} onPress={()=>this.follow('follow')}>
            <Text style={styles.followButtonText}>{"+ Follow"}</Text>
        </TouchableOpacity>
      );

      switch(this.state.followAction){
        case 'following':
        followButton = (
          <TouchableOpacity style={styles.unFollowButton} onPress={()=>this.follow('unfollow')}>
              <Text style={styles.followButtonText}>{"Unfollow"}</Text>
          </TouchableOpacity>
        );
        break;
        case 'pending':
        followButton = (
          <TouchableOpacity style={styles.pendingButton} disabled>
              <Text style={styles.followButtonText}>{"Pending"}</Text>
          </TouchableOpacity>
        );
        break;
        default:
        followButton = (
          <TouchableOpacity style={styles.followButton} onPress={()=>this.follow('follow')}>
              <Text style={styles.followButtonText}>{"+ Follow"}</Text>
          </TouchableOpacity>
        )
      }

        return (
            <View style={styles.container}>
                <View style={styles.contents}>
                    <ScrollView>
                        <Image style={{flex: 0,width: null,alignSelf: 'stretch',alignItems: 'center',borderWidth: 0, resizeMode:'cover'}} source={
                            this.props.previewProfileType=='public'?require('../../assets/profile-public-preview-background.png'):require('../../assets/profile-public-preview-background.png')
                          }>
                            <View style={{backgroundColor: 'transparent',padding: 30,alignItems: 'center'}}>
                                <View style={[styles.profileImageContainer],{flex:0, paddingTop: 20}}>
                                    <View style={styles.profileImageContent}>
                                        <Image style={styles.profileImage} source={{uri:this.props.previewProfileData.photoUrl}}/>
                                    </View>
                                </View>
                                <View style={{alignItems: 'center'}}>
                                    <Text style={{fontSize: 20}}>{this.props.previewProfileData.displayName}</Text>
                                    <View style={{flexDirection: 'row'}}>
                                      <View style={{flexDirection:'row'}}>
                                        <Text style={styles.profileFollowerText}>{this.state.followingCounter + " Following"}</Text>
                                        <Text style={styles.profileFollowerText}>{this.state.followersCounter  + " Followers"}</Text>
                                      </View>
                                    </View>
                                    <View style={{justifyContent: 'center',margin: 10}}>
                                        <Text style={styles.profileDetailsText}>{"Nationality: " + this.props.previewProfileData.nationality}</Text>
                                    </View>
                                </View>

                                {followButton}

                            </View>
                        </Image>

                    <Display enable={this.props.previewProfileType=='public'?true:false}>
                      <View style={{backgroundColor: '#FFF',flexDirection: 'row',alignItems: 'center'}}>
                          <TouchableOpacity onPress={()=>Actions.chat({data: this.props.previewProfileData, message_type:"new", target_uid: this.props.previewProfileKey})} style={styles.profileButtonMessages}>
                              <View style={styles.profileButtonContent}>
                                  <Icon name="comments" style={styles.profileButtonIcon}></Icon>
                                  <Text style={styles.profileButtonText}>Message</Text>
                              </View>
                          </TouchableOpacity>
                          <TouchableOpacity onPress={OnButtonPress} style={styles.profileButtonCamera}>
                              <View style={styles.profileButtonContent}>
                                  <Icon name="camera" style={styles.profileButtonIcon}></Icon>
                                  <Text style={styles.profileButtonText}>Photos</Text>
                              </View>
                          </TouchableOpacity>
                          <TouchableOpacity 
                          onPress={()=>Actions.track({data:this.props.previewProfileData, title: "Tracking "+this.props.previewProfileData.displayName, backpackerKey: this.props.previewProfileKey})} 
                          style={styles.profileButtonTracker}>
                              <View style={styles.profileButtonContent}>
                                  <Icon name="map" style={styles.profileButtonIcon}></Icon>
                                  <Text style={styles.profileButtonText}>Track</Text>
                              </View>
                          </TouchableOpacity>
                      </View>
                      <Text style={{padding: 20,textAlign: 'center'}}>No feed yet.</Text>
                    </Display>

                    <Display enable={this.props.previewProfileType=='private'?true:false}>
                      <View style={{backgroundColor: '#FFF',flexDirection: 'row',alignItems: 'center'}}>
                    {/*TODO: This event should open an existing message channel if it already exists. */}
                          <TouchableOpacity onPress={()=>Actions.chat({data: this.props.previewProfileData, message_type:"new", target_uid: this.props.previewProfileKey})} style={styles.profileButtonMessages}>
                              <View style={styles.profileButtonContent}>
                                  <Icon name="comments" style={styles.profileButtonIcon}></Icon>
                                  <Text style={styles.profileButtonText}>Message</Text>
                              </View>
                          </TouchableOpacity>                    
                      </View>
                    </Display>                    

                    <Display enable={this.props.previewProfileType=='private'?true:false}>
                      <View style={{flexDirection: 'row',alignItems: 'center', padding: 20}}>
                          <Icon name="lock"/>
                          <Text style={styles.profileFollowerText}>{"Feed is protected. Follow "+ this.props.previewProfileData.displayName}</Text>
                      </View>
                    </Display>

                    </ScrollView>
                </View>
            </View>
        )
    }
}
