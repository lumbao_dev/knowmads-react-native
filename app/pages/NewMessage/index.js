import React, {Component} from 'react'
import {View, Text, StyleSheet, ScrollView, TextInput, Image, TouchableOpacity} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import {Actions} from 'react-native-router-flux'
import {connect} from 'react-redux'
import {setBackpackers} from '../../actions/backpackerActions'
import SearchInput, {createFilter} from 'react-native-search-filter'
import API from '../../api'
const styles = require('./style.js')
const KEYS_TO_FILTERS = ['displayName', 'firstName', 'lastName']

@connect((store)=>{
  return {
    backpackers: store.backpackers.backpackers,
    messages: store.messages.messages
  }
},(dispatch)=>{
  return {
    getBackpackers: () => {
      dispatch(setBackpackers())
    },
    getMessages: (fromuid)=>{
      dispatch(setMessages(fromuid))
    }
  }
})
export default class NewMessage extends Component {
    constructor(props) {
      super(props);
      this.state = {
        searchTerm: '',
        backpackers: {}
      }
    }

    searchUpdated(term) {
      this.setState({ searchTerm: term })
    }

    componentWillMount(){
      const app = this;
      app.props.getBackpackers();
    }

    componentDidMount(){
      const app = this;
      app.setState({backpackers: app.props.backpackers})
    }

    render() {
      let backpackers = Object.values(this.state.backpackers);
      let filteredBackpackers = filteredBackpackers = backpackers.filter(createFilter(this.state.searchTerm, KEYS_TO_FILTERS));
      console.log(filteredBackpackers)
      return (
        <View style={styles.container}>
          <View style={styles.contents}>
            <View style={styles.searchBox}>
              <SearchInput placeholder="Backpacker Name" style={{
                padding: 0, flex: 1
              }} onChangeText={(term) => { this.searchUpdated(term) }} />
            <Icon name="search" style={{color: 'rgba(0,0,0,0.3)',fontSize: 20}} />
            </View>
            <View style={styles.searchResultBox}>
              <Text style={styles.subheader}>{"Suggestions"}</Text>
              <ScrollView>
                {filteredBackpackers.map(res => {
                 return (
                   <TouchableOpacity key={API.uuid()} style={styles.backpackersItem} onPress={()=>Actions.chat({data:res, message_type: "new", target_uid: res.id})}>
                      <Image style={styles.profileImage} source={{uri: res.photoUrl}} />
                      <Text style={styles.displayName}>{res.displayName==null?res.firstName:res.displayName}</Text>
                   </TouchableOpacity>
                 )
               })}
              </ScrollView>
            </View>
          </View>
        </View>
      )
    }
  }
