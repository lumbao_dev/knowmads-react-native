import React, {Component, PropTypes} from 'react';
// TODO: Organize imports to be aranged alphabetically
import {
    Text,
    View,
    Navigator,
    StyleSheet,
    TouchableHighlight,
    Alert,
    TextInput,
    ScrollView,
    Button,
    DatePickerAndroid,
    ActivityIndicator,
    TouchableOpacity,
    Image,
    Picker
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import ImagePicker from 'react-native-image-crop-picker';
// import Firestack from 'react-native-firestack';
const nationalities = require('../../components/Nationalities');
import * as Animatable from 'react-native-animatable';
import Display from 'react-native-display';
const Item = Picker.Item;
const animate = require('../../components/UIAnimations');
import { Actions } from 'react-native-router-flux';
const styles = require('./style.js');
const imagePickerOptions = {
    title: 'Select Profile Photo',
    storageOptions: {
        skipBackup: true,
        path: 'images'
    },
    customButtons: [
        {
            name: 'fb',
            title: 'Choose Photo from Facebook'
        }
    ],
    quality: 0.1,
    videoQuality: 'low'
};

// const firestackOptions = {
//     debug: true,
//     storageBucket: 'gs://carbon-syntax-155109.appspot.com/'
// };

// const firestack = new Firestack(firestackOptions);

import RNFirebase from 'react-native-firebase'
const configurationOptions = {
    debug: false,
    storageBucket: 'gs://carbon-syntax-155109.appspot.com/'
  }
  
firebase = RNFirebase.initializeApp(configurationOptions);

export default class SignUp extends Component {
  constructor(props) {
       super(props)
       this.state = {
           profileImage: require('../../assets/avatar-default.png'),
           displayName: '',
           firstName: '',
           lastName: '',
           email: '',
           birthday: 'BIRTHDAY',
           password: '',
           passwordConfirm: '',
           profileImagePath: '',
           loading: false,
           photoUrl: '',
           user: '',
           inputTextActiveBirthday: 'rgba(0,0,0,0.4)',
           inputTextActiveCountry: 'rgba(0,0,0,0.4)',
           nationality: 'None',
           modalVisible: false,
           errorString: '',
           showActionSheet: false,
           actionSheetMode: "",
           actionSheetSliderIsClosed: true,
           passwordInitShow: true
       }
   }

   componentWillMount() {

   }

   toggleActionSheet() {
       let toggle = !this.state.showActionSheet;
       this.setState({ showActionSheet: toggle });
       this.state.actionSheetSliderIsClosed == true
           ? this.refs.actionSheet.mapActionSheetSlideDown().then(this.setState({ actionSheetSliderIsClosed: false }))
           : this.refs.actionSheet.mapActionSheetSlideUp().then(this.setState({ actionSheetSliderIsClosed: true }))
   }

   /**
   * TODO: Use react-native-image-crop-picker here.
   */
   launchImagePicker() {
       const parent = this;
       Alert.alert('Set Your Profile Picture', "Please select an option below.", [
           {
               text: 'Cancel',
               onPress: () => console.log('Operation canceled.')
           }, {
               text: 'Gallery',
               onPress: () => parent.launchPickerFromGallery()
           }, {
               text: 'Camera',
               onPress: () => parent.launchPickerFromCamera()
           }
       ]);
   }

   emailIsValid(email) {
       var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
       return re.test(email);
   }

   launchPickerFromCamera() {
     this.toggleActionSheet();
       ImagePicker.openCamera({width: 300, height: 300, cropping: true}).then(source => {
           console.log(source);
           this.setState({
               profileImage: {
                   uri: source.path
               }
           });
           this.setState({profileImagePath: source.path});
       });
   }

   /** TODO: Implement show password.
   */
   signin(){
       navigate('Root', 'right', this.props.navigator, 'pop');
   }

   launchPickerFromGallery() {
       this.toggleActionSheet();
       ImagePicker.openPicker({width: 300, height: 300, cropping: true}).then(source => {
           console.log(source);
           this.setState({
               profileImage: {
                   uri: source.path
               }
           });
           this.setState({profileImagePath: source.path});
       });
   }

   togglePasswordInit() {
       let value = !this.state.passwordInitShow;
       this.setState({ passwordInitShow: value });
   }

   async pickDate() {
       try {
           const {action, year, month, day} = await DatePickerAndroid.open();
           if (action === DatePickerAndroid.dismissedAction) {
               console.log("dismissed");
           } else {
               var date = new Date(year, month, day);
               this.setState({birthday: date.toLocaleDateString()})
               this.setState({inputTextActiveBirthday: "rgba(0,0,0,1)"})
           }
       } catch ({code, message}) {
           console.warn(`Error in example: `, message);
       }
   }

   createAccount() {
       // Validate user details first before proceeding to avoid errors
       var error = 0;
       var error_messages = "";
       const app = this;

       if(this.state.profileImage==require('../../assets/avatar-default.png')){
         error_messages += "You haven't set your profile picture yet. \n";
         error+=1;
       }

       if (!this.emailIsValid(this.state.email)) {
           error_messages += "The email you entered is invalid. \n";
           error += 1;
       }

       if (this.state.firstName == '') {
           error_messages += "Your first name is empty. \n";
           error += 1;
       }

       if (this.state.lastName == '') {
           error_messages += "Your last name is empty.  \n";
           error += 1;
       }

       if (this.state.birthday == '') {
           error_messages += "Your birthday is invalid. \n";
           error += 1;
       }

       if (this.state.password != this.state.passwordConfirm) {
           error_messages += "Password did not match. \n";
           error += 1;
       }

       if (this.state.password == '') {
         error_messages += "Password is empty. \n";
         error += 1;
       }

       if (error > 0) {
           Alert.alert("An Error Occured", error_messages, [
             {
               text: 'Retry', onPress: () => {error_messages = ""}
             }
           ]);
       } else {
           firestack.auth.createUserWithEmail(this.state.email, this.state.password).then((data) => {
               // Set view to loading state.
               this.setState({loading: true});

               app.setState({user: data.user});

               if(this.state.profileImagePath==''){
                 // Update user profile even if the profile picture was not updated.
                 firebase.database().ref('users/' + app.state.user.uid).update({
                   photoUrl: 'https://firebasestorage.googleapis.com/v0/b/carbon-syntax-155109.appspot.com/o/photos%2Fprofile%2Favatar-default.png?alt=media&token=aaf56f25-3f4e-4728-ad06-ac87d4496ca8',
                   firstName: this.state.firstName,
                   lastName: this.state.lastName,
                   birthday: this.state.birthday,
                   nationality: this.state.nationality,
                   connections: {followers:0,following:0},
                   email: this.state.email
                 });

                 // Update user profile using Firebase auth.
                 firestack.auth.updateUserProfile({
                     displayName: app.state.displayName,
                 }).then(() => {
                     navigate('Map', 'right', this.props.navigator, 'push', '');
                 });

               }else{
                 //console.log("Uploading to..." + 'photos/profile/' + app.state.user.uid);

                 /** TODO: Compress profile image for the map markers. Compressed images load faster
                 * and improves map performance. Use: https://github.com/bamlab/react-native-image-resizer -this
                 * process should also be async.
                 **/

                 // Upload user profile photo.
                 firestack.storage.uploadFile('photos/profile/' + app.state.user.uid, this.state.profileImagePath, {
                     contentType: 'image/jpeg',
                     contentEncoding: 'base64'
                 }).then((res) => {
                     // Get profile photo link from storage.
                     const storageRef = firestack.storage.ref('photos/profile/' + app.state.user.uid);
                     storageRef.downloadUrl().then((res) => {
                         app.setState({photoUrl: res.fullPath});
                         // Update user profile with photoUrl included.
                         firebase.database().ref('users/' + app.state.user.uid).update({
                           photoUrl: res.fullPath,
                           firstName: this.state.firstName,
                           lastName: this.state.lastName,
                           birthday: this.state.birthday,
                           nationality: this.state.nationality,
                           connections: {followers:0,following:0},
                           email: this.state.email
                         });
                     });
                     // Update user profile using Firebase auth.
                     firestack.auth.updateUserProfile({
                         displayName: app.state.displayName,
                         photoUrl: app.state.photoUrl
                     }).then(() => {
                         navigate('Map', 'right', this.props.navigator, 'push', '');
                     });

                 }).catch(err =>
                   Alert.alert("Aw snap!", err.errorMessage, [
                       {
                           text: 'Ok'
                       }
                   ])
                 )
               }

           }).catch((err) => {
             Alert.alert("Aw snap!", err.errorMessage, [
                 {
                     text: 'Ok'
                 }
             ]);
           })
       }
   }

   render() {
       if (this.state.loading) {
           return (<ActivityIndicator size='large' style={styles.loadingActivity}/>);
       } else {
           return (
               <View style={styles.container}>
                   <ScrollView>
                       <View style={styles.profileImageContainer}>
                           <View style={styles.profileImageContent}>
                               <Image style={styles.profileImage} resizeMethod="scale" source={this.state.profileImage}></Image>
                               <TouchableHighlight style={styles.profileImageEditIcon} onPress={this.toggleActionSheet.bind(this)}>
                                   <Icon name="pencil" style={{
                                       fontSize: 15,
                                       color: '#FFF'
                                   }}></Icon>
                               </TouchableHighlight>
                           </View>
                       </View>
                       <View style={{
                           marginTop: 0,
                           padding: 20,
                           marginBottom: 20
                       }}>
                       <View style={styles.inputGroup}>
                           <TextInput style={styles.textInputDefault} placeholder={"DISPLAY NAME"} underlineColorAndroid="transparent" value={this.state.displayName} onChangeText={(text) => this.setState({displayName: text})}/>
                       </View>
                           <View style={styles.inputGroup}>
                               <TextInput style={styles.textInputDefault} placeholder={"FIRST NAME"} underlineColorAndroid="transparent" value={this.state.firstName} onChangeText={(text) => this.setState({firstName: text})}/>
                           </View>
                           <View style={styles.inputGroup}>
                               <TextInput style={styles.textInputDefault} placeholder={"LAST NAME"} underlineColorAndroid="transparent" value={this.state.lastName} onChangeText={(text) => this.setState({lastName: text})}/>
                           </View>
                           <View style={styles.inputGroup}>
                               <TextInput style={styles.textInputDefault} placeholder={"EMAIL"} underlineColorAndroid="transparent" value={this.state.email} onChangeText={(text) => {
                                   var e = text.replace(/\s/g, '');
                                   this.setState({email: e});
                               }}/>
                           </View>
                           <View style={styles.inputPickerGroup}>
                               <TouchableOpacity style={{
                                   paddingTop: 10,
                                   paddingBottom: 10,
                                   paddingLeft: 5
                               }} onPress={this.pickDate.bind(this)}>
                                   <Text style={{
                                       color: this.state.inputTextActiveBirthday
                                   }}>{this.state.birthday}</Text>
                               </TouchableOpacity>
                           </View>
                           <View style={{
                               borderBottomWidth: 0.5,
                               borderColor: "rgba(0,0,0,0.1)",
                               paddingTop: 10
                           }}>
                               <Text style={{
                                   color: this.state.inputTextActiveCountry,
                                   fontSize: 11,
                                   marginLeft: 5
                               }}>{"NATIONALITY"}</Text>
                               <Picker selectedValue={this.state.nationality} onValueChange={(c) => {
                                   this.setState({nationality: c});
                                   this.setState({inputTextActiveCountry: 'rgba(0,0,0,1)'})
                               }} mode="dialog" prompt="Select Your Nationality">
                                   {nationalities.map((res, i) => (<Item key={i} label={res.value} value={res.value} style={{
                                       color: this.state.inputTextActiveCountry
                                   }}/>))}
                               </Picker>
                           </View>
                           <View style={styles.inputGroup}>
                               <TextInput style={styles.textInputDefault} secureTextEntry={this.state.passwordInitShow} placeholder={"PASSWORD"} underlineColorAndroid="transparent" value={this.state.password} onChangeText={(text) => this.setState({password: text})}/>
                               <TouchableOpacity onPress={this.togglePasswordInit.bind(this)}>
                                 <Text>{this.state.passwordInitShow == true ? "SHOW" : "HIDE"}</Text>
                               </TouchableOpacity>
                           </View>
                           <View style={styles.inputGroup}>
                               <TextInput style={styles.textInputDefault} secureTextEntry={this.state.passwordInitShow} placeholder={"CONFIRM PASSWORD"} underlineColorAndroid="transparent" value={this.state.passwordConfirm} onChangeText={(text) => this.setState({passwordConfirm: text})}/>
                           </View>

                           <View style={styles.spacer}></View>
                           <Icon.Button name="plus-circle" backgroundColor="#36ADE3" onPress={this.createAccount.bind(this)} style={styles.defaultButton}>
                               <Text style={styles.defaultDefaultText}>Create Account</Text>
                           </Icon.Button>
                           <TouchableOpacity style={{
                               flex: 1,
                               marginTop: 10,
                               padding: 10,
                               alignItems: 'center',
                               justifyContent: 'center'
                           }} onPress={Actions.auth} underlayColor='#F4EDEE'>
                               <View style={{
                                   flexDirection: 'row'
                               }}>
                                   <Text style={styles.signUpText}>{"ALREADY HAVE AN ACCOUNT? "}</Text>
                                   <Text style={styles.signUpTextLink}>{"SIGN IN"}</Text>
                               </View>
                           </TouchableOpacity>
                       </View>
                   </ScrollView>

                   <Display enable={this.state.showActionSheet} style={styles.modalBackground}>
                       <TouchableHighlight style={{
                           flex: 1
                       }} onPress={this.toggleActionSheet.bind(this)} underlayColor="rgba(0,0,0,0.2)">
                           <View></View>
                       </TouchableHighlight>

                   </Display>

                   <Animatable.View duration={300} ref="actionSheet" style={styles.actionSheetContainer}>
                       <View style={styles.actionSheetContent}>
                           <Text style={styles.actionSheetTitle}>{"Set Your Profile Photo"}</Text>
                           <View style={styles.actionSheetLine}></View>
                           <TouchableHighlight underlayColor="#CCC" onPress={this.launchPickerFromCamera.bind(this)}>
                               <View style={styles.actionSheetItem}>
                                 <Icon name="camera"></Icon>
                                   <Text style={styles.actionSheetText}>{"Camera"}</Text>
                               </View>
                           </TouchableHighlight>
                           <TouchableOpacity underlayColor="#CCC" onPress={this.launchPickerFromGallery.bind(this)}>
                               <View style={styles.actionSheetLastItem}>
                                   <Icon name="th"></Icon>
                                   <Text style={styles.actionSheetText}>{"Gallery"}</Text>
                               </View>
                           </TouchableOpacity>
                       </View>

                       <TouchableHighlight style={styles.actionSheetDismiss} underlayColor="#CCC" onPress={this.toggleActionSheet.bind(this)}>
                           <View>
                               <Text style={styles.actionSheetDismissText}>{"Cancel"}</Text>
                           </View>
                       </TouchableHighlight>
                   </Animatable.View>
               </View>
           )
       }

   }
  }
