import {StyleSheet} from 'react-native';
const primaryColor = '#36ADE3';
module.exports = StyleSheet.create({
  container: {
        flex: 1,
        backgroundColor: '#FFF'
    },
    statusBar: {
        backgroundColor: primaryColor
    },
    profileImage: {
        height: 100,
        borderRadius: 100,
        width: 100
    },
    profileImageContent: {
        width: 130,
        height: 130,
        borderRadius: 50,
        justifyContent: 'center',
        alignItems: 'center'
    },
    profileImageContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 60
    },
    profileImageEditIcon: {
        backgroundColor: '#32D1C0',
        padding: 10,
        borderRadius: 50,
        position: 'absolute',
        left: 90,
        width: 40,
        height: 40,
        top:15,
        alignItems: 'center',
        justifyContent: 'center'
    },
    inputLabelDefault: {
        textAlign: 'left',
        fontSize: 12
    },

    inputGroup: {
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: 0.5,
        borderColor: "rgba(0,0,0,0.1)",
        paddingTop: 10,
        paddingBottom: 10,
        justifyContent: 'center'
    },
    inputPickerGroup: {
        flexDirection: 'row',
        borderBottomWidth: 0.5,
        borderColor: "rgba(0,0,0,0.1)",
        paddingTop: 10,
        paddingBottom: 10
    },
    textInputDefault: {
        flex: 1,
        backgroundColor: '#FFF',
        padding: 5,
        borderWidth: 0,
        textAlign: 'left'
    },
    defaultButton: {
        padding: 16
    },
    defaultDefaultText: {
        flex: 1,
        color: '#fff',
        textAlign: 'center',
        fontSize: 17
    },
    spacer: {
        height: 20
    },
    signUpText: {
        fontSize: 13,
        textAlign: 'center'
    },
    signUpTextLink: {
        fontSize: 13,
        fontWeight: '700',
        textAlign: 'center'
    },
    checkImageContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    continueButton: {
        backgroundColor: primaryColor,
        padding: 10,
        width: 200,
        margin: 20,
        borderRadius: 8
    },
    checkImage: {
        width: 140,
        resizeMode: 'contain'
    },
    loadingActivity: {
        height: 80,
        flex: 1
    },
    actionSheetContainer: {
        position: 'absolute',
        bottom: -300,
        left: 0,
        right: 0,
        flex: 1
    },
    actionSheetContent: {
        margin: 10,
        backgroundColor: '#FFF',
        borderRadius: 7,
        flex: 1
    },
    actionSheetItem: {
        flexDirection: 'row',
        padding: 8,
        alignItems: 'center',
        borderColor: 'rgba(0,0,0,0.2)',
        borderBottomWidth: 1
    },
    actionSheetLastItem: {
        flexDirection: 'row',
        padding: 10,
        alignItems: 'center'
    },
    actionSheetTitle: {
        margin: 10,
        fontSize: 18,
        color: primaryColor
    },
    actionSheetText: {
        fontSize: 18,
        textAlign: 'left',
        flex: 1,
        marginLeft: 10
    },
    actionSheetLine: {
        flex: 1,
        borderColor: primaryColor,
        borderBottomWidth: 1
    },
    actionSheetIcon: {
        width: 35,
        height: 35
    },
    actionSheetDismiss: {
        margin: 10,
        backgroundColor: '#FFF',
        borderRadius: 7,
        position: 'absolute',
        bottom: -50,
        left: 0,
        right: 0,
        padding: 15,
        marginTop: 20
    },
    actionSheetDismissText: {
        fontSize: 18,
        color: primaryColor,
        textAlign: 'center'
    },
    modalBackground: {
        backgroundColor: 'rgba(0,0,0,0.5)',
        flex: 1,
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0
    }
})
