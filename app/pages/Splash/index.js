import React, { Component } from 'react';
import { Image, View, AsyncStorage, Dimensions } from 'react-native';
import { Actions, ActionConst } from 'react-native-router-flux';
import RNFirebase from 'react-native-firebase';
const configurationOptions = {
    debug: true
}
const firebase = RNFirebase.initializeApp(configurationOptions);

export default class Splash extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        //AsyncStorage.removeItem('onboardingDone'); //for onboarding debugging only
        firebase.auth().onAuthStateChanged(function (user) {
            if (user) {
                AsyncStorage.getItem('onboardingDone', (err, result) => {
                    if (result !== null) {
                        Actions.home({ type: ActionConst.RESET })
                    } else {
                        Actions.onboarding({ type: ActionConst.RESET })
                    }
                });
            } else {
                Actions.auth({ type: ActionConst.RESET })
            }
        });
    }

    render() {
        return (
                <View style={{alignItems: 'center', justifyContent: 'center', flex:1, backgroundColor: '#FFF'}}>
                    <Image
                        style={{ width: 150, height: 150}}
                        source={require('../../assets/knowmads-logo.png')}
                        resizeMode="contain"
                    />
                </View>
        )
    }
}