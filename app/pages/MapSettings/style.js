import {StyleSheet} from 'react-native';

const primaryColor = '#36ADE3';

module.exports = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF'
    },
    statusBar: {
        backgroundColor: primaryColor
    },
    navBar: {
        backgroundColor: '#FFF',
        alignItems: 'center'
    },
    title: {
        color: '#4D4D4D',
        fontWeight: '400',
        textAlign: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        paddingLeft: 30
    },
    buttonText: {
        color: '#FFF',
        fontSize: 16
    },
    navButton: {
        backgroundColor: primaryColor
    },
    navButtonIcon: {
        color: '#4D4D4D',
        fontSize: 25,
        fontWeight: '700'
    },
    resetButton: {
        color: primaryColor,
        fontWeight: '700',
        paddingRight: 10
    },
    contents: {
        padding: 0,
        marginTop: 0
    },
    filterCatergory: {
        fontSize: 15,
        color: '#808080',
        marginBottom: 10
    },
    filterItem: {
        flexDirection: 'row',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10,
        marginBottom: 10
    },
    filterItemText: {
        flex: 1,
        flexDirection: 'column',
        padding: 15
    },
    fiterItemTitle: {
        fontSize: 18,
        color: '#4D4D4D'
    },
    filterDescription: {
        fontSize: 13,
        color: '#808080'
    },
    filterItemIcon: {
        height: 50,
        width: 50
    },
    filterItemIconBackpacker: {
        height: 20,
        width: 20,
        margin: 15
    },
    toggleImage: {
        height: 30,
        width: 30
    },
    hr: {
        borderColor: '#CCC',
        borderBottomWidth: 1,
        flex: 1
    },
    emptyFilterItem: {
        flex: 1,
        height: 100
    }
});
