
// TODO: Settings are not implemented. Listen to Action.pop() method to pass the new props.
import React, {Component, PropTypes} from 'react';
import {
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    Alert,
    Image,
    TextInput,
    Button,
    ScrollView,
    AsyncStorage,
    ActivityIndicator,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import * as Animatable from 'react-native-animatable';
import {resetFilters} from '../../actions/userConnectionActions'
import * as types from '../../actions/types'
import {connect} from 'react-redux';

const styles = require('./style.js');
const toggleOnImage = require('../../assets/toggle-on.png');
const toggleOffImage = require('../../assets/toggle-off.png');


@connect((store)=>{
  return {
    user:store.user,
    showClientLocation: store.mapSettings.showClientLocation,
    showPublicUsers: store.mapSettings.showPublicUsers,
    showPrivateUsers: store.mapSettings.showPrivateUsers,
    showAccommodations: store.mapSettings.showAccommodations,
    showAttractions: store.mapSettings.showAttractions,
  };
},(dispatch)=>{
    return {
        showAccommodationsAction:(val)=>{
            dispatch({type: types.SHOW_ACCOMMODATIONS, payload: val})
        },
        showAttractionsAction:(val)=>{
            dispatch({type: types.SHOW_ATTRACTIONS, payload: val})
        },
        showPublicUsersAction:(val)=>{
            dispatch({type: types.SHOW_PUBLIC_USERS, payload: val})
        },
        showPrivateUsersAction:(val)=>{
            dispatch({type: types.SHOW_PRIVATE_USERS, payload: val})
        },
        showClientLocation:(val)=>{
            dispatch({type: types.SHOW_CLIENT_LOCATION, payload: val})
        },
        resetFilters: ()=>{
        dispatch(resetFilters())
        } 
    }
})
export default class MapSettings extends Component {
    constructor(props) {
        super(props);
        this.state = {
            // App states
            loading: true,

            // App default settings
            showClientLocation: this.props.showClientLocation,
            showPublicUsers: this.props.showPublicUsers,
            showPrivateUsers: this.props.showPrivateUsers,
            showAccommodations: this.props.showAccommodations,
            showAttractions: this.props.showAttractions

        };
    }

    componentDidMount() {
        this.setState({loading: false});
        console.log("================Settings Props================");
        console.log(this.props);
        console.log("==============================================");
    }

    toggleAccommodations() {
        let toggle = !this.state.showAccommodations;
        this.setState({showAccommodations: toggle});
        this.props.showAccommodationsAction(toggle);
    }

    toggleAttractions() {
        let toggle = !this.state.showAttractions;
        this.setState({showAttractions: toggle});
        this.props.showAttractionsAction(toggle);
    }

    toggleOwnerLocation() {
        let toggle = !this.state.showClientLocation;
        this.setState({showClientLocation: toggle});
        this.props.showClientLocation(toggle);
    }

    togglePublicUsers() {
        let toggle = !this.state.showPublicUsers;
        this.setState({showPublicUsers: toggle});
        this.props.showPublicUsersAction(toggle);
    }

    togglePrivateUsers() {
        let toggle = !this.state.showPrivateUsers;
        this.setState({showPrivateUsers: toggle});
        this.props.showPrivateUsersAction(toggle);
    }

    toggleMapSetting(value) {
        if (value) {
            return toggleOnImage
        } else {
            return toggleOffImage
        }
    }

    componentWillReceiveProps(){

    }

    render() {
        if (this.state.loading) {
            return (<ActivityIndicator size='large' style={styles.loadingActivity}/>);
        } else {

            return (
                <View style={styles.container}>
                    <View style={styles.contents}>
                        <ScrollView>
                            <View style={{
                                padding: 20
                            }}>
                                <Text style={styles.filterCatergory}>{"Places"}</Text>
                                <View style={styles.filterItem}>
                                    <Image style={styles.filterItemIcon} source={require('../../assets/store-icon.png')}></Image>
                                    <View style={styles.filterItemText}>
                                        <Text style={styles.fiterItemTitle}>{"Accommodation"}</Text>
                                        <Text style={styles.filterDescription}>{"Hotels, motels, homes...etc."}</Text>
                                    </View>
                                    <TouchableOpacity onPress={this.toggleAccommodations.bind(this)}>
                                        <Image key="toggle1" style={styles.toggleImage} source={this.toggleMapSetting(this.state.showAccommodations)}></Image>
                                    </TouchableOpacity>
                                </View>
                                <View style={styles.filterItem}>
                                    <Image style={styles.filterItemIcon} source={require('../../assets/attraction-icon.png')}></Image>
                                    <View style={styles.filterItemText}>
                                        <Text style={styles.fiterItemTitle}>{"Attraction"}</Text>
                                        <Text style={styles.filterDescription}>{"Places backpackers love to visit"}</Text>
                                    </View>
                                    <TouchableOpacity onPress={this.toggleAttractions.bind(this)}>
                                        <Image key="toggle2" style={styles.toggleImage} source={this.toggleMapSetting(this.state.showAttractions)}></Image>
                                    </TouchableOpacity>
                                </View>
                            </View>

                            <View style={styles.hr}></View>

                            <View style={{
                                padding: 20
                            }}>
                                <Text style={styles.filterCatergory}>{"Backpackers"}</Text>
                                <View style={styles.filterItem}>
                                    <Animatable.View animation="pulse" easing="ease-out" iterationCount="infinite">
                                        <Image style={styles.filterItemIcon} source={require('../../assets/my-location.png')}></Image>
                                    </Animatable.View>
                                    <View style={styles.filterItemText}>
                                        <Text style={styles.fiterItemTitle}>{"Me"}</Text>
                                        <Text style={styles.filterDescription}>{"Show your current location"}</Text>
                                    </View>
                                    <TouchableOpacity onPress={this.toggleOwnerLocation.bind(this)}>
                                        <Image key="toggle3" style={styles.toggleImage} source={this.toggleMapSetting(this.state.showClientLocation)}></Image>
                                    </TouchableOpacity>
                                </View>
                                <View style={styles.filterItem}>
                                    <Image style={styles.filterItemIconBackpacker} source={require('../../assets/knowmad-public-marker.png')}></Image>
                                    <View style={styles.filterItemText}>
                                        <Text style={styles.fiterItemTitle}>{"Public Backpackers"}</Text>
                                        <Text style={styles.filterDescription}>{"Backpackers you follow or whose profiles are public."}</Text>
                                    </View>
                                    <TouchableOpacity onPress={this.togglePublicUsers.bind(this)}>
                                        <Image key="toggle4" style={styles.toggleImage} source={this.toggleMapSetting(this.state.showPublicUsers)}></Image>
                                    </TouchableOpacity>
                                </View>
                                <View style={styles.filterItem}>
                                    <Image style={styles.filterItemIconBackpacker} source={require('../../assets/knowmad-private-marker.png')}></Image>
                                    <View style={styles.filterItemText}>
                                        <Text style={styles.fiterItemTitle}>{"Private Backpackers"}</Text>
                                        <Text style={styles.filterDescription}>{"Only the profile name of these backpackers will be visible until your follow request is accepted."}</Text>
                                    </View>
                                    <TouchableOpacity onPress={this.togglePrivateUsers.bind(this)}>
                                        <Image key="toggle5" style={styles.toggleImage} source={this.toggleMapSetting(this.state.showPrivateUsers)}></Image>
                                    </TouchableOpacity>
                                </View>
                            </View>

                        </ScrollView>
                    </View>

                </View>
            )
        }
    }
}
