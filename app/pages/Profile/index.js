import React, {Component} from 'react';
import {
  ActivityIndicator,
  Alert,
  Button,
  ScrollView,
  StyleSheet,
  Text,
  TouchableHighlight,
  TouchableOpacity,
  View,
  Image,
  AsyncStorage,
  ListView
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Actions} from 'react-native-router-flux';
import Geocoder from 'react-native-geocoder';
import * as Animatable from 'react-native-animatable';
import Display from 'react-native-display';
// import Firestack from 'react-native-firestack';
import GiftedListView from 'react-native-gifted-listview';
import NotificationItem from '../../components/NotificationItem/';
import OnButtonPress from '../../components/OnButtonPress/';
import API from '../../api/';
import {connect} from 'react-redux';
const styles = require('./style.js');
const animate = require('../../components/UIAnimations/');
// const firestack = new Firestack();

import RNFirebase from 'react-native-firebase'
const configurationOptions = {
    debug: false
  }
  
firebase = RNFirebase.initializeApp(configurationOptions);

const dataSource = new ListView.DataSource({
rowHasChanged: (r1, r2) => r1 !== r2
});

@connect((store)=>{
  return {
    user: store.user.user
  }
})

export default class Profile extends Component {

  constructor(props) {
    super(props);
    this.state = {
      loaded: true,
      mapSettingIsClosed: true,
      currentPosition: '',
      nationality: this.props.user.nationality,
      currentGeocodeName: 'Geocoding your coordinates. Please wait...',
      photoUrl: this.props.user.photoUrl,
      notifications: null,
      uid: null,
      followingCounter: 0,
      followersCounter: 0,
    };
  }

  async geoCodeAddressFromCoordinate(coordinates) {
    var geocoderesult;
    Geocoder.geocodePosition(coordinates).then(res => {
      geocoderesult = res[0].streetName + ", " + res[0].locality + ", " + res[0].countryCode;
      // console.log(geocoderesult);
      this.setState({currentGeocodeName: geocoderesult});
    }).catch(err => console.log(err))
  }

  setUserNationality(uid) {
    const app = this;
    firebase.database().ref('/users/' + uid + '/nationality').on('value', function(snapshot) {
      if (snapshot.val()) {
        app.setState({nationality: snapshot.val()})
      }
    })
  }

  setConnections(uid) {
    const app = this;
    firebase.database().ref('/users/' + uid + '/connections/').once('value').then((snapshot) => {
      let data = snapshot.val();
      if (data != null) {
        if (typeof data.following != 'undefined') {
          app.setState({
            followingCounter: Object.keys(data.following).length
          })
        }

        if (typeof data.followers != 'undefined') {
          app.setState({
            followersCounter: Object.keys(data.followers).length
          })
        }
      }
    })
  }

  setUserProfilePhoto(uid) {
    const app = this;
    firebase.database().ref('/users/' + uid + '/photoUrl').on('value', function(snapshot) {
      if (snapshot.val()) {
        app.setState({photoUrl: snapshot.val()});
        console.log(snapshot.val());
      } 
    })
  }

  loadData() {
    const app = this;
    let user = app.props.user;
    app.setUserNationality(user.uid);
    app.setUserProfilePhoto(user.uid);
    app.setConnections(user.uid);
    app.setUserName(user.displayName);
    API.backpackerProfileNotifications(user.uid).on('value', (snapshot) => {
      app.setState({notifications:snapshot.val()})
    });
  }


  componentDidMount() {
    const app = this;
    app.loadData();
    //TODO: Location should be coming from firebase. Geocoding should no longer use navigator
    // navigator.geolocation.getCurrentPosition((position) => {
    //   this.geoCodeAddressFromCoordinate({lat: position.coords.latitude, lng: position.coords.longitude});
    // }, (error) => console.log(JSON.stringify(error)));
  }

  setUserName(displayName){
    const app = this;
    app.setState({displayName: displayName})
  }

  _renderRowView(rowData){
    return (
      <NotificationItem
        notifId={"_"+rowData.timestamp}
        approved={rowData.approved}
        fromUid={rowData.followSourceUid}
        toUid={this.props.user.uid}
        type={rowData.type}
        timestamp={rowData.timestamp}/>
    )
  }

  render() {
    var notificationComponents = [];
    return (
      <View style={styles.contents}>
        <ScrollView>
          <View style={styles.scrollContents}>
            <View style={styles.profileImageContainer}>
              <View style={styles.profileImageContent}>
                <Image style={styles.profileImage} source={{
                  uri: this.state.photoUrl
                }}/>
                <Display enable={false}>
                  <View style={styles.profileImageEditIcon}>
                    <Icon name="pencil" color="#FFF"/>
                    </View>
                </Display>
              </View>
            </View>
            <View style={styles.profileDetailsContainer}>
              <View style={styles.profileDetailsContents}>
                <Text style={styles.displayName}>{this.state.displayName}</Text>
                <TouchableOpacity onPress={OnButtonPress}>
                  <Text>
                    <Text style={styles.followersCounter}>{this.state.followingCounter}
                    </Text>
                    <Text>{" following "}
                    </Text>
                    <Text style={styles.followersCounter}>{this.state.followersCounter}
                    </Text>
                    <Text>{" followers "}
                    </Text>
                  </Text>
                </TouchableOpacity>
              </View>
              <Text style={styles.textBold}>{"Last Location"}</Text>
              <Text>{this.state.currentGeocodeName}</Text>
              <Text style={styles.textBold}>{"Nationality"}</Text>
              <Text>{this.state.nationality}</Text>
              <Text style={styles.textBold}>{"Locations Visited"}</Text>
              <Text>0</Text>
            </View>
          </View>
          <View style={styles.profileButtonContainer}>
            <TouchableOpacity style={styles.editProfileButton} onPress={()=>Actions.editProfile()}>
              <Text style={styles.profileButtonText}>
                <Icon name="pencil" style={styles.profileButtonIcon}></Icon>
                {" Edit Profile"}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.viewPhotosButton} onPress={OnButtonPress}>
              <View style={styles.profileButtonContainer}>
                <Text style={styles.profileButtonText}>
                  <Icon name="camera" style={styles.profileButtonIcon}></Icon>
                  {" Photos"}
                </Text>
              </View>
            </TouchableOpacity>
          </View>

          <View style={styles.notificationsContainer}>
            <Text style={styles.notificationsHeader}>{"NOTIFICATIONS"}</Text>
            {API.getNotificationComponents(this.props.user.uid,this.state.notifications)}
          </View>
        </ScrollView>
      </View>
    )
  }
}
