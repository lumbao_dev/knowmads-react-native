import {StyleSheet} from 'react-native';
const primaryColor = '#36ADE3';
module.exports = StyleSheet.create({
  contents: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: '#FFF',
    alignItems: 'center',
    justifyContent: 'center'
},
scrollContents:{
  backgroundColor: '#fff',
  padding: 10,
  alignItems: 'center'
},
profileImageContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 20
},
profileImageContent: {
    width: 100,
    height: 100,
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#E7E7E8',
    margin: 10
},
profileImage: {
    height: 100,
    borderRadius: 50,
    width: 100
},
profileImageEditIcon: {
    backgroundColor: '#32D1C0',
    padding: 10,
    borderRadius: 100,
    position: 'absolute',
    left: 70,
    zIndex: 200,
    paddingLeft:12,
    paddingRight:12
},
notificationsHeader:{
  marginLeft:10, marginRight:10, padding: 10, borderBottomWidth: 0, borderColor: '#bfbfbf'
},
textBold:{
  fontWeight: 'bold'
},
profileButtonContainer:{
  flex: 1,
  flexDirection: 'row',
  alignItems: 'center'
},
editProfileButton:{
  backgroundColor: "#00D2C4",
  flex: 1
},
viewPhotosButton:{
  backgroundColor: "#00ADE3",
  flex: 1
},
profileButtonText:{
  color: '#FFF',
  padding: 15,
  flex: 1,
  textAlign: 'center',
  fontSize: 15
},
profileButtonIcon:{
  color: '#FFF',
  alignSelf: 'center',
  fontSize: 15
},
notificationsContainer:{
  flex:1
},
followCounters:{
  fontWeight: '700',
  marginBottom: 20
},
profileDetailsContainer:{
  flex: 1,
  justifyContent: 'center',
  paddingLeft: 20,
  paddingRight: 20
},
profileDetailsContents:{
  alignItems: 'center',
  marginBottom: 20
},
displayName:{
  fontSize: 20
}
})
