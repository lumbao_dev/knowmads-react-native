import React, {Component, PropTypes} from 'react'
// TODO: Organize imports to be aranged alphabetically
import {
    Text,
    View,
    Navigator,
    StyleSheet,
    TouchableHighlight,
    TouchableOpacity,
    Alert,
    TextInput,
    Button,
    ScrollView,
    Dimensions,
    InteractionManager,
    Image
} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import StarRating from 'react-native-star-rating'
import Display from 'react-native-display'
import StarRatingStatic from '../../components/StarRatingStatic/'
import {connect} from 'react-redux'
import {Actions} from 'react-native-router-flux'
// import Firestack from 'react-native-firestack'
import OnButtonPress from '../../components/OnButtonPress/'
const styles = require('../../components/GlobalStyles/Main')
const starActiveColor = '#FBE645'
const starInactiveColor = '#CCCCCC'
// const firestack = new Firestack();

var width = Dimensions.get('window').width; //full width

import RNFirebase from 'react-native-firebase'
const configurationOptions = {
    debug: false
  }
  
firebase = RNFirebase.initializeApp(configurationOptions);

@connect((store)=>{
    return {
        user: store.user.user
    }
})
export default class PreviewPlace extends Component {
    constructor(props) {
        super(props)
        this.state = {
            starCount: 0,
            placeAuthor: null,
            placeAuthorPhotoUrl: 'https://firebasestorage.googleapis.com/v0/b/carbon-syntax-155109.appspot.com/o/photos%2Fprofile%2Favatar-default.png?alt=media&token=aaf56f25-3f4e-4728-ad06-ac87d4496ca8',
            rating: 0,
            ratingCount: 0
            //deviceWidth: Dimensions.get('window').width //full width
        }
    }

    onStarRatingPress(rating) {
        this.setState({starCount: rating});        
        var transactionString = '{"'+this.props.user.uid+'":'+rating+'}';
        firebase.database().ref('places/'+this.props.previewPlaceKey+'/rating').update(JSON.parse(transactionString))
      
        firebase.database().ref('feed/').push().then((res) => {
        let newPostKey = res.key;
        firestack.ServerValue.then(map => {
          const postData = {
            timestamp: map.TIMESTAMP,
            type: 'review',
            author: this.props.user.uid,
            placeid: this.props.previewPlaceKey,
            rating: rating,
            views: 0
          }
          let updates = {}
          updates['feed/' + newPostKey] = postData;
          firebase.database().ref().update(updates).then(() => {

          }).catch((error) => {
            console.log(error.message);
          });
        })
      })        
    }

    calculateStars(arrayObj) {
        if(JSON.stringify(arrayObj)!='null'){
            let count = Object.keys(arrayObj).length;
            let curr = 0;
            let max = 0;
            Object.keys(arrayObj).map(function(key) {
            //console.log("starval: "+arrayObj[key]);
            curr += arrayObj[key];
            max += 5
            });
            return Math.floor(((curr / max) * 50 + 50) * 0.05)
        }else{
            return 0
        }
    }

    lookUpAuthor(){
        const authorId = this.props.previewPlaceData.author;
        const app = this;
        var authorName = "";
        var authorPhotoUrl = "";
        return firebase.database().ref('users/'+authorId+'/displayName')
        .once('value')
        .then((snapshot)=>{
            authorName = snapshot.val()
            return snapshot
        })
        .then(()=>{
            return firebase.database().ref('users/'+authorId+'/photoUrl')
            .once('value')
            .then((snapshot)=>{
                authorPhotoUrl = snapshot.val()
                return snapshot
            })            
        })
        .then((snapshot)=>{
            app.setState({placeAuthor: authorName})
            app.setState({placeAuthorPhotoUrl: authorPhotoUrl})
            return snapshot
        })
    }

    componentWillMount() {

    }

    componentDidMount(){
        const app =this;
        app.lookUpAuthor();
        if(typeof app.props.previewPlaceData.rating != 'undefined'){
            app.setState({rating: app.calculateStars(app.props.previewPlaceData.rating)})
            app.setState({ratingCount: Object.keys(app.props.previewPlaceData.rating).length})
        }


        
        InteractionManager.runAfterInteractions(() => {
            firebase.database().ref('places/'+this.props.previewPlaceKey+'/rating').on('value', (snapshot)=>{
                app.setState({rating: app.calculateStars(snapshot.val())})
            })
        })
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.contents}>
                    <ScrollView>
                        <Image style={{
                            flex: 1,
                            width: null,
                            alignSelf: 'stretch',
                            alignItems: 'center',
                            borderWidth: 0,
                            maxHeight: 240,
                            minHeight: 240
                        }} source={{uri:this.props.previewPlaceData.featureImage}}>
                            <View style={{
                                backgroundColor: 'rgba(0,0,0,0.4)',
                                padding: 20,
                                alignItems: 'flex-start',
                                flex: 1,
                                alignSelf: 'stretch'
                            }}>
                                <View style={{
                                    flex: 1,
                                    alignItems: 'flex-start'
                                }}>
                                    <Text style={{
                                        fontSize: 23,
                                        color: "#FFF",
                                        marginTop: 150
                                    }}>{this.props.previewPlaceData.displayName}</Text>

                                    <View style={{
                                        flexDirection: 'row',
                                        alignItems: 'center'
                                    }}>
                                        <StarRatingStatic stars={this.state.rating}/>
                                        <Text style={{
                                            color: "#FFF"
                                        }}>{" "+this.state.ratingCount}</Text>
                                    </View>
                                </View>
                            </View>
                        </Image>
                        <Text style={{
                            padding: 20,
                            textAlign: 'left'
                        }}>{this.props.previewPlaceData.description}</Text>
                        <View style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            paddingLeft: 10,
                            paddingRight: 50
                        }}>
                            <Image style={{
                                width: 40,
                                height: 40,
                                marginRight: 10,
                                marginLeft: 10
                            }} source={require('../../assets/spot-marker.png')}></Image>
                            <Text style={{
                                maxWidth: width - 100
                            }}>{this.props.previewPlaceData.displayName}</Text>
                        </View>
                        <View style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            paddingLeft: 10
                        }}>
                            <Image style={{
                                width: 40,
                                height: 40,
                                marginRight: 10,
                                marginLeft: 10
                            }} source={require('../../assets/phone.png')}></Image>
                            <Text style={{
                                maxWidth: width - 100
                            }}>{this.props.previewPlaceData.phone}</Text>
                        </View>
                        <View style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            paddingLeft: 10
                        }}>
                            <Image style={{
                                width: 40,
                                height: 40,
                                marginRight: 10,
                                marginLeft: 10
                            }} source={require('../../assets/email.png')}></Image>
                            <Text>{this.props.previewPlaceData.email}</Text>
                        </View>
                        <Text style={{
                            padding: 20,
                            textAlign: 'left',
                            color: "#000",
                            fontSize: 14
                        }}>{"Authored by"}</Text>
                            <View style={{
                                flexDirection: 'row',
                                alignItems: 'center',
                                marginLeft: 20,
                                justifyContent: 'flex-start'
                            }}>
                                <Image style={{
                                    width: 50,
                                    height: 50,
                                    borderRadius: 50,
                                    margin: 3
                                }} source={{uri:this.state.placeAuthorPhotoUrl}}></Image>
                                <Text style={{
                                    fontSize: 20, marginLeft: 5
                                }}>{this.state.placeAuthor}</Text>
                                <Text style={{
                                    fontSize: 20,
                                    color: '#36ADE3'
                                }}>{""}</Text>
                            </View>


                        <View style={{
                            justifyContent: 'center'
                        }}>
                            <Text style={{
                                padding: 20,
                                textAlign: 'left',
                                color: "#000",
                                fontSize: 14
                            }}>{"Rate this place"}</Text>

                            <View style={{
                                flex: 1,
                                maxWidth: 230,
                                alignSelf: 'center',
                                marginBottom: 30
                            }}>
                                <StarRating 
                                disabled={false} 
                                maxStars={5} 
                                rating={this.state.starCount} 
                                selectedStar={(rating) => this.onStarRatingPress(rating)} 
                                starColor={starActiveColor} 
                                emptyStar={"star"} 
                                emptyStarColor={starInactiveColor} 
                                starSize={40}/>
                            </View>
                        </View>

                        <View style={{
                            flex: 1,
                            height: 5,
                            borderBottomWidth: 1,
                            borderColor: 'rgba(0,0,0,0.1)'
                        }}></View>
<Display enable={false}>
                      <TouchableOpacity style={{
                            margin: 10,
                            padding: 20,
                            borderRadius: 10
                        }} onPress={OnButtonPress}>
                            <Text style={{
                                textAlign: 'center',
                                color: '#36ADE3',
                                fontSize: 20
                            }}>{"Add a comment"}</Text>
                        </TouchableOpacity>
</Display>


                    </ScrollView>
                </View>
            </View>
        )
    }
}
