import React, {Component, PropTypes} from 'react';
// TODO: Organize imports to be aranged alphabetically
import {
    Text,
    View,
    Navigator,
    StyleSheet,
    TouchableHighlight,
    Alert,
    TextInput,
    ScrollView,
    Button,
    Image,
    DatePickerAndroid,
    ActivityIndicator,
    TouchableOpacity,
    Picker,
    AsyncStorage
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import ImagePicker from 'react-native-image-crop-picker';
// import Firestack from 'react-native-firestack';
import {Actions} from 'react-native-router-flux';
import * as Animatable from 'react-native-animatable';
import Display from 'react-native-display';
import OnButtonPress from '../../components/OnButtonPress/';
import {connect} from 'react-redux';
const animate = require('../../components/UIAnimations/');
const nationalities = require('../../components/Nationalities/');
const styles = require('./style.js');
const Item = Picker.Item;

const imagePickerOptions = {
    title: 'Select Profile Photo',
    storageOptions: {
        skipBackup: true,
        path: 'images'
    },
    customButtons: [
        {
            name: 'fb',
            title: 'Choose Photo from Facebook'
        }
    ],
    quality: 0.1,
    videoQuality: 'low'
};

// const firestackOptions = {
//     debug: true,
//     storageBucket: 'gs://carbon-syntax-155109.appspot.com/'
// };

// const firestack = new Firestack(firestackOptions);

import RNFirebase from 'react-native-firebase'
const configurationOptions = {
    debug: false,
    storageBucket: 'gs://carbon-syntax-155109.appspot.com/'
  }
  
firebase = RNFirebase.initializeApp(configurationOptions);

@connect((store)=>{
  return {
    user: store.user.user
  }
})

export default class EditProfile extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: this.props.data,
            profileImage: {uri: 'https://firebasestorage.googleapis.com/v0/b/carbon-syntax-155109.appspot.com/o/photos%2Fprofile%2Favatar-default.png?alt=media&token=aaf56f25-3f4e-4728-ad06-ac87d4496ca8'},
            firstName: '',
            lastName: '',
            email: '',
            birthday: 'BIRTHDAY',
            profileImagePath: '',
            loading: false,
            photoUrl: '',
            inputTextActiveBirthday: 'rgba(0,0,0,0.4)',
            inputTextActiveCountry: 'rgba(0,0,0,0.4)',
            nationality: 'None',
            modalVisible: false,
            errorString: '',
            displayName: '',
            showActionSheet: false,
            actionSheetMode: "",
            actionSheetSliderIsClosed: true,
        }
    }

    toggleActionSheet() {
        let toggle = !this.state.showActionSheet;
        this.setState({ showActionSheet: toggle });
        this.state.actionSheetSliderIsClosed == true
            ? this.refs.actionSheet.mapActionSheetSlideDown().then(this.setState({ actionSheetSliderIsClosed: false }))
            : this.refs.actionSheet.mapActionSheetSlideUp().then(this.setState({ actionSheetSliderIsClosed: true }))
    }

    async setUser(user){
      const app = this;

      app.setState({displayName: user.displayName});
      // console.log(evt.user);
      app.setState({email: user.email});
      app.setState({profileImage: {uri:user.photoUrl}});

      firebase.database().ref('/users/' + user.uid).once('value').then((snapshot)=>{
        var user = snapshot.val();
        var empty = "Not Set"
        // console.log(snapshot.val());

        // Check if user details are null, if true, set to empty.
        if(user.firstName==null){
          app.setState({firstName: null})
        }else{
          app.setState({firstName: user.firstName})
        }

        if(user.lastName==null){
          app.setState({lastName: null})
        }else{
          app.setState({lastName: user.lastName})
        }

        // if(user.email==null){
        //   app.setState({email: null})
        // }else{
        //   app.setState({email: user.email})
        // }

        if(user.birthday!=null){
          app.setState({inputTextActiveBirthday:'rgba(0,0,0,1)'})
          app.setState({birthday: user.birthday})
        }

        // if(user.displayName!=null){
        //   app.setState({displayName: user.displayName})
        // }else{
        //   app.setState({displayName: app.state.data.displayName})
        // }

        if(user.nationality!=null){
          app.setState({nationality: empty})
        }

        if(user.photoUrl==null){
          // NOTE: Method to use if user used a different auth provider other than email.
          app.setState({profileImage: {uri:'https://firebasestorage.googleapis.com/v0/b/carbon-syntax-155109.appspot.com/o/photos%2Fprofile%2Favatar-default.png?alt=media&token=aaf56f25-3f4e-4728-ad06-ac87d4496ca8'}}) // This is to avoid image source warnings.
          // TODO: Disabled for bugs...use another method.
          // firestack.auth.listenForAuth(function(evt) {
          //   app.setState({profileImage: {uri:evt.user.photoUrl}});
          // });
        }else{
          app.setState({profileImage: {uri:user.photoUrl}})
        }

        if(user.nationality==null){
          app.setState({nationality: null})
        }else{
          app.setState({nationality: user.nationality})
        }
      })
    }

    componentWillUnmount() {
        // TODO: Disabled for bugs...use another method.
        // firestack.auth.unlistenForAuth();
    }

    componentDidMount(){
      const app = this;
      app.setUser(app.props.user); 
    }

    /**
    * TODO: Use react-native-image-crop-picker here.
    */
    launchImagePicker() {
        const parent = this;
        Alert.alert('Set Your Profile Picture', "Please select an option below.", [
            {
                text: 'Cancel',
                onPress: () => console.log('Operation canceled.')
            }, {
                text: 'Gallery',
                onPress: () => parent.launchPickerFromGallery()
            }, {
                text: 'Camera',
                onPress: () => parent.launchPickerFromCamera()
            }
        ]);
    }

    emailIsValid(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    launchPickerFromCamera() {
      this.toggleActionSheet();
        ImagePicker.openCamera({width: 300, height: 300, cropping: true}).then(source => {
            console.log(source);
            this.setState({
                profileImage: {
                    uri: source.path
                }
            });
            this.setState({profileImagePath: source.path});
        });
    }

    launchPickerFromGallery() {
      this.toggleActionSheet();
        ImagePicker.openPicker({width: 300, height: 300, cropping: true}).then(source => {
            console.log(source);
            this.setState({
                profileImage: {
                    uri: source.path
                }
            });
            this.setState({profileImagePath: {uri:source.path}});
        });
    }

    async pickDate() {
        try {
            const {action, year, month, day} = await DatePickerAndroid.open();
            if (action === DatePickerAndroid.dismissedAction) {
                console.log("dismissed");
            } else {
                var date = new Date(year, month, day);
                this.setState({birthday: date.toLocaleDateString()})
                this.setState({inputTextActiveBirthday: "rgba(0,0,0,1)"})
            }
        } catch ({code, message}) {
            console.warn(`Error in example: `, message);
        }
    }

    updateAccount() {
        // Validate User Details
        var error = 0;
        var error_messages = "";
        const parent = this;
        const uid = this.state.data.uid;

        if (!this.emailIsValid(this.state.email)) {
            error_messages += "The email you entered is invalid. \n";
            error += 1;
        }

        if (this.state.firstName == '') {
            error_messages += "Your first name is empty. \n";
            error += 1;
        }

        if (this.state.lastName == '') {
            error_messages += "Your last name is empty.  \n";
            error += 1;
        }

        if (this.state.birthday == '') {
            error_messages += "Your birthday is invalid. \n";
            error += 1;
        }

        if (error > 0) {
            Alert.alert("An Error Occured", error_messages, [
              {
                text: 'Retry', onPress: () => {error_messages = ""}
              }
            ]);
        } else {
          this.setState({loading:true});
          console.log(this.state.profileImagePath.uri);
          if(this.state.profileImagePath.uri!=null){
            // Upload user profile photo.
            firebase.storage.uploadFile('photos/profile/' + uid, this.state.profileImagePath.uri, {
                contentType: 'image/jpeg',
                contentEncoding: 'base64'
            }).then((res) => {
                // Get profile photo link from storage.
                const storageRef = firebase.storage.ref('photos/profile/' + uid);
                storageRef.downloadUrl().then((res) => {
                    // Update user profile with photoUrl included.
                    firebase.database().ref('users/' + uid).update({
                      photoUrl: res.fullPath,
                      firstName: this.state.firstName,
                      lastName: this.state.lastName,
                      birthday: this.state.birthday,
                      nationality: this.state.nationality,
                      email: this.state.email
                    });
                });
                // Update user profile using Firebase auth.
                firebase.auth().currentUser.updateUserProfile({
                    displayName: parent.state.displayName,
                    photoUrl: parent.state.photoUrl
                }).then(() => {
                    //navigate('Profile', 'right', this.props.navigator, 'pop', '');
                });
            }).catch(err => console.log('There was an error uploading the file', err));
          }else{

            firebase.database().ref('users/' + uid).update({
              firstName: this.state.firstName,
              lastName: this.state.lastName,
              birthday: this.state.birthday,
              nationality: this.state.nationality,
              email: this.state.email
            });

            firebase.auth().currentUser.updateUserProfile({
                displayName: parent.state.displayName,
                photoUrl: parent.state.photoUrl
            }).then(() => {
                Actions.pop()
            });

          }
        }
    }

    render() {
        if (this.state.loading) {
            return (<ActivityIndicator size='large' style={styles.loadingActivity}/>);
        } else {
            return (
                <View style={styles.container}>
                    <ScrollView>
                        <View style={styles.profileImageContainer}>
                            <View style={styles.profileImageContent}>
                                <Image style={styles.profileImage} resizeMethod="scale" source={this.state.profileImage}></Image>
                                <TouchableHighlight style={styles.profileImageEditIcon} onPress={this.toggleActionSheet.bind(this)}>
                                    <Icon name="pencil" style={{
                                        fontSize: 15,
                                        color: '#FFF'
                                    }}></Icon>
                                </TouchableHighlight>
                            </View>
                        </View>
                        <View style={{
                            marginTop: 0,
                            padding: 20,
                            marginBottom: 20
                        }}>
                        <View style={styles.inputGroup}>
                            <Icon name="id-card-o" style={{width:20}}></Icon><TextInput style={styles.textInputDefault} placeholder={"USER NAME"} underlineColorAndroid="transparent" value={this.state.displayName} onChangeText={(text) => this.setState({displayName: text})}/>
                        </View>
                            <View style={styles.inputGroup}>
                                <Icon name="user-o" style={{width:20}}></Icon><TextInput style={styles.textInputDefault} placeholder={"FIRST NAME"} underlineColorAndroid="transparent" value={this.state.firstName} onChangeText={(text) => this.setState({firstName: text})}/>
                            </View>
                            <View style={styles.inputGroup}>
                                <Icon name="user" style={{width:20}}></Icon><TextInput style={styles.textInputDefault} placeholder={"LAST NAME"} underlineColorAndroid="transparent" value={this.state.lastName} onChangeText={(text) => this.setState({lastName: text})}/>
                            </View>
                            <View style={styles.inputGroup}>
                                <Icon name="envelope-o" style={{width:20}}></Icon><TextInput style={styles.textInputDefault} placeholder={"EMAIL"} underlineColorAndroid="transparent" value={this.state.email} onChangeText={(text) => {
                                    var e = text.replace(/\s/g, '');
                                    this.setState({email: e});
                                }}/>
                            </View>
                            <View style={styles.inputPickerGroup}>
                                <TouchableOpacity style={{
                                    paddingTop: 10,
                                    paddingBottom: 10,
                                    paddingLeft: 0,
                                    flexDirection: 'row',
                                    alignItems: 'center'
                                }} onPress={this.pickDate.bind(this)}>
                                    <Icon name="birthday-cake" style={{width:25}}></Icon>
                                    <Text style={{
                                        color: this.state.inputTextActiveBirthday
                                    }}>{this.state.birthday}</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{
                                borderBottomWidth: 0.5,
                                borderColor: "rgba(0,0,0,0.1)",
                                paddingTop: 10
                            }}>
                                <View style={{flexDirection:'row', alignItems:'center'}}>
                                  <Icon name="flag-o" style={{width:25}}></Icon>
                                  <Text style={{color: this.state.inputTextActiveCountry,fontSize: 11}}>{"NATIONALITY"}</Text>
                                </View>
                                <Picker selectedValue={this.state.nationality} onValueChange={(c) => {
                                    this.setState({nationality: c});
                                    this.setState({inputTextActiveCountry: 'rgba(0,0,0,1)'})
                                }} mode="dialog" prompt="Select Your Nationality">
                                    {nationalities.map((res, i) => (<Item key={i} label={res.value} value={res.value} style={{
                                        color: this.state.inputTextActiveCountry
                                    }}/>))}
                                </Picker>
                            </View>
                            <View style={styles.spacer}></View>
                        </View>
                    </ScrollView>

                    <Display enable={this.state.showActionSheet} style={styles.modalBackground}>
                        <TouchableHighlight style={{
                            flex: 1
                        }} onPress={this.toggleActionSheet.bind(this)} underlayColor="rgba(0,0,0,0.2)">
                            <View></View>
                        </TouchableHighlight>

                    </Display>

                    <Animatable.View duration={300} ref="actionSheet" style={styles.actionSheetContainer}>
                        <View style={styles.actionSheetContent}>
                            <Text style={styles.actionSheetTitle}>{"Set Your Profile Photo"}</Text>
                            <View style={styles.actionSheetLine}></View>
                            <TouchableHighlight underlayColor="#CCC" onPress={this.launchPickerFromCamera.bind(this)}>
                                <View style={styles.actionSheetItem}>
                                  <Icon name="camera"></Icon>
                                    <Text style={styles.actionSheetText}>{"Camera"}</Text>
                                </View>
                            </TouchableHighlight>
                            <TouchableOpacity underlayColor="#CCC" onPress={this.launchPickerFromGallery.bind(this)}>
                                <View style={styles.actionSheetLastItem}>
                                    <Icon name="th"></Icon>
                                    <Text style={styles.actionSheetText}>{"Gallery"}</Text>
                                </View>
                            </TouchableOpacity>
                        </View>

                        <TouchableHighlight style={styles.actionSheetDismiss} underlayColor="#CCC" onPress={this.toggleActionSheet.bind(this)}>
                            <View>
                                <Text style={styles.actionSheetDismissText}>{"Cancel"}</Text>
                            </View>
                        </TouchableHighlight>
                    </Animatable.View>

                </View>
            )
        }

    }
}
