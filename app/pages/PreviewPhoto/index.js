import React, { Component } from 'react'
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    Image,
    Modal
} from 'react-native'
import MapView from 'react-native-maps'
import Icon from 'react-native-vector-icons/FontAwesome'
import { Actions } from 'react-native-router-flux'
import ImageViewer from 'react-native-image-zoom-viewer'
const styles = require('../../components/GlobalStyles/Main.js')
const colors = require('../../components/GlobalStyles/ColorPalette.js')

export default class PreviewPhoto extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {

    }

    render() {
        return (
            <View style={{flex:1}}>
                <ImageViewer 
                imageUrls={this.props.data}
                onClick={()=>{close()}}
                style={{backgroundColor: 'rgba(0,0,0,0.5)'}}
                />
            </View>
        )
    }
}
