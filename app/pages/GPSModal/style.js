import {StyleSheet} from 'react-native';
const primaryColor = '#36ADE3';
module.exports = StyleSheet.create({
  container:{
    margin: 10,
    flexDirection: 'row',
    alignItems: 'center'
  },
  contents:{
    flexDirection: 'column',
    flex: 0.8
  },
  text:{
    fontSize: 20
  },
  description:{
    flexWrap: 'wrap'
  }
})
