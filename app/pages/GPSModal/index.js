import React, { Component } from 'react';
import { View, Text} from 'react-native';
import { Actions } from 'react-native-router-flux';
const styles = require('./style.js');
//<Image source={require('../../assets/spot-marker.png')}/>
export default class GPSModal extends Component {

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.contents}>
          <Text style={styles.text}>{"Enable Location"}</Text>
          <Text style={styles.description} numberOfLines={5}>{"Your Locations Settings is set to 'Off'. Please Enable Location to fully use this app feature."}</Text>
        </View>
      </View>
    )
  }
}
