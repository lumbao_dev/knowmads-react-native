import {StyleSheet} from 'react-native';
const colors = require('../../components/GlobalStyles/ColorPalette.js')

module.exports = StyleSheet.create({
  navBar: {
    backgroundColor: colors.DeepSkyBlue,
    borderWidth: 0,
    paddingRight:10
  },
  navTitle:{
    color: colors.White,
    borderColor: 'rgba(0,0,0,0.1)'
  },
  tabBar:{
    borderTopWidth: 0.5,
    borderColor: 'rgba(0,0,0,0.2)'
  },
  icons: {
    width: 20,
    height: 20,
    resizeMode: 'contain',
    padding: 5
  }
})
