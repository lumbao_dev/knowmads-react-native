/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react'
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  Platform
} from 'react-native';
import {
  Scene,
  Reducer,
  Router,
  Switch,
  Modal,
  Actions,
  ActionConst,
  Stack,
  Tabs,
  Lightbox
} from 'react-native-router-flux'
import Icon from 'react-native-vector-icons/FontAwesome'
import { connect } from "react-redux"
import API from '../../api/'
import Onboarding from '../Onboarding/'
import Feed from '../Feed/'
import Profile from '../Profile/'
import Messages from '../Messages/'
import Auth from '../Auth/'
import NewAccount from '../NewAccount/'
import Map from '../Map/'
import AddPlace from '../AddPlace/'
import Track from '../Track/'
import MapSettings from '../MapSettings/'
import EditProfile from '../EditProfile/'
import PreviewProfile from '../PreviewProfile/'
import FilterButtonIcon from '../../components/FilterButtonIcon/'
import TabButtonIcon from '../../components/TabButtonIcon/'
import StatusModal from '../../components/StatusModal/'
import PreviewPlace from '../PreviewPlace/'
import PreviewCheckin from '../PreviewCheckin/'
import PreviewPhoto from '../PreviewPhoto/'
import NewMessage from '../NewMessage/'
import Chat from '../Chat/'
import Splash from '../Splash/'

const colors = require('../../components/GlobalStyles/ColorPalette.js')

const styles = require('./style.js');

const backButtonIcon = require('../../assets/back-icon.png');

// Handles the tab icon states and icon to display.
// const TabIcon = ({ selected, title }) => {
//   switch (title) {
//     case "Knowmads Map": return selected ? (<TabButtonIcon type={'map-active'} />) : (<TabButtonIcon type={'map-inactive'} />); break;
//     case "Feed": return selected ? (<TabButtonIcon type={'feed-active'} />) : (<TabButtonIcon type={'feed-inactive'} />); break;
//     case "Profile": return selected ? (<TabButtonIcon type={'profile-active'} />) : (<TabButtonIcon type={'profile-inactive'} />); break;
//     case "Messages": return selected ? (<TabButtonIcon type={'message-active'} />) : (<TabButtonIcon type={'message-inactive'} />); break;
//     default: return (<TabButtonIcon type={'map-active'} />)
//   }
// }

//Create a dedicated class that will manage the tabBar icon
class TabIcon extends Component {
  // componentWillReceiveProps(){
  //   console.log(this.props)
  // }
  render() {
  switch (this.props.title) {
    case "Knowmads Map": return this.props.selected ? (<TabButtonIcon type={'map-active'} />) : (<TabButtonIcon type={'map-inactive'} />); break;
    case "Feed": return this.props.selected ? (<TabButtonIcon type={'feed-active'} />) : (<TabButtonIcon type={'feed-inactive'} />); break;
    case "Profile": return this.props.selected ? (<TabButtonIcon type={'profile-active'} />) : (<TabButtonIcon type={'profile-inactive'} />); break;
    case "Messages": return this.props.selected ? (<TabButtonIcon type={'message-active'} />) : (<TabButtonIcon type={'message-inactive'} />); break;
    default: return (<TabButtonIcon type={'map-active'} />)
  }
  }
}

// TODO: Should be added on top "TabIcon method"
// API.backpackerProfileNotifications(app.props.data.uid).on('value', (snapshot) => {
//   let counter = 0;
//   let data = snapshot.val();
//   if (snapshot.val() != null) {
//     Object.keys(data).map((key) => {
//       if (!data[key].reviewed) {
//         counter += 1;
//       }
//     })
//   };
//   app.setState({profileNotifications: counter})
// })

const cancelBack = () => {
  return false
}

const renderFilterButton = (props) => {
  return (
    <TouchableOpacity onPress={Actions.mapSettings}>
      <FilterButtonIcon />
    </TouchableOpacity>
  );
}

const renderResetFiltersButton = (props) => {
  return (
    <TouchableOpacity
      onPress={() => {
        //API.resetMapFilters(props);
        Actions.mapSettings({ type: ActionConst.RESET })
      }}
    >
      <Text style={{ color: '#FFF' }}>{"Reset All"}</Text>
    </TouchableOpacity>
  );
}

const renderSignOutButton = (props) => {
  return (
    <TouchableOpacity onPress={() => API.signOut()}>
      <Icon name="sign-out" style={{ color: '#FFF', fontSize: 25 }} />
    </TouchableOpacity>
  )
}

const renderSearchButton = (props) => {
  return (
    <TouchableOpacity onPress={() => API.signOut()}>
      <Icon name="search" style={{ color: '#FFF', fontSize: 25 }} />
    </TouchableOpacity>
  )
}

const renderAddMessageButton = (props) => {
  return (
    <TouchableOpacity onPress={() => Actions.newMessage()}>
      <Icon name="plus" style={{ color: '#FFF', fontSize: 25 }} />
    </TouchableOpacity>
  )
}

const testComponent = () => {
  return (
    <View style={{
      position:'absolute', 
      backgroundColor: 'rgba(0,0,0,0.5)',
      ...StyleSheet.absoluteFillObject,
        flex: 1,
        top: 0,
        bottom: 0,
        left: 0,
        right: 0
      }}>
    <TouchableOpacity onPress={()=>Actions.pop()}>
    <Text>Test</Text>
      </TouchableOpacity>
    </View>
  )
}

@connect((store) => {
  return {
    user: store.user.user
  };
})
export default class Container extends Component {

  constructor(props) {
    super(props)
  }

  render() {
    return (
      <Router navigationBarStyle={styles.navBar} titleStyle={styles.navTitle}>
        <Lightbox>
        <Scene key="root" hideNavBar={true}>
          {/*Home*/}
          <Scene key="home">
            <Tabs
              key="tabbar"
              swipeEnabled
              showLabel={false}
              activeBackgroundColor="white"
              inactiveBackgroundColor="rgba(255,255,255,0.1)"
              tabBarPosition="bottom"
              activeTintColor="#000"
              lazy={false}
              style={styles.tabBar}
            >
              <Scene
                key="knowmadsMap" 
                icon={TabIcon} 
                component={Map}
                title="Knowmads Map"
                renderRightButton={renderFilterButton}
                />
              <Scene key="feed" icon={TabIcon} component={Feed} title="Feed" />
              <Scene 
              key="profile" 
              icon={TabIcon} 
              component={Profile} 
              title="Profile" 
              renderRightButton={renderSignOutButton}
              />
              <Scene 
              key="messages" 
              icon={TabIcon} 
              component={Messages} 
              title="Messages" 
              renderRightButton={renderAddMessageButton}
              />
            </Tabs>
          </Scene>
          {/*Misc Scenes*/}
          <Scene key="splash" component={Splash} tabs={false} hideNavBar initial={true} />
          <Scene key="onboarding" component={Onboarding} title="Onboarding" tabs={false} hideNavBar initial={false} />
          <Scene key="auth" component={Auth} type={ActionConst.REPLACE} title="Sign In" tabs={false} hideNavBar />
          <Scene key="newAccount" component={NewAccount} title="Sign Up" tabs={false} hideNavBar={false} backButtonImage={backButtonIcon} />
          <Scene key="mapSettings" component={MapSettings} hideNavBar={false} direction="vertical" title="Filters" tabs={false} backButtonImage={backButtonIcon} />
          <Scene key="addPlace" component={AddPlace} hideNavBar={false} direction="vertical" title="Places" tabs={false} backButtonImage={backButtonIcon} />
          <Scene key="editProfile" component={EditProfile} hideNavBar={false} direction="vertical" title="Edit Profile" tabs={false} backButtonImage={backButtonIcon} />
          <Scene key="previewProfile" component={PreviewProfile} hideNavBar={false} direction="vertical" title="Backpacker" tabs={false} backButtonImage={backButtonIcon} />
          <Scene key="track" component={Track} hideNavBar={false} direction="horizontal" title="Track" tabs={false} backButtonImage={backButtonIcon} />
          <Scene key="previewPlace" component={PreviewPlace} hideNavBar={false} direction="horizontal" title="Places" tabs={false} backButtonImage={backButtonIcon} />
          <Scene key="previewCheckin" component={PreviewCheckin} hideNavBar={false} direction="horizontal" title="Map" tabs={false} backButtonImage={backButtonIcon} />
          <Scene key="previewPhoto" component={PreviewPhoto} hideNavBar={true} direction="vertical" title="Photo" tabs={false} backButtonImage={backButtonIcon} />
          <Scene key="newMessage" component={NewMessage} hideNavBar={false} direction="horizontal" title="New Message" tabs={false} backButtonImage={backButtonIcon} />
          <Scene key="chat" component={Chat} type={ActionConst.PUSH} hideNavBar={false} direction="horizontal" title="Chat" tabs={false} backButtonImage={backButtonIcon} />

          {/*Reserved for special modals.*/}
          <Scene key="statusModal" component={StatusModal} direction="vertical" hideNavBar />
        </Scene>
      {/*Testing component only.*/}
        <Scene key="xxx" component={testComponent}/>
        </Lightbox>
      </Router>
    )
  }
}
