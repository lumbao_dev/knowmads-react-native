import React, { Component } from 'react'
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    Image
} from 'react-native'
import MapView from 'react-native-maps'
import Icon from 'react-native-vector-icons/FontAwesome'
import { Actions } from 'react-native-router-flux'
const styles = require('../../components/GlobalStyles/Main.js')
const colors = require('../../components/GlobalStyles/ColorPalette.js')

export default class PreviewCheckin extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {

    }

    render() {
        return (
            <View style={styles.mapContainer}>
                <MapView
                    style={{ ...StyleSheet.absoluteFillObject, }}
                    initialRegion={{
                        latitude: this.props.data.placeDetails.location.latitude,
                        longitude: this.props.data.placeDetails.location.longitude,
                        latitudeDelta: 0.0922,
                        longitudeDelta: 0.0421,
                    }}
                    onPress={() => this.marker.showCallout()}
                    onRegionChangeComplete={() => this.marker.showCallout()}
                >
                    <MapView.Marker
                        ref={marker => (this.marker = marker)}
                        coordinate={{
                            latitude: this.props.data.placeDetails.location.latitude,
                            longitude: this.props.data.placeDetails.location.longitude
                        }}
                        title={this.props.data.authorDetails.displayName + " was here!"}
                    >
                    </MapView.Marker>
                </MapView>
            </View>
        )
    }
}
