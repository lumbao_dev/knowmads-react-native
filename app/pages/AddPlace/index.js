import React, {Component, PropTypes} from 'react'
// TODO: Organize imports to be aranged alphabetically
import {
    Text,
    View,
    Navigator,
    StyleSheet,
    TouchableHighlight,
    Alert,
    TextInput,
    Button,
    ScrollView,
    BackAndroid,
    AsyncStorage,
    TouchableOpacity,
    ActivityIndicator,
    ToastAndroid
} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import Geocoder from 'react-native-geocoder'
import ImagePicker from 'react-native-image-crop-picker'
import RNGooglePlaces from 'react-native-google-places'
import Image from 'react-native-image-progress'
// import Firestack from 'react-native-firestack'
import * as Animatable from 'react-native-animatable'
import {connect} from 'react-redux'
import {Actions} from 'react-native-router-flux'
import Display from 'react-native-display'
import OnButtonPress from '../../components/OnButtonPress/'
import * as types from '../../actions/types'
const animate = require('../../components/UIAnimations/')
import TimerMixin from 'react-timer-mixin'
// const firestack = new Firestack()
const styles = require('../../components/GlobalStyles/Main.js')

import RNFirebase from 'react-native-firebase'
const configurationOptions = {
    debug: false
  }
  
firebase = RNFirebase.initializeApp(configurationOptions);

@connect((store)=>{
  return{
    user: store.user.user,
    addAttraction: store.mapAction.addAttraction,
    addAccommodation: store.mapAction.addAccommodation
  }
}, (dispatch)=>{
    return{
       accommodationSubmitted: ()=>{
            dispatch({type: types.ADD_ACCOMMODATION_FULFILLED, payload: true})
       },
       attractionSubmitted: ()=>{
           dispatch({type: types.ADD_ATTRACTION_FULFILLED, payload: true})
       },
       processingMapRequestVal: (val)=>{
           dispatch({type: types.PROCESSING_MAP_REQUEST, payload: val})
       },
       toastNotify:(val)=>{
         dispatch({type: types.TOAST_NOTIFICATION, payload: val})
       }

    }
})
export default class AddPlace extends Component {
  mixins: [TimerMixin]
    constructor(props) {
        super(props)
        this.state = {
            currentGeocodeName: 'Tap to change location',
            currentPlaceLocation: 'unknown',
            featureImage: require('../../assets/places-feature-image-background.png'),
            email: '',
            phone: '',
            displayName: '', // Name of the Place
            description: '',
            showActionSheet: false,
            actionSheetMode: '',
            actionSheetSliderIsClosed: true,
            featureImagePath: '',
            loading: false,
            placeLocation: '',
            appid: ''
        }
    }

    // TODO: Geocoding should be treated globally next time.
    async geoCodeAddressFromCoordinate(coordinate) {
        this.setState({infoBarText: "Geocoding your coordinates. Please wait..."})
        var geocoderesult;
        Geocoder.geocodePosition(coordinate).then(res => {
            geocoderesult = res[0].formattedAddress;
            this.setState({currentGeocodeName: geocoderesult})
        }).catch(err => console.log(err))
    }

    loadData() {
      //TODO: Transfer this one using redux.
        AsyncStorage.getItem("currentPlaceLocation").then((value) => {
            if (value != null) {
                var result = JSON.parse(value);
                this.setState({currentPlaceLocation: result});
                this.geoCodeAddressFromCoordinate(result)
            }
        }).done();
    }

    componentWillReceiveProps() {
        this.loadData()
    }

    submitPlace(){
      const app = this;
      app.props.processingMapRequestVal(true);
      var type = app.props.addAttraction===true?"attraction":"accommodation";
      let newPostKey = null;
      Actions.pop();

      // Post to Places
      firebase.database().ref('places').push().then((res) => {
        newPostKey = res.key;
        app.setState({appid:newPostKey});
        let updates = {};

        firestack.ServerValue.then(map => {
          const postData = {
            displayName: app.state.displayName,
            timestamp: map.TIMESTAMP,
            description: app.state.description,
            email: app.state.email,
            phone: app.state.phone,
            location: app.state.placeLocation,
            author: app.props.user.uid,
            placeid: newPostKey,
            type: type.toLowerCase()
           }

           updates['/places/' + newPostKey] = postData;

           firebase.database().ref().update(updates)
         })

         if(app.state.featureImagePath!=''){
           firestack.storage.uploadFile('photos/places/' + newPostKey, this.state.featureImagePath, {
               contentType: 'image/jpeg',
               contentEncoding: 'base64'
           }).then((res) => {
               // Get profile photo link from storage.
               const storageRef = firestack.storage.ref('photos/places/' + newPostKey);
               return storageRef.downloadUrl().then((res_from_storage) => {
                  // res.fullPath
                   updates['/places/' + newPostKey] = {
                     displayName: app.state.displayName,
                     timestamp: res.TIMESTAMP,
                     description: app.state.description,
                     email: app.state.email,
                     phone: app.state.phone,
                     location: app.state.placeLocation,
                     author: app.props.user.uid,
                     featureImage:res_from_storage.fullPath,
                     type: app.props.addAccommodation===true?"accommodation":"attraction"
                   };

                   firebase.database().ref().update(updates);
                   return res_from_storage;
               });
           })
           // Post to feed
           .then((res)=>{
             return firebase.database().ref().push().then((snapshot) => {
               let updates = {};
               var constantKey = snapshot.key;
               firestack.ServerValue.then(map => {
                 const postData = {
                   type: app.props.addAccommodation===true?"accommodation":"attraction",
                   placeid: newPostKey,
                   author: app.props.user.uid,
                   timestamp: map.TIMESTAMP,
                   views: 0
                  }

                  updates['/feed/' + constantKey] = postData;

                  firebase.database().ref().update(updates);
                  firebase.database().ref('users/'+app.props.user.uid).child('feed').push([constantKey]).then(()=>{
                   app.props.processingMapRequestVal(false);
                   ToastAndroid.show('Your status is now updated!', ToastAndroid.SHORT);
                  });

                });
                return snapshot
              });
           })
           .catch((err) =>{
             ToastAndroid.show('Error in updating your status.', ToastAndroid.SHORT);
           })
         }
      });


    }

    launchImagePicker() {
        ImagePicker.showImagePicker(imagePickerOptions, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                let source = {
                    uri: 'data:image/jpeg;base64,' + response.data
                };

                // You can also display the image using data:
                // let source = { uri: 'data:image/jpeg;base64,' + response.data };

                this.setState({featureImage: source});
            }
        });
    }

    toggleActionSheet() {
        let toggle = !this.state.showActionSheet;
        this.setState({ showActionSheet: toggle });
        this.state.actionSheetSliderIsClosed == true
            ? this.refs.actionSheet.mapActionSheetSlideDown().then(this.setState({ actionSheetSliderIsClosed: false }))
            : this.refs.actionSheet.mapActionSheetSlideUp().then(this.setState({ actionSheetSliderIsClosed: true }))
    }

    launchPickerFromCamera() {
      this.toggleActionSheet();
        ImagePicker.openCamera({width: 1366, height: 768, cropping: true}).then(source => {
            // console.log(source);
            this.setState({featureImage: {uri:source.path}});
            this.setState({featureImagePath: source.path });
        });
    }

    launchPickerFromGallery() {
        this.toggleActionSheet();
        ImagePicker.openPicker({width: 300, height: 300, cropping: true}).then(source => {
            // console.log(source);
            this.setState({featureImage: {uri:source.path}});
            this.setState({featureImagePath: source.path });
        });
    }

    openSearchModal() {
      const app = this;
     RNGooglePlaces.openPlacePickerModal().then((place) => {
        console.log(place);
        app.setState({currentGeocodeName: place.address});
        app.setState({placeLocation: place});
        })
        .catch((err) => {
                    Alert.alert("Aw snap!", err.message, [
                {
                    text: 'Ok'
                }
            ]);
        });  // error is a Javascript Error object
      }

    render() {
      if (this.state.loading) {
          return (<ActivityIndicator size='large' style={styles.loadingActivity} />);
      }else{
        return (
            <View style={styles.container}>
                <View style={styles.contents}>
                    <ScrollView>
                        <Image style={styles.placeFeatureImage} source={this.state.featureImage}>
                            <TouchableHighlight style={{
                                backgroundColor: 'rgba(0,0,0,0.2)',
                                borderRadius: 50,
                                width: 50,
                                height: 50
                            }} onPress={this.toggleActionSheet.bind(this)}>
                                <Image source={require('../../assets/upload-feature-image-icon.png')}/>
                            </TouchableHighlight>
                        </Image>

                        <View style={{
                            flex: 1
                        }}>
                            <View style={styles.inputGroup}>
                                <TextInput style={styles.textInputDefault} placeholder={(this.props.addAccommodation===true?"ACCOMMODATION":"ATTRACTION")+" NAME"} underlineColorAndroid="transparent" value={this.state.displayName} onChangeText={(text) => this.setState({displayName: text})}/>
                            </View>

                            <View style={styles.inputGroup}>
                                <TextInput style={styles.textInputDefault} numberOfLines={3} multiline={true} placeholder="DESCRIPTION" underlineColorAndroid="transparent" value={this.state.description} onChangeText={(text) => this.setState({description: text})}/>
                            </View>

                            <TouchableOpacity style={{
                                paddingRight: 5
                            }} onPress={this.openSearchModal.bind(this)}>
                                <View style={[styles.inputGroup, {alignItems:'center'}]}>
                                    <Image source={require('../../assets/spot-marker.png')} style={{
                                        width: 40,
                                        height: 40
                                    }}></Image>
                                  <View style={{flexDirection: 'column', flex: 0.8, alignItems: 'center'}}>
                                    <Text style={{textAlign:'left'}} numberOfLines={3}>{this.state.currentGeocodeName}</Text>
                                  </View>
                                </View>
                            </TouchableOpacity>

                            <View style={styles.inputGroup}>
                                <Image source={require('../../assets/phone.png')} style={{
                                    width: 40,
                                    height: 40
                                }}></Image>
                              <TextInput style={styles.textInputDefault} placeholder="PHONE/MOBILE" underlineColorAndroid="transparent" value={this.state.phone} onChangeText={(text) => this.setState({phone: text})}/>
                            </View>

                            <View style={styles.inputGroup}>
                                <Image source={require('../../assets/email.png')} style={{
                                    width: 40,
                                    height: 40
                                }}></Image>
                              <TextInput style={styles.textInputDefault} placeholder="EMAIL" underlineColorAndroid="transparent" value={this.state.email} onChangeText={(text) => this.setState({email: text})}/>
                            </View>

                            <View style={styles.submitGroup}>
                                <TouchableHighlight style={styles.loginButtonDefault} onPress={this.submitPlace.bind(this)}>
                                    <Text style={{
                                        color: '#FFF',
                                        textAlign: 'center'
                                    }}>{"SUBMIT"}</Text>
                                </TouchableHighlight>
                            </View>
                        </View>
                    </ScrollView>

                    <Display enable={this.state.showActionSheet} style={styles.modalBackground}>
                        <TouchableHighlight style={{
                            flex: 1
                        }} onPress={this.toggleActionSheet.bind(this)} underlayColor="rgba(0,0,0,0.2)">
                            <View></View>
                        </TouchableHighlight>

                    </Display>

                    <Animatable.View duration={300} ref="actionSheet" style={styles.actionSheetContainer}>
                        <View style={styles.actionSheetContent}>
                            <Text style={styles.actionSheetTitle}>{"Set Your Profile Photo"}</Text>
                            <View style={styles.actionSheetLine}></View>
                            <TouchableHighlight underlayColor="#CCC" onPress={this.launchPickerFromCamera.bind(this)}>
                                <View style={styles.actionSheetItem}>
                                  <Icon name="camera"></Icon>
                                    <Text style={styles.actionSheetText}>{"Camera"}</Text>
                                </View>
                            </TouchableHighlight>
                            <TouchableOpacity underlayColor="#CCC" onPress={this.launchPickerFromGallery.bind(this)}>
                                <View style={styles.actionSheetLastItem}>
                                    <Icon name="th"></Icon>
                                    <Text style={styles.actionSheetText}>{"Gallery"}</Text>
                                </View>
                            </TouchableOpacity>
                        </View>

                        <TouchableHighlight style={styles.actionSheetDismiss} underlayColor="#CCC" onPress={this.toggleActionSheet.bind(this)}>
                            <View>
                                <Text style={styles.actionSheetDismissText}>{"Cancel"}</Text>
                            </View>
                        </TouchableHighlight>
                    </Animatable.View>

                </View>
            </View>
        )
      }

    }
}
