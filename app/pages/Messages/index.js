import React, {Component} from 'react'
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  Image
} from 'react-native'
import {Actions} from 'react-native-router-flux'
import {setMessages} from '../../actions/messages'
import OnButtonPress from '../../components/OnButtonPress/'
import {setBackpackers} from '../../actions/backpackerActions'
import {connect} from 'react-redux'
import API from '../../api'
const styles = require('./style.js')

@connect((store) => {
  return {
    backpackers: store.backpackers.backpackers,
    user: store.user.user, messages: store.messages.messages}
}, (dispatch) => {
  return {
    getMessages: (fromuid) => {
      dispatch(setMessages(fromuid))
    },
    getBackpackers: () => {
      dispatch(setBackpackers())
    }
  }
})
export default class Messages extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  componentWillMount(){
    const app = this;
    console.log("================================");
    console.log("Getting messages...");
    console.log("================================");
    console.log(app.props.user.uid);
    app.props.getMessages(app.props.user.uid);
    app.props.getBackpackers();
  }

  componentDidMount() {
    const app = this;
    console.log("================================");
    console.log("Messages...");
    console.log("================================");
    console.log(app.props.messages);
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.contents}>
          <ScrollView>
            {this.props.messages!=null? this.props.messages.map(res => {
              return (
                <TouchableOpacity key={API.uuid()} style={styles.messageItem} onPress={() => Actions.chat({data: res, message_type:"existing"})}>
                  <Image style={styles.profileImage} source={{
                    uri: res.user.photoUrl
                  }}/>
                  <View style={{
                    flexDirection: 'column'
                  }}>
                    <Text style={styles.displayName}>{res.user.displayName == null
                        ? res.firstName
                        : res.user.displayName}</Text>
                    <Text style={styles.message}>{res.text}</Text>
                  </View>
                </TouchableOpacity>
              )
            }): null}
          </ScrollView>
        </View>
      </View>
    )
  }
}
