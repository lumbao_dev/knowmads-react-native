import {StyleSheet} from 'react-native';

const primaryColor = '#36ADE3';

module.exports = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF'
    },
    contents: {
      margin: 0,
    },
    subheader:{
      fontSize: 13,
      paddingBottom: 10
    },
    searchBox: {
      borderBottomWidth: 0.5,
      padding: 5,
      paddingLeft: 10,
      paddingRight: 10,
      borderColor: 'rgba(0,0,0,0.3)',
      flexDirection: 'row',
      alignItems: 'center'
    },
    searchResultBox: {
      padding: 10
    },
    profileImage: {
      width: 50,
      height: 50,
      borderRadius: 100
    },
    messageItem:{
      padding: 10,
      flexDirection: 'row',
      alignItems: 'center',
      borderColor: 'rgba(0,0,0,0.3)',
      borderBottomWidth: 0.5,

    },
    displayName: {
      marginLeft: 10,
      marginRight: 10
    },
    message:{
      color: 'rgba(0,0,0,0.3)',
      fontSize: 13,
      marginLeft: 10,
      marginRight: 10
    }
});
