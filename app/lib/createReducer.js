export default function createReducer(initialState, handlers) {
  console.log("Creating a reducer...")
  return function reducer(state = initialState, action) {
    console.log("Action type: "+action.type);
    if (handlers.hasOwnProperty(action.type)) {      
      return handlers[action.type](state, action)
    } else {
      return state
    }
  }
}
