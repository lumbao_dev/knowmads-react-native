import * as types from './types';
import API from '../api/';

export function setFeed(){
  return function(dispatch){
    fetch('https://us-central1-carbon-syntax-155109.cloudfunctions.net/getFeed')
    .then((response) => response.json())
    .then((responseJson)=>{
      dispatch({ type: types.FETCH_FEED_FULFILLED, payload: responseJson });
    }).catch((err)=>{
      dispatch({ type: types.FETCH_FEED_FAILED, payload: err });
    })
  }
}
