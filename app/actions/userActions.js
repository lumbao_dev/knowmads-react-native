import * as types from './types';
import {Actions} from 'react-native-router-flux';
import RNFirebase from 'react-native-firebase'
const configurationOptions = {
    debug: false
  }
  
firebase = RNFirebase.initializeApp(configurationOptions);

// export function signInUser(email, password) {
//     return function (dispatch) {
//         firestack.auth.signInWithEmail(email, password).then((user) => {
//             dispatch({ type: types.FETCH_USER_FULFILLED, payload: user });
//             Actions.home();
//         }).catch((err) => {
//             dispatch({ type: types.FETCH_USER_REJECTED, payload: err });
//         })
//     }
// }


export function setMapUser(user) {
    return function (dispatch) {
        dispatch({ type: types.SET_USER, payload: user });
        Actions.home();
    }
}


export function setUser(user) {
    return function (dispatch) {
        dispatch({ type: types.SET_USER, payload: user });
    }
}

export function setUserNotifications(user){
        return function (dispatch) {
        //dispatch({ type: types.SET_USER, payload: user });
    }
}

export function resetUserSettings(){
    return function (dispatch) {
        dispatch({ type: types.RESET_MAP_FILTERS, payload: null });
    }
}
