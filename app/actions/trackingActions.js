import * as types from './types';

export function setTrackingCoordinates(uid){
  return function(dispatch){
    fetch('https://us-central1-carbon-syntax-155109.cloudfunctions.net/getTrackingCoordinates?uid='+uid)
    .then((response) => response.json())
    .then((responseJson)=>{
      console.log(responseJson);
      dispatch({ type: types.FETCH_TRACKS_FULFILLED, payload: responseJson });
    }).catch((err)=>{
      dispatch({ type: types.FETCH_TRACKS_FAILED, payload: err });
    })
  }
}