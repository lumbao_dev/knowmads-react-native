import * as types from './types';
import API from '../api/';

export function setAccommodation(){
  return function(dispatch){
    fetch('https://us-central1-carbon-syntax-155109.cloudfunctions.net/getAccommodation')
    .then((response) => response.json())
    .then((responseJson)=>{
      dispatch({ type: types.FETCH_ACCOMMODATIONS_FULFILLED, payload: responseJson });
    }).catch((err)=>{
      dispatch({ type: types.FETCH_ACCOMMODATIONS_FAILED, payload: err });
    })
  }
}

export function setAttraction(){
  return function(dispatch){
    fetch('https://us-central1-carbon-syntax-155109.cloudfunctions.net/getAttraction')
    .then((response) => response.json())
    .then((responseJson)=>{
      dispatch({ type: types.FETCH_ATTRACTIONS_FULFILLED, payload: responseJson });
    }).catch((err)=>{
      dispatch({ type: types.FETCH_ATTRACTIONS_FAILED, payload: err });
    })
  }
}
