import * as types from './types';
import API from '../api/';

export function setBackpackers(){
  return function(dispatch){
    fetch('https://us-central1-carbon-syntax-155109.cloudfunctions.net/getBackpackers')
    .then((response) => response.json())
    .then((responseJson)=>{
      dispatch({ type: types.FETCH_BACKPACKERS_FULFILLED, payload: responseJson });
    }).catch((err)=>{
      dispatch({ type: types.FETCH_BACKPACKERS_FAILED, payload: err });
    })
  }
}
