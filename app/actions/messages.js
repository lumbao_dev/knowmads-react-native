import * as types from './types';
import API from '../api/';

// Fetch all messages from client. These are organized by channel.
// Channels are created when the user creates a new message thread.
export function setMessages(fromuid){
  return function(dispatch){
    fetch('https://us-central1-carbon-syntax-155109.cloudfunctions.net/getAllMessageChannelsFromClient?fromuid='+fromuid)
    .then((response) => response.json())
    .then((responseJson)=>{
      dispatch({ type: types.FETCH_MESSAGES_FULFILLED, payload: responseJson });
    }).catch((err)=>{
      dispatch({ type: types.FETCH_MESSAGES_FAILED, payload: err });
    })
  }
}

// Get message thread from channel.

export function setChannelMessages(fromuid,channel){
  return function(dispatch){
    fetch('https://us-central1-carbon-syntax-155109.cloudfunctions.net/getAllMessagesFromChannel?fromuid='+fromuid+'&channel='+channel)
    .then((response) => response.json())
    .then((responseJson)=>{
      dispatch({ type: types.FETCH_MESSAGES_FROM_CHANNEL_FULFILLED, payload: responseJson });
    }).catch((err)=>{
      dispatch({ type: types.FETCH_MESSAGES_FROM_CHANNEL_FAILED, payload: err });
    })
  }
}
