import * as types from './types';
import API from '../api/';

export function setUserConnections(uid) {
    return function (dispatch) {
      API.backpackerConnections(uid).on('value', (snapshot) => {
        dispatch({ type: types.FETCH_USER_CONNECTIONS_FULFILLED, payload: snapshot.val() });
      }).catch((err)=>{
        dispatch({ type: types.FETCH_USER_CONNECTIONS_REJECTED, payload: err });
      })
    }
}

export function setFriends(uid){
  return function(dispatch){
    fetch('https://us-central1-carbon-syntax-155109.cloudfunctions.net/getFriends?touid='+uid)
    .then((response) => response.json())
    .then((responseJson)=>{
      dispatch({ type: types.FETCH_FRIENDS_FULFILLED, payload: responseJson });
    }).catch((err)=>{
      dispatch({ type: types.FETCH_FRIENDS_FAILED, payload: err });
    })
  }
}

export function setNonFriends(uid){
  return function(dispatch){
    fetch('https://us-central1-carbon-syntax-155109.cloudfunctions.net/getNonFriends?touid='+uid)
    .then((response) => response.json())
    .then((responseJson)=>{
      dispatch({ type: types.FETCH_NON_FRIENDS_FULFILLED, payload: responseJson });
    }).catch((err)=>{
      dispatch({ type: types.FETCH_NON_FRIENDS_FAILED, payload: err });
    })
  }
}
