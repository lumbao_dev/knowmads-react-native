/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react'
import { AppRegistry, AsyncStorage } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import { Provider } from 'react-redux'
import store from './store/'
import {persistStore} from 'redux-persist'
import Container from './pages/Container/'

persistStore(store, {storage:AsyncStorage},()=>{
  console.log('restored')
});

const App = () => (
  <Provider store={store}>
    <Container />
  </Provider>
)

AppRegistry.registerComponent('knowmads', () => App);
