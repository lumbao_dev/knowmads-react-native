import {StyleSheet} from 'react-native';

module.exports = StyleSheet.create({
  icons: {
    width: 20,
    height: 20,
    resizeMode: 'contain',
    padding: 5
  }
})
