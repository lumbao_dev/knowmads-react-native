import {StyleSheet} from 'react-native';

module.exports = StyleSheet.create({
  myLocationMarker: {
      width: 50,
      height: 50
  }
})
