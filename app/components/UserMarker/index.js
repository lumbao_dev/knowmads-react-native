import React, {Component, PropTypes} from 'react';
import MapView from 'react-native-maps';
import {Image, View} from 'react-native';
const styles = require('./style.js');

export default class UserMarker extends Component {

  constructor(props) {
      super(props);
  }

  render(){
    return (
      <MapView.Marker key={'owner_' + Date.now()} coordinate={{latitude: this.props.latitude, longitude: this.props.longitude}}>
          <Image style={styles.myLocationMarker} source={require('../../assets/my-location.png')}></Image>
      </MapView.Marker>
    )
  }
}
