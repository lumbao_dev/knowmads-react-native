import React, { Component } from 'react';
import {Image, TouchableOpacity} from 'react-native';
const styles = require('./style.js');

export default class TabButtonIcon extends Component {
  constructor(props){
    super(props)
  }

  render() {
    const app = this;
    // Determine what icon to display based on the prop type.
    switch (this.props.type) {
      case "map-active": return (<Image style={styles.icons} source={require('../../assets/map-active.png')} />); break;
      case "map-inactive": return (<Image style={styles.icons} source={require('../../assets/map-inactive.png')} />); break;
      case "feed-active": return (<Image style={styles.icons} source={require('../../assets/feed-active.png')} />); break;
      case "feed-inactive": return (<Image style={styles.icons} source={require('../../assets/feed-inactive.png')} />); break;
      case "profile-active": return (<Image style={styles.icons} source={require('../../assets/profile-active.png')} />); break;
      case "profile-inactive": return (<Image style={styles.icons} source={require('../../assets/profile-inactive.png')} />); break;
      case "message-active": return (<Image style={styles.icons} source={require('../../assets/message-active.png')} />); break;
      case "message-inactive": return (<Image style={styles.icons} source={require('../../assets/message-inactive.png')} />); break;
      default: return (<Image style={styles.icons} />)
    }
  }
}
