import React, { Component } from 'react';
import {Image, TouchableOpacity} from 'react-native';
const styles = require('./style.js');

export default class TabButton extends Component {
  constructor(props){
    super(props)
    this.state = {
      iconSource: null
    }
  }

  setIconStates(){

    const app =this;
    switch (app.props.title) {
      case "Knowmads Map":
        if(app.props.selected){
          app.setState({iconSource:require('../../assets/map-active.png')})
        }else{
          app.setState({iconSource:require('../../assets/map-inactive.png')})
        }
        break;
      case "Feed":
        if(app.props.selected){
          app.setState({iconSource:require('../../assets/feed-active.png')})
        }else{
          app.setState({iconSource:require('../../assets/feed-inactive.png')})
        }
        break;
      default:
    }
  }

  componentDidMount(){
    //this.setIconStates()
  }

  render() {
    const app = this;
    var component = (<Image style={styles.icons} />);
    switch (app.props.title) {
      case "Knowmads Map":
        if(app.props.selected){
          component = (
            <Image style={styles.icons} source={require('../../assets/map-active.png')} />
          )
        }else{
          component = (
            <Image style={styles.icons} source={require('../../assets/map-inactive.png')} />
          )
        }
        break;
      case "Feed":
        if(app.props.selected){
          component = (
            <Image style={styles.icons} source={require('../../assets/feed-active.png')} />
          )
        }else{
          component = (
            <Image style={styles.icons} source={require('../../assets/feed-inactive.png')} />
          )
        }
        break;
      default:
        component = (
          <Image style={styles.icons} />
        )
    }

    return (
        <Image style={styles.icons} />
    )
  }
}
