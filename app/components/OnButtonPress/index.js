import {Alert} from 'react-native';

export default function OnButtonPress() {
  Alert.alert("We'll get there soon enough!", "This feature is still under heavy development and will be added on future updates.", [{text: 'Ok'}]);
}
