import React, { Component } from 'react';
import {Image } from 'react-native';
const styles = require('./style.js');

export default class FilterButtonIcon extends Component {
  constructor(props){
    super(props)
  }

  render() {
    return (
        <Image style={styles.icons} source={require('../../assets/settings.png')} />
    )
  }
}
