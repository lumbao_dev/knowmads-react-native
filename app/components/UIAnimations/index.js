import * as Animatable from 'react-native-animatable';

// Configurations
const ActionSheetPos = 350;
const ActionSheetBottomPos = 50;

// Handles all custom UI animations
module.exports = Animatable.initializeRegistryWithDefinitions({
    // mapSettingsSlideDown: {
    //     from: {
    //         top: -200
    //     },
    //     to: {
    //         top: 0
    //     }
    // },
    // mapSettingsSlideUp: {
    //     from: {
    //         top: 0
    //     },
    //     to: {
    //         top: -200
    //     }
    // },

    mapActionSheetSlideDown: {
        from: {
            bottom: ActionSheetPos * -1
        },
        to: {
            bottom: ActionSheetBottomPos
        }
    },
    mapActionSheetSlideUp: {
        from: {
            bottom: ActionSheetBottomPos
        },
        to: {
            bottom: ActionSheetPos * -1
        }
    }

})
