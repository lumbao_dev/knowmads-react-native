import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableHighlight } from 'react-native';
import { Actions } from 'react-native-router-flux';

export default class StatusModal extends Component {

  constructor(props) {
    super(props)
    // set state with passed in props
    this.state = {
      message: props.error,
      hide: props.hide,
    }
    // bind functions
    this.dismissModal = this.dismissModal.bind(this)
  }

  dismissModal() {
    this.setState({hide: true})
  }

  // show or hide Modal based on 'hide' prop
  render() {
    if(this.state.hide){
      return (
        <View>
        </View>
      )
    } else {
        return (
          <View style={styles.container}>
            <TouchableHighlight onPress={()=>Actions.pop()}>
              <Text>{this.state.message}</Text>
            </TouchableHighlight>
          </View>
        )
      }
  }
}

const styles = StyleSheet.create({
  container:{
    flex:1
  }
});
