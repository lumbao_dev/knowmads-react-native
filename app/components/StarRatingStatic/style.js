import {StyleSheet} from 'react-native';

const primaryColor = '#36ADE3'
const starPadding = 1
const starActiveColor = '#FBE645'
const starInactiveColor = '#CCCCCC'

module.exports = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    stars: {
        paddingLeft: starPadding,
        paddingRight: starPadding,
        color: starActiveColor
    },
    grayStars: {
        paddingLeft: starPadding,
        paddingRight: starPadding,
        color: starInactiveColor
    }
})
