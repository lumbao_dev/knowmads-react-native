import React, {Component, PropTypes} from 'react';
import {View} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

const styles = require('./style');

export default class StarRatingStatic extends Component {
    //TODO: Dynamically add stars based on prop value
    constructor(props) {
        super(props);
    }

    render() {
        // Populate stars
        var rows = [];
        var starCount = 0;
        // console.log(this.props.stars);
        for (var i = 0; i < this.props.stars; i++) {
            starCount += 1;
            rows.push(
                <Icon key={i + "lit"} name="star" style={[styles.stars, {fontSize:this.props.size}]}></Icon>
            );
        }
        var unlitStars = 5 - starCount;
        for (var i = 0; i < unlitStars; i++) {
            rows.push(
                <Icon key={i + "unlit"} name="star" style={[styles.grayStars, {fontSize:this.props.size}]}></Icon>
            );
        }
        return (
            <View style={styles.container}>
                {rows}
            </View>
        )
    }
}
