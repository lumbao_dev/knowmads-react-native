import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';
const styles = require('./style.js');

export default class GPSModal extends Component {
  constructor(props) {
    super(props)
    // set state with passed in props
    this.state = {
      error: props.error,
      hide: props.hide,
    }
    // bind functions
    this.dismissModal = this.dismissModal.bind(this)
  }

  dismissModal() {
    this.setState({hide: true})
  }
  render() {
    return (
      <View style={styles.container}>
        <Image source={require('../../assets/spot-marker.png')}/>
        <View style={styles.contents}>
          <Text style={styles.text}>{"Enable Location"}</Text>
          <Text style={styles.description} numberOfLines={5}>{"Your Locations Settings is set to 'Off'. Please Enable Location to fully use this app feature."}</Text>
          <Text style={styles.description} numberOfLines={5}>this.state.error</Text>
        </View>
      </View>
    )
  }
}
