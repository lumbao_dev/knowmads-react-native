// Knowmads color palette

module.exports = {
  Aquamarine:'#31C593',
  Crimson:'#DF0033',
  DeepSkyBlue: '#39AEE7',
  Turquise:'#45D4C4',
  Portica:'#FEE55A',
  DimGray:'#747474',
  Festival:'#F7D74A',
  PoloBlue: '#A5D3FF',
  PoloBlueLighter: '#CFE8FF',
  JordyBlue: '#A5CFFF',
  AthensGrey: '#E0E0E0',
  RiceCake: '#EFEBDE',
  White: '#FFF',
  Opal: '#A7A8A7'
}
