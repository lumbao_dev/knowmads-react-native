import {StyleSheet, Dimensions} from 'react-native'
const colors = require('./ColorPalette')

const primaryColor = '#36ADE3';
var width = Dimensions.get('window').width; //full width
module.exports = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF'
    },
    contents: {
        flex: 1,
        flexDirection: 'row',
        backgroundColor: '#FFF',
        alignItems: 'center',
        justifyContent: 'center'
    },
    feedContents: {
        flex: 1,
        flexDirection: 'row',
        zIndex: -2,
        backgroundColor: '#FFF'
    },
    statusBar: {
        backgroundColor: primaryColor
    },
    navBar: {
        backgroundColor: primaryColor,
        padding: 10
    },
    title: {
        color: '#FFF',
        fontWeight: '400'
    },
    navButton: {
        backgroundColor: 'transparent',
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: '#333',
        borderWidth: 0,
        padding: 0
    },
    navButtonRight: {
        marginRight: 14
    },
    navButtonIcon: {
        color: '#FFF',
        fontSize: 25,
        fontWeight: '700'
    },
    icons: {
        width: 23,
        height: 23,
        resizeMode: 'contain',
        padding: 5
    },
    searchBarContainer: {
        flex: 1,
        marginLeft: 30,
        marginRight: 15,
        backgroundColor: '#fff',
        borderRadius: 4,
        padding: 5,
        justifyContent: 'center'
    },
    iconSearch: {
        width: 13,
        height: 13,
        resizeMode: 'contain',
        marginLeft: 5
    },
    tabBar: {
        backgroundColor: '#FFF',
        height: 50,
        borderColor: '#C6C4C1',
        borderTopWidth: 1,
        flexDirection: 'row',
        alignItems: 'center'
    },
    tabBarButton: {
        flex: 1,
        alignItems: 'center',
        padding: 10
    },
    mapSettings: {
        borderBottomWidth: 0.5,
        backgroundColor: '#fff',
        borderColor: '#333',
        padding: 10,
        position: 'absolute',
        top: -200,
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        minWidth: width

    },
    mapSettingsItem: {
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10,
        flex: 1,
        flexDirection: 'row'
    },
    mapSettingsItemText: {
        flex: 1,
        textAlign: 'left',
        paddingLeft: 10

    },
    map: {
        ...StyleSheet.absoluteFillObject,
        flex: 1,
        top: 0,
        bottom: 0,
        left: 0,
        right: 0
    },
    modalBackground: {
        backgroundColor: 'rgba(0,0,0,0.5)',
        flex: 1,
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0
    },
    fabFloating: {
        backgroundColor: '#FFF',
        width: 60,
        height: 60,
        borderRadius: 50,
        margin: 20,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        right: 5,
        bottom: 5
    },
    fabFloatingIcon: {
        width: 50,
        height: 50
    },
    drawerIcons: {
        width: 30,
        height: 30,
        resizeMode: 'contain',
        marginLeft: 10,
        marginRight: 10
    },
    feedProfIcon: {
        width: 50,
        height: 50,
        resizeMode: 'contain'
    },
    profileImage: {
        height: 100,
        borderRadius: 50,
        width: 100
    },
    profileImageContent: {
        width: 100,
        height: 100,
        borderRadius: 100,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#E7E7E8',
        margin: 10
    },
    profileImageContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 20
    },
    profileImageEditIcon: {
        backgroundColor: '#32D1C0',
        padding: 10,
        borderRadius: 100,
        position: 'absolute',
        left: 70,
        zIndex: 200,
        paddingLeft:12,
        paddingRight:12
    },
    mapContainer: {
        flex: 1,
        alignItems: 'stretch'
    },
    chatBox: {
        flex: 1
    },

    // Custom Actionsheet styles

    actionSheetContainer: {
        position: 'absolute',
        bottom: -350,
        left: 0,
        right: 0,
        flex: 1
    },

    actionSheetContent: {
        margin: 10,
        backgroundColor: '#FFF',
        borderRadius: 7,
        flex: 1,
        marginBottom: 20
    },
    actionSheetItem: {
        flexDirection: 'row',
        padding: 8,
        alignItems: 'center',
        borderColor: 'rgba(0,0,0,0.2)',
        borderBottomWidth: 1
    },
    actionSheetLastItem: {
        flexDirection: 'row',
        padding: 10,
        alignItems: 'center'
    },
    actionSheetTitle: {
        margin: 10,
        fontSize: 18,
        color: primaryColor
    },
    actionSheetText: {
        fontSize: 18,
        marginLeft: 20
    },
    actionSheetLine: {
        flex: 1,
        borderColor: primaryColor,
        borderBottomWidth: 1
    },
    actionSheetIcon: {
        width: 35,
        height: 35
    },
    actionSheetDismiss: {
        margin: 10,
        backgroundColor: '#FFF',
        borderRadius: 7,
        position: 'absolute',
        bottom: -50,
        left: 0,
        right: 0,
        padding: 15,
        marginTop: 20
    },
    actionSheetDismissText: {
        fontSize: 18,
        color: primaryColor,
        textAlign: 'center'
    },
    myLocationMarker: {
        width: 50,
        height: 50
    },
    pointMarkers: {
        width: 30,
        height: 30,
        borderRadius: 50,
        borderWidth: 2,
        borderColor:'#31C593'
    },
    publicMarkers:{
      width: 30,
      height: 30,
      borderRadius: 50,
      borderWidth: 2,
      borderColor:'#31C593'
    },
    privateMarkers:{
      width: 30,
      height: 30,
      borderRadius: 50,
      borderWidth: 2,
      borderColor:'#DF0033'
    },
    spotMarkers: {
        width: 15,
        height: 15
    },
    loadingActivity: {
        height: 80,
        flex: 1
    },
    pointMarkerCallout: {
        flex: 1,
        width: 180,
        alignItems: 'center',
        padding: 7
    },
    avatarContainerMap: {
        width: 40,
        height: 40,
        borderRadius: 50,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.DimGray
    },
    markerAvatarPublic: {
        width: 40,
        height: 40,
        borderRadius: 50,
        backgroundColor: colors.DimGray
    },
    markerAvatarPrivate: {
        width: 40,
        height: 40,
        borderRadius: 50,
        backgroundColor: colors.DimGray
    },
    // TODO: to be removed in the future
    markerAvatarUnknown: {
        width: 40,
        height: 40,
        borderRadius: 50,
        backgroundColor: colors.DimGray
    },
    markerAvatar: {
        width: 40,
        height: 40,
        borderRadius: 50
    },
    markerText: {
        fontSize: 13,
        flex: 1,
        textAlign: 'left',
        paddingLeft: 10,
        width: 100
    },
    followerText: {
        paddingRight: 3,
        fontSize: 9,
        fontWeight: '700'
    },
    profileFollowerText: {
        fontSize: 13,
        fontWeight: '700',
        margin: 5,
        color: '#C6C4C1'
    },
    unFollowButton: {
        backgroundColor: '#ef5350',
        borderRadius: 10,
        paddingLeft: 20,
        paddingRight: 20,
        paddingTop: 10,
        paddingBottom: 10,
        marginBottom: 20
    },
    followButton:{
      backgroundColor: '#31C593',
      borderRadius: 10,
      paddingLeft: 20,
      paddingRight: 20,
      paddingTop: 10,
      paddingBottom: 10,
      marginBottom: 20
    },
    pendingButton:{
      backgroundColor: '#cfd8dc',
      borderRadius: 10,
      paddingLeft: 20,
      paddingRight: 20,
      paddingTop: 10,
      paddingBottom: 10,
      marginBottom: 20
    },
    followButtonText: {
        color: '#FFF',
        fontSize: 15,
        textAlign: 'center'
    },
    profileDetailsText: {
        textAlign: 'center'
    },
    profileButton: {
        backgroundColor: '#32D1C0'
    },
    profileButtonMessages: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10,
        backgroundColor: '#32D1C0',
        flex: 1
    },
    profileButtonCamera: {
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10,
        backgroundColor: '#36ADE3',
        flex: 1
    },
    profileButtonTracker:{
      alignItems: 'center',
      justifyContent: 'center',
      padding: 10,
      backgroundColor: '#85C592',
      flex: 1
    },
    profileButtonContent: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    profileButtonIcon: {
        color: '#fff',
        fontSize: 20
    },
    profileButtonText: {
        color: '#fff',
        padding: 5
    },
    avatarBackground: {
        flex: 1,
        width: undefined,
        height: undefined,
        backgroundColor: 'transparent',
        justifyContent: 'center',
        alignItems: 'center'
    },
    infoBar: {
        backgroundColor: 'rgb(255,255,255)',
        padding: 20
    },
    infoBarContents: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    infoBarIcon: {
        color: primaryColor,
        fontSize: 30,
        marginRight: 10
    },
    infoBarTextActive: {
        flex: 1,
        marginLeft: 5,
        marginRight: 3
    },
    infoBarTextInactive: {
        flex: 1,
        marginLeft: 5,
        marginRight: 3,
        color: "rgba(0,0,0,0.2)"
    },
    infoBarOKButton: {
        backgroundColor: primaryColor,
        padding: 10,
        paddingLeft: 20,
        paddingRight: 20
    },
    placeFeatureImage: {
        flex: 1,
        width: null,
        alignSelf: 'auto',
        alignItems: 'center',
        borderWidth: 0,
        resizeMode: 'cover',
        justifyContent: 'center',
        minHeight: 200,
        marginTop: 45
    },
    inputLabelDefault: {
        textAlign: 'left',
        fontSize: 12,
        color: "#969699"
    },
    textInputDefault: {
        flex: 1,
        backgroundColor: '#FFF',
        padding: 5,
        borderWidth: 0,
        textAlign: 'left'
    },
    inputGroup: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        borderBottomWidth: 0.5,
        borderColor: "rgba(0,0,0,0.3)",
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 20,
        paddingRight: 20
    },
    loginButtonDefault: {
        backgroundColor: primaryColor,
        padding: 16,
        borderRadius: 5,
        marginTop: 5,
        marginBottom: 5
    },
    submitGroup: {
        margin: 20
    },
    loginButtonDefault: {
        backgroundColor: primaryColor,
        padding: 16,
        borderRadius: 5,
        marginTop: 5,
        marginBottom: 5
    },
    notificationsHeader:{
      marginLeft:10, marginRight:10, padding: 10, borderBottomWidth: 0, borderColor: '#bfbfbf'
    }
});
