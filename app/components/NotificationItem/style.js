import {StyleSheet} from 'react-native';

module.exports = StyleSheet.create({
  container: {
    padding:5,
    backgroundColor: 'rgba(205, 205, 205, 0.1)'
  },
  notification__item:{
    flexDirection:'row', alignItems: 'center', padding: 10
  },
  notification__image:{
    borderRadius:50, width:50, height: 50, margin: 6
  },
  notification__textbox:{
    flexDirection: 'column', flex: 0.8, alignItems: 'flex-start'
  },
  notification__text:{

  },
  notification__text_left:{
    textAlign:'left'
  },
  notification__button:{
    padding: 8, paddingRight: 10, paddingLeft: 10, borderRadius: 5, margin: 3
  },
  notification__text_white:{
    color: '#FFF'
  },
  notification__text_bold:{
    fontWeight: '700'
  },
  notification__text_timeago:{
    color: '#cfcfcf'
  }
})
