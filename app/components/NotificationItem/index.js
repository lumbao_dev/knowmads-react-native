import React, {Component, PropTypes} from 'react';
import {View, Text, Image, TouchableOpacity, TouchableHighlight} from 'react-native';
import API from '../../api';
import OnButtonPress from '../../components/OnButtonPress/';
const TimeAgo = require('react-native-timeago');

const styles = require('./style.js');

export default class NotificationItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
          requestDisplayName: 'unknown',
          approved: false,
          type: 'follow', // Currently, there are two types: follow and track.
          requestPhotoUrl: 'https://firebasestorage.googleapis.com/v0/b/carbon-syntax-155109.appspot.com/o/photos%2Fprofile%2Favatar-default.png?alt=media&token=aaf56f25-3f4e-4728-ad06-ac87d4496ca8',
          requestButtonText: this.props.approved?"Revoke":"Confirm"
        }
    }

    componentDidMount(){
      const app = this;
      app.setState({type:app.props.type});
      API.backpacker(this.props.fromUid).once('value').then((snapshot)=>{
        let result = snapshot.val();
        app.setState({requestDisplayName: result.displayName});
        app.setState({requestPhotoUrl:result.photoUrl});
      });
    }

    submitRequest(){
      const app = this;
      API.submitFollowRequest(this.props.fromUid, this.props.toUid, this.props.notifId, app.props.approved==true?'revoke':'confirm');
      app.setState({approved:!app.state.approved});
    }

    render() {
      const app = this;
      var description = "";
      // Dynamic components
      if(!app.props.approved){
        ctaColor = '#36ADE3';
        switch (app.props.type) {
          case 'follow':
            description = 'requested to follow you. ';
            break;
            case 'track':
            description = 'requested to track your travel. ';
          default:
            console.log("Notification type not found.");
        }
      }else{
        ctaColor = '#EF5350';
        switch (app.props.type) {
          case 'follow':
            description = 'is following you. ';
            break;
            case 'track':
            description = 'is tracking you. ';
          default:
            console.log("Notification type not found.");
        }
      }


      return (
          <TouchableOpacity style={styles.container} onPress={OnButtonPress}>
            <View style={styles.notification__item}>
              <Image style={styles.notification__image} source={{uri:this.state.requestPhotoUrl}}/>
                <View style={styles.notification__textbox}>
                  <Text>
                    <Text style={styles.notification__text_bold}>{this.state.requestDisplayName + " "}</Text>
                    <Text style={[styles.notification__text,styles.notification__text_left]} numberOfLines={3}>
                      {description}
                    </Text>
                    <TimeAgo style={[styles.notification__text_bold, styles.notification__text_timeago]} time={this.props.timestamp} hideAgo={true} />
                  </Text>
                </View>
              <TouchableHighlight style={[styles.notification__button,{backgroundColor:ctaColor}]} onPress={()=>this.submitRequest()}>
                  <Text style={styles.notification__text_white}>{this.state.requestButtonText}</Text>
                </TouchableHighlight>
            </View>
          </TouchableOpacity>
        )
    }
}
