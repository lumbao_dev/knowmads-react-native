// Feed item component. Will handle spot reviews, photo posts and current location feed.
/** Feed item types:
Accomodation
Location
Review
Comment
Photo
Attraction
**/
import React, { Component, PropTypes } from 'react'
import {
  Text,
  View,
  Navigator,
  StyleSheet,
  TouchableHighlight,
  Alert,
  TextInput,
  Button,
  TouchableOpacity,
  InteractionManager,
  Image
} from 'react-native'
import API from '../../api'
import Icon from 'react-native-vector-icons/FontAwesome'
import ProgressBar from 'react-native-progress/Pie'
import FitImage from 'react-native-fit-image'
// import Firestack from 'react-native-firestack'
// const firestack = new Firestack()
const TimeAgo = require('react-native-timeago')
import Swiper from 'react-native-swiper'
import OnButtonPress from '../OnButtonPress/'
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux'
import MapView from 'react-native-maps'

const styles = require('./style')
const colors = require('../../components/GlobalStyles/ColorPalette.js')


// Custom components
import StarRatingStatic from '../StarRatingStatic/';

/*
TODO: Add a method that will determine if it is a review, photo feed or location feed.
Then generate the corresponding components for it.
*/
@connect((store)=>{
  return {
    user: store.user.user
  }
})
export default class FeedItem extends Component {
  constructor(props) {
    super(props);
    this.state={
      placeLocation: {
          latitude: 8.519043399999997,
          longitude: 120.89582419999998,
          latitudeDelta: 0.0922,
          longitudeDelta: 0.0421,
      }
    }
  }

  onRegionChange(region) {
  this.setState({ region });
  }

  componentDidMount(){
    const app = this;
  }

  // Accomodation Feed
  generateAccommodationFeed(data) {
    return (
      <View>
        <View style={styles.itemBox}>
          <View style={{ flex: 1, flexDirection: 'row', margin: 8, alignItems: 'flex-start' }}>
            <View style={{ padding: 0 }}>
              {/*Goes the targeted backpacker's profile]*/}
              <TouchableOpacity>
                <Image style={styles.feedProfIcon} source={{ uri: data.authorDetails.photoUrl }} />
              </TouchableOpacity>
            </View>
            <View style={styles.itemContent}>
              <View style={{ flex: 1 }}>
                <View style={{ flex: 1, alignItems: 'flex-start' }}>
                  <TouchableOpacity>
                    <Text style={styles.feedItemLink}>{data.authorDetails.displayName}</Text>
                  </TouchableOpacity>
                  <View style={{ flexDirection: 'row' }}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                      <Icon style={styles.postExtraDetails} name="map"></Icon>
                      <Text>{" Added Accommodation"}</Text>
                    </View>
                  </View>
                </View>
              </View>
            </View>
          </View>
        </View>
        <TouchableOpacity style={styles.mapContainer}  onPress={() => {
          Actions.previewPlace({ previewPlaceData: data.placeDetails, previewPlaceKey: data.placeid });
          API.updateViewCount(data.feedid,this.props.user.uid);
          }}>
          <MapView
            style={{ ...StyleSheet.absoluteFillObject, }}
            initialRegion={{
                latitude: data.placeDetails.location.latitude,
                longitude: data.placeDetails.location.longitude,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421,                
              }}
            onPress={() => this.marker.showCallout()}
            onRegionChangeComplete={() => this.marker.showCallout()}
            liteMode={true}
          >
            <MapView.Marker
              ref={marker => (this.marker = marker)}
              title={data.placeDetails.displayName}
              coordinate={{
                latitude: data.placeDetails.location.latitude,
                longitude: data.placeDetails.location.longitude
              }}
              pinColor={colors.DeepSkyBlue}
            />
          </MapView>
        </TouchableOpacity>
        <View style={{ margin: 8, flexDirection: 'row' }}>
          <View style={{ alignItems: 'center', flexDirection: 'row' }}>
            <Icon style={[styles.postExtraDetails, { marginTop: 2 }]} name="clock-o"></Icon>
            <Text>{' '}</Text>
            <TimeAgo style={styles.postExtraDetails} time={data.timestamp} hideAgo={false} />
          </View>
          <View style={{ flex: 1 }}></View>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <Icon style={styles.postExtraDetails} name="eye"></Icon>
            <Text style={[styles.postExtraDetails, { paddingRight: 3 }]}>{' ' + Object.keys(data.views).length + ' '}</Text>
          </View>
        </View>
        <View style={{ margin: 8, marginTop: 0 }}>
          <Text style={[styles.feedItemTitle, { color: '#333' }]}>
            {data.placeDetails.description}
          </Text>
        </View>
      </View>
    )
  }

  // Checkin Feed
  generateCheckinFeed(data) {
   return (
      <View>
        <View style={styles.itemBox}>
          <View style={{ flex: 1, flexDirection: 'row', margin: 8, alignItems: 'flex-start' }}>
            <View style={{ padding: 0 }}>
              {/*Goes the targeted backpacker's profile]*/}
              <TouchableOpacity>
                <Image style={styles.feedProfIcon} source={{ uri: data.authorDetails.photoUrl }} />
              </TouchableOpacity>
            </View>
            <View style={styles.itemContent}>
              <View style={{ flex: 1 }}>
                <View style={{ flex: 1, alignItems: 'flex-start' }}>
                  <TouchableOpacity>
                    <Text style={styles.feedItemLink}>{data.authorDetails.displayName}</Text>
                  </TouchableOpacity>
                  <View style={{ flexDirection: 'row' }}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                      <Icon style={styles.postExtraDetails} name="map-marker"></Icon>
                      <Text>{' '+data.placeDetails.location.name}</Text>
                    </View>
                  </View>
                </View>
              </View>
            </View>
          </View>
        </View>
        <TouchableOpacity style={styles.mapContainer} onPress={()=>{
          Actions.previewCheckin({data:this.props.data})
          API.updateViewCount(data.feedid,this.props.user.uid);
          }}>
          <MapView
            style={{ ...StyleSheet.absoluteFillObject, }}
            initialRegion={{
                latitude: data.placeDetails.location.latitude,
                longitude: data.placeDetails.location.longitude,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421,                
              }}
            liteMode={true}
          >
            <MapView.Marker
              coordinate={{
                latitude: data.placeDetails.location.latitude,
                longitude: data.placeDetails.location.longitude
              }}
              onPress={OnButtonPress}
            >
              <Image style={[styles.backpackerMarker, {borderColor: colors.DeepSkyBlue}]} source={{uri: data.authorDetails.photoUrl}}></Image>
            </MapView.Marker>
          </MapView>
        </TouchableOpacity>
        <View style={{ margin: 8, flexDirection: 'row' }}>
          <View style={{ alignItems: 'center', flexDirection: 'row' }}>
            <Icon style={[styles.postExtraDetails, { marginTop: 2 }]} name="clock-o"></Icon>
            <Text>{' '}</Text>
            <TimeAgo style={styles.postExtraDetails} time={data.timestamp} hideAgo={false} />
          </View>
          <View style={{ flex: 1 }}></View>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <Icon style={styles.postExtraDetails} name="eye"></Icon>
            <Text style={[styles.postExtraDetails, { paddingRight: 3 }]}>{' ' + Object.keys(data.views).length + ' '}</Text>
          </View>
        </View>      
      </View>
    )   
  }

  // Review Feed
  generateReviewFeed(data) {
    return (
      <View>
        <View style={styles.itemBox}>
          <View style={{ flex: 1, flexDirection: 'row', margin: 8, alignItems: 'flex-start' }}>
            <View style={{ padding: 0 }}>
              {/*Goes the targeted backpacker's profile]*/}
              <TouchableOpacity>
                <Image style={styles.feedProfIcon} source={{ uri: data.authorDetails.photoUrl }} />
              </TouchableOpacity>
            </View>
            <View style={styles.itemContent}>
              <View style={{ flex: 1 }}>
                <View style={{ flex: 1, alignItems: 'flex-start' }}>
                  <TouchableOpacity>
                    <Text style={styles.feedItemLink}>{data.authorDetails.displayName}</Text>
                  </TouchableOpacity>
                  <View style={{ flexDirection: 'row' }}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                      <Icon style={styles.postExtraDetails} name="star-half-o"></Icon>
                      <Text>{" Reviewed "}</Text>
                      <Text style={styles.feedItemLink}>{data.placeDetails.displayName}</Text>
                    </View>                
                  </View>                          
                </View>
              </View>
            </View>
          </View>
        </View>
        <TouchableOpacity onPress={()=>{
          //Actions.previewCheckin({data:this.props.data})
          API.updateViewCount(data.feedid,this.props.user.uid);
          }} style={{alignItems: 'center', padding:20, backgroundColor: colors.RiceCake}}>
          <StarRatingStatic stars={data.rating} size={25}/>
        </TouchableOpacity>
        <View style={{ margin: 8, flexDirection: 'row' }}>
          <View style={{ alignItems: 'center', flexDirection: 'row' }}>
            <Icon style={[styles.postExtraDetails, { marginTop: 2 }]} name="clock-o"></Icon>
            <Text>{' '}</Text>
            <TimeAgo style={styles.postExtraDetails} time={data.timestamp} hideAgo={false} />
          </View>
          <View style={{ flex: 1 }}></View>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <Icon style={styles.postExtraDetails} name="eye"></Icon>
            {/*<Text style={[styles.postExtraDetails, { paddingRight: 3 }]}>{JSON.stringify(data.views)}</Text>*/}
            <Text style={[styles.postExtraDetails, { paddingRight: 3 }]}>{' ' + Object.keys(data.views).length + ' '}</Text>
          </View>
        </View>
      </View>
    )
  }

  // Comment Feed
  generateCommentFeed(data) {
    return (
      <View>
        <View style={styles.itemBox}>
          <View style={{
            flex: 1,
            flexDirection: 'row',
            margin: 15,
            alignItems: 'flex-start'
          }}>
            <View style={{
              padding: 0
            }}>
              <Image style={styles.feedProfIcon} source={data.profilePhoto} />
            </View>
            <View style={styles.itemContent}>
              <View style={{
                flex: 1
              }}>
                <View style={{
                  flex: 1,
                  flexDirection: 'row',
                  alignItems: 'center'
                }}>
                  <Text style={styles.feedItemLink}>{data.userid}</Text>
                  <Text style={styles.feedItemTitle}>
                    {" commented on "}
                  </Text>

                  <Text style={styles.feedItemLink}>{this.state.placeName}</Text>

                </View>
                <View style={{
                  flexDirection: 'row',
                  alignItems: 'center'
                }}>
                  <Icon style={styles.time} name="clock-o"></Icon>
                  <Text style={styles.time}>
                    {' ' + data.timestamp}</Text>
                  <View style={{
                    flex: 1
                  }}></View>
                </View>
              </View>
            </View>
          </View>
        </View>
      </View>
    )
  }

  // Attraction Feed
   generateAttractionFeed(data) {
    return (
      <View>
        <View style={styles.itemBox}>
          <View style={{ flex: 1, flexDirection: 'row', margin: 8, alignItems: 'flex-start' }}>
            <View style={{ padding: 0 }}>
              {/*Goes the targeted backpacker's profile]*/}
              <TouchableOpacity>
                <Image style={styles.feedProfIcon} source={{ uri: data.authorDetails.photoUrl }} />
              </TouchableOpacity>
            </View>
            <View style={styles.itemContent}>
              <View style={{ flex: 1 }}>
                <View style={{ flex: 1, alignItems: 'flex-start' }}>
                  <TouchableOpacity>
                    <Text style={styles.feedItemLink}>{data.authorDetails.displayName}</Text>
                  </TouchableOpacity>
                  <View style={{ flexDirection: 'row' }}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                      <Icon style={styles.postExtraDetails} name="map"></Icon>
                      <Text>{" Added Attraction"}</Text>
                    </View>
                  </View>
                </View>
              </View>
            </View>
          </View>
        </View>
        <TouchableOpacity style={styles.mapContainer}  onPress={() => {
          Actions.previewPlace({ previewPlaceData: data.placeDetails, previewPlaceKey: data.placeid })
          API.updateViewCount(data.feedid,this.props.user.uid);
          }}>
          <MapView
            style={{ ...StyleSheet.absoluteFillObject, }}
            initialRegion={{
                latitude: data.placeDetails.location.latitude,
                longitude: data.placeDetails.location.longitude,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421,                
              }}
            onPress={() => this.marker.showCallout()}
            onRegionChangeComplete={() => this.marker.showCallout()}
            liteMode={true}

          >
            <MapView.Marker
              ref={marker => (this.marker = marker)}
              title={data.placeDetails.displayName}
              coordinate={{
                latitude: data.placeDetails.location.latitude,
                longitude: data.placeDetails.location.longitude
              }}
              pinColor={colors.Festival}
            />
          </MapView>
        </TouchableOpacity>
        <View style={{ margin: 8, flexDirection: 'row' }}>
          <View style={{ alignItems: 'center', flexDirection: 'row' }}>
            <Icon style={[styles.postExtraDetails, { marginTop: 2 }]} name="clock-o"></Icon>
            <Text>{' '}</Text>
            <TimeAgo style={styles.postExtraDetails} time={data.timestamp} hideAgo={false} />
          </View>
          <View style={{ flex: 1 }}></View>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <Icon style={styles.postExtraDetails} name="eye"></Icon>
            <Text style={[styles.postExtraDetails, { paddingRight: 3 }]}>{' ' + Object.keys(data.views).length + ' '}</Text>
          </View>
        </View>
        <View style={{ margin: 8, marginTop: 0 }}>
          <Text style={[styles.feedItemTitle, { color: '#333' }]}>
            {data.placeDetails.description}
          </Text>
        </View>
      </View>
    )
  }

  // Photo Feed
  generatePhotoFeed(data) {
    return (
      <View>
        <View style={styles.itemBox}>
          <View style={{ flex: 1, flexDirection: 'row', margin: 8, alignItems: 'flex-start' }}>
            <View style={{ padding: 0 }}>
              {/*Goes the targeted backpacker's profile]*/}
              <TouchableOpacity>
                <Image style={styles.feedProfIcon} source={{ uri: data.authorDetails.photoUrl }} />
              </TouchableOpacity>
            </View>
            <View style={styles.itemContent}>
              <View style={{ flex: 1 }}>
                <View style={{ flex: 1, alignItems: 'flex-start' }}>
                  <TouchableOpacity>
                    <Text style={styles.feedItemLink}>{data.authorDetails.displayName}</Text>
                  </TouchableOpacity>
                  <View style={{ flexDirection: 'row' }}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                      <Icon style={styles.postExtraDetails} name="camera"></Icon>
                      <Text>{' ' + data.placeDetails.location.name}</Text>
                    </View>
                  </View>
                </View>
              </View>
            </View>
          </View>

        </View>
        <TouchableOpacity onPress={()=>{
          Actions.previewPhoto({data:[{
            url: data.photoUrl
          }]})
          API.updateViewCount(data.feedid,this.props.user.uid);
          }}>
          <FitImage style={styles.photoGalleryItem} indicator indicatorColor="#333" // react native colors or color codes like #919191
            indicatorSize="small" // (small | large) or integer
            resizeMode="cover" source={{
              uri: data.photoUrl
            }}
          />          
        </TouchableOpacity>
        <View style={{ margin: 8, flexDirection: 'row' }}>
          <View style={{ alignItems: 'center', flexDirection: 'row' }}>
            <Icon style={[styles.postExtraDetails, { marginTop: 2 }]} name="clock-o"></Icon>
            <Text>{' '}</Text>
            <TimeAgo style={styles.postExtraDetails} time={data.timestamp} hideAgo={false} />
          </View>
          <View style={{ flex: 1 }}></View>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <Icon style={styles.postExtraDetails} name="eye"></Icon>
            <Text style={[styles.postExtraDetails, { paddingRight: 3 }]}>{' ' + Object.keys(data.views).length + ' '}</Text>
          </View>
        </View>
      </View>
    )
  }

  render() {
    var component;
    switch (this.props.data.type) {
      case "checkin": {
        component = this.generateCheckinFeed(this.props.data);
        console.log("checkin fired")
      };
      break;
      case "accommodation":{
        component = this.generateAccommodationFeed(this.props.data);
         console.log("accommodation fired")
      };
      break;
      case "attraction":{
        component = this.generateAttractionFeed(this.props.data);
         console.log("attraction fired")
      };
      break;
      case "photo":{
        component = this.generatePhotoFeed(this.props.data);
         console.log("photopost fired")
      };
      break;
      case "review":{
        component = this.generateReviewFeed(this.props.data);
         console.log("review fired")
      };
      break;
      default:
        component = [];
      break;
    }

    // XXX: Complicated to implement
    // if (this.props.data.type == "comment") {
    //   component = this.generateCommentFeed(this.props.data);
    // }
    return (
      <View>{component}</View>
    )
  }
}
