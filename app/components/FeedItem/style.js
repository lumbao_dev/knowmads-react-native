import {StyleSheet} from 'react-native'
const colors = require('../../components/GlobalStyles/ColorPalette.js')
const primaryColor = '#36ADE3';
const sidebarWidth = 5;

module.exports = StyleSheet.create({
    mapContainer:{
        flex: 1, height: 210
    },
    dateHeaderContainer: {
        backgroundColor: '#FFF',
        flexDirection: 'row'
    },
    feedProfIcon: {
        width: 50,
        height: 50,
        borderRadius: 25
    },
    postExtraDetails: {
        color: '#777',
    },
    itemSidebarGray: {
        backgroundColor: '#E7E7E8',
        width: sidebarWidth
    },
    itemSidebarBluegreen: {
        backgroundColor: '#32D1C0',
        width: sidebarWidth
    },
    itemSidebarBlue: {
        backgroundColor: primaryColor,
        width: sidebarWidth
    },
    itemSidebarOrange: {
        backgroundColor: '#FFA95E',
        width: sidebarWidth
    },
    itemDate: {
        fontSize: 16,
        padding: 8,
        backgroundColor: '#F8F8F8',
        flex: 1
    },
    itemBox: {
        backgroundColor: '#FFF',
        flexDirection: 'row',
        borderBottomWidth: 0.5,
        borderColor: '#E7E7E8',
        justifyContent: 'flex-start'
    },
    itemContent: {
        padding: 8,
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start'
    },
    feedItemTitle: {
        fontSize: 14,
        color: '#333'
    },
    feedItemLink: {
        fontSize: 14,
        backgroundColor: 'transparent',
        fontWeight: '700',
        color: primaryColor
    },
    photoFeed: {
        flex: 1,
        flexDirection: 'row'
    },
    photoThumbSmall: {
        width: 40,
        height: 40,
        margin: 5
    },
    photoGalleryPost:{
        width: 50,
        height: 50
    },
    wrapper:{

    },
    photoGalleryItem:{
        minHeight: 200, backgroundColor: '#777'
    },
    backpackerMarker:{
        width: 40,
        height: 40,
        borderRadius: 20,
        borderColor: colors.Aquamarine,
        borderWidth: 2
    }
})
